import argparse
import logging
import tempfile
from dataclasses import dataclass
from os.path import dirname, join
from typing import Any, Dict, Iterable, List, Set, Tuple

from jinja2 import Environment, FileSystemLoader, StrictUndefined
from paramiko import SSHClient
from paramiko.client import AutoAddPolicy
from scp import SCPClient  # type: ignore
from yaml import Loader, load

logging.basicConfig(level=logging.INFO)

DEPLOY_PROCESS = [
    "docker-compose pull",
    "docker-compose up -d --remove-orphans",
    "yes | docker image prune",
]
EXCLUDED_FOLDERS = {"/tmp"}


@dataclass
class Deployment:
    name: str
    path: str


@dataclass
class FileTemplates:
    name: str
    path: str
    dest: str


@dataclass
class ComposeDeployment(Deployment):
    dest: str
    folders: Iterable[str]
    templates: Iterable[FileTemplates]


@dataclass
class AnsiblePlaybook(Deployment):
    pass


@dataclass
class Server:
    name: str
    host: str
    username: str
    deployments: List[Deployment]


def _extract_compose_folders(path: str) -> Set[str]:
    folders = set()
    with open(path, "r") as f:
        data = load(f, Loader=Loader)
        for config in data.get("services", {}).values():
            volumes = config.get("volumes", [])
            for volume in volumes:
                if not isinstance(volume, dict):
                    continue
                if volume.get("type") == "bind" and "source" in volume:
                    source = volume["source"]
                    if "." in source:
                        source = dirname(source)
                    folders.add(source)
    return folders - EXCLUDED_FOLDERS


def _extract_file_templates(template_config: Dict[str, Any]) -> List[FileTemplates]:
    templates = []
    for name, details in template_config.items():
        templates.append(FileTemplates(name, details["path"], details["dest"]))
    return templates


def _load_servers(base_path: str) -> List[Server]:
    servers = []
    with open(join(base_path, "deployments.yaml"), "r") as f:
        data = load(f, Loader=Loader)
        for server_name, payload in data["servers"].items():
            server = Server(server_name, payload["host"], payload["username"], [])
            for title, details in payload["deployments"].items():
                match details["type"]:
                    case "compose":
                        server.deployments.append(
                            ComposeDeployment(
                                title,
                                details["path"],
                                details["dest"],
                                _extract_compose_folders(join(base_path, details["path"])),
                                _extract_file_templates(details.get("templates", {})),
                            )
                        )
                    case "ansible":
                        server.deployments.append(AnsiblePlaybook(title, details["path"]))
                    case _:
                        logging.error("Error parsing deployment %s (%s)", title, details)
                        exit(-1)
            servers.append(server)
    return servers


def _find_deployments(servers: List[Server], changed_files: Set[str]) -> List[Tuple[Server, Deployment]]:
    to_deploy = []
    for server in servers:
        for deployment in server.deployments:
            if deployment.path in changed_files:
                to_deploy.append((server, deployment))
    return to_deploy


def _load_variables(config_path: str, server_name: str, deployment_name: str) -> Dict[str, Any]:
    with open(config_path, "r") as f:
        data = load(f, Loader=Loader)
        variables = data.get("servers", {}).get(server_name, {}).get(deployment_name, {})
        assert isinstance(variables, dict)
        return variables


def _render_compose_templates(
    ssh: SSHClient,
    jinja: Environment,
    templates: Iterable[FileTemplates],
    variables: Dict[str, Any],
) -> None:
    for template in templates:
        ssh.exec_command(f"mkdir -p {dirname(template.dest)}")
        with tempfile.NamedTemporaryFile("w") as tmp_file:
            tmp_file.write(jinja.get_template(template.path).render(**variables))
            tmp_file.flush()
            with SCPClient(ssh.get_transport()) as scp:
                scp.put(tmp_file.name, template.dest)


def _deploy_compose_file(
    config_path: str,
    ssh_key_path: str,
    base_path: str,
    server: Server,
    deployment: ComposeDeployment,
) -> None:
    with SSHClient() as ssh:
        ssh.set_missing_host_key_policy(AutoAddPolicy)
        variables = _load_variables(config_path, server.name, deployment.name)
        ssh.load_system_host_keys()
        ssh.connect(hostname=server.host, username=server.username, key_filename=ssh_key_path)

        dest_dir = dirname(deployment.dest)
        ssh.exec_command(f"mkdir -p {dest_dir}")
        for folder in deployment.folders:
            ssh.exec_command(f"mkdir -p {folder}")

        jinja = Environment(
            loader=FileSystemLoader(base_path),
            trim_blocks=True,
            lstrip_blocks=True,
            undefined=StrictUndefined,
        )
        _render_compose_templates(ssh, jinja, deployment.templates, variables)

        with tempfile.NamedTemporaryFile("w") as tmp_file:
            tmp_file.write(jinja.get_template(deployment.path).render(**variables))
            tmp_file.flush()
            with SCPClient(ssh.get_transport()) as scp:
                scp.put(tmp_file.name, deployment.dest)

        for command in DEPLOY_PROCESS:
            _, stdout, _ = ssh.exec_command(f"cd {dest_dir} && {command}")
            exit_code = stdout.channel.recv_exit_status()
            stdout.channel.set_combine_stderr(True)
            for line in stdout.readlines():
                logging.info("[REMOTE(%s)] %s", server.host, line)
            if exit_code != 0:
                logging.error("FAILED - %s", exit_code)
                exit(-1)


def _deploy_ansible_playbook(server: Server, deployment: AnsiblePlaybook) -> None:
    pass


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument("--servers-dir", type=str, action="store", required=True)
    parser.add_argument("--config-path", type=str, action="store", required=True)
    parser.add_argument("--ssh-key-path", type=str, action="store", required=True)
    parser.add_argument("--file", type=str, action="store", required=True, nargs="+")
    args = parser.parse_args()
    print(args)

    base_path = args.servers_dir or ""
    config_path = args.config_path
    ssh_key_path = args.ssh_key_path

    changed_files = {filename for filename in args.file}
    logging.info("Changed Files (%s)", changed_files)

    servers = _load_servers(base_path)
    logging.info("Loaded Server/Deployment info %s", servers)

    to_deploy = _find_deployments(servers, changed_files)
    logging.info("Identified %s deployments (%s)", len(to_deploy), to_deploy)

    for server, deployment in to_deploy:
        logging.info("Deploying (%s) to (%s)", deployment, server.host)
        match deployment:
            case ComposeDeployment():
                _deploy_compose_file(config_path, ssh_key_path, base_path, server, deployment)
            case AnsiblePlaybook():
                _deploy_ansible_playbook(server, deployment)
            case _:
                logging.error("Unable to deploy %s - unknown type", deployment)
                exit(-1)
        logging.info("Deployed (%s)", deployment)


if __name__ == "__main__":
    main()
