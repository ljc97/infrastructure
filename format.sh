#! /bin/sh
nix-shell nix/projects.nix --attr tools.format-shell --command "treefmt"
nix run nixpkgs#statix -- check . # TEMP see ./treefmt.toml
