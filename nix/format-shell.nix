{ pkgs }: pkgs.mkShell {
  buildInputs = with pkgs; [
    treefmt
    nixpkgs-fmt
    rustfmt
    nodePackages.prettier
    python311Packages.black
    python311Packages.isort
    ruff
    clang
    go
    shellcheck
    shfmt
    statix
  ];
}
