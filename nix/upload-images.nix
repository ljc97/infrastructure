{ push_latest ? false, auth_path ? "" }:
let
  projects = import ./projects.nix;
  uploadImage = { name, derivation, remote_registry ? "registry.gitlab.com/ljc97/infrastructure", push_latest, auth_path }: projects.pkgs.stdenv.mkDerivation {
    inherit name derivation remote_registry;
    __noChroot = true;
    phases = [ "buildPhase" ];

    inherit auth_path;

    tag = derivation.imageTag;
    inherit push_latest;

    buildInputs = [ projects.pkgs.skopeo ];
    buildPhase = ''
      mkdir $out
      echo "pth $dst_auth_path"
      echo "Uploading $name:$tag ($derivation) to $remote_registry/$name:$tag"
      skopeo --insecure-policy copy docker-archive:$derivation docker://$remote_registry/$name:$tag --dest-authfile=$auth_path
      if [ "$push_latest" = "1" ]; then
        echo "Uploading $name:latest ($derivation) to $remote_registry/$name:latest"
        skopeo --insecure-policy copy docker://$remote_registry/$name:$tag docker://$remote_registry/$name:latest --authfile=$auth_path
      fi
    '';
  };
in
{
  # e.g nix-instantiate  nix/upload-images.nix  --attr uploaded_images --arg push_latest true --argstr auth_path ~/.docker/config.json
  uploaded_images = builtins.mapAttrs (name: value: uploadImage { inherit name; derivation = value; inherit push_latest; inherit auth_path; }) projects.images;
}
