{ config_path, ssh_key_path }:
let
  projects = import ./projects.nix;
  inherit (projects) pkgs;
  servers = pkgs.stdenv.mkDerivation {
    name = "servers";
    src = projects.gitignore.gitignoreSource ../deployments/servers;
    nativeBuildInputs = [ pkgs.yq ];

    phases = [ "buildPhase" ];
    buildPhase = ''
      mkdir $out
      cp -r $src/* $out
      yq "." $out/deployments.yaml > $out/deployments.json
    '';
  };

  services = builtins.fromJSON (builtins.readFile "${servers}/deployments.json");
  #(builtins.mapAttrs (name: value: {"media_deployments_${name}"={name=name; path=value.path;};}) (pkgs.lib.attrsets.filterAttrs(n: v: v.type == "compose") (builtins.fromJSON(builtins.readFile "${servers}/deployments.json")).servers.media.deployments))
  deployService = { name, path }: pkgs.stdenv.mkDerivation {
    inherit name;
    __noChroot = true;

    phases = [ "buildPhase" ];
    buildInputs = [ projects.python_projects.deployment.dependencyEnv servers ];

    servers_path = servers;
    inherit config_path;
    inherit ssh_key_path;

    buildPhase = ''
      mkdir $out
      echo "Server Base Config at $servers_path"
      echo "Config Path $config_path"
      echo "SSH Key Path $ssh_key_path"

      #python3 -m deploy --config-path $config_path --ssh-key-path $ssh_key_path --servers-dir $servers_path --file media/base/compose.yaml
    '';
  };
in
{
  inherit servers services;
  deployed_services = { media_base_infra = deployService { name = "media_base_infra"; path = "media/base/compose.yaml"; }; };
}
