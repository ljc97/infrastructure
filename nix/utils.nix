{ pkgs, poetry2nix, gitignore }:
rec {
  mkImage = { name, contents, pkgs_override ? pkgs }: pkgs_override.dockerTools.buildLayeredImage {
    inherit name;
    contents = contents ++ [ pkgs_override.dockerTools.caCertificates ];
  };

  mkPoetryApp = { projectDir, propagatedBuildInputs ? [ ], doCheck ? true }:
    poetry2nix.mkPoetryApplication {
      inherit projectDir;
      inherit propagatedBuildInputs;
      inherit doCheck;

      # TODO: once mypy > 1.5.2 instead of exclude in mypy.ini
      # TODO: --untyped-calls-exclude=prefect.task_runners
      checkPhase = ''
        runHook preCheck

        IFS=':'; python_libs=($PYTHONPATH); unset IFS;
        lint_path=""
        for path in "''${python_libs[@]}"
        do
            if [[ $path =~ .*"-python-linter-".* ]]
            then
                lint_path="$path"/python_linter/;
            fi
        done

        echo "---- black ----"
        black . --check -l 120
        echo "---- ruff ----"
        ruff check . --ignore E501 --ignore E999 --config "$lint_path".ruff.toml
        echo "---- mypy ----"
        mypy --config-file "$lint_path"mypy.ini .
        echo "---- pytest ----"
        find -name '*.py' | grep -v "__main__.py" | xargs pytest -vv

        runHook postCheck
      '';
    };
  mkPoetryPackage = { projectDir }:
    poetry2nix.mkPoetryPackages {
      inherit projectDir;
    };

  mkProtoSrc = { name, src }: pkgs.stdenv.mkDerivation {
    name = "${name}-proto";
    src = gitignore.gitignoreSource src;
    phases = [ "buildPhase" ];
    buildPhase = ''
      mkdir $out
      cp $src/*.proto $out/
    '';
  };

  mkRustPackageWithProtos = { naersk, name, src, protos }: naersk.buildPackage {
    inherit name;
    src = gitignore.gitignoreSource src;
    PROTO_PATH = protos;
    nativeBuildInputs = [ pkgs.protobuf protos ];
    doCheck = true;
  };
}
