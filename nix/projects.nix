let
  sources = import ./sources.nix;

  p2n = pkgs.callPackage sources.poetry2nix { };
  naersk = pkgs.callPackage sources.naersk { };
  gitignore = pkgs.callPackage sources.gitignore { };

  withPackages = { pypackage, pysuper, packages }: pypackage.overridePythonAttrs
    (
      old: {
        buildInputs = (old.buildInputs or [ ]) ++ packages;
      }
    );
  preferWheel = { pypackage }: pypackage.override {
    preferWheel = true;
  };

  setup_overlay = { pkgs }: import pkgs {
    overlays = [
      (self: super: {
        poetry2nix = p2n.overrideScope (p2nixself: p2nixsuper: {
          defaultPoetryOverrides = p2nixsuper.defaultPoetryOverrides.extend (pyself: pysuper: {
            promql-parser = preferWheel { pypackage = pysuper.promql-parser; };
            polars = preferWheel { pypackage = pysuper.polars; };
            pyroscope-io = preferWheel { pypackage = pysuper.pyroscope-io; };
            ruff = preferWheel { pypackage = pysuper.ruff; };
            pyarrow = preferWheel { pypackage = pysuper.pyarrow; }; #https://github.com/nix-community/poetry2nix/issues/1724
            pendulum = preferWheel { pypackage = pysuper.pendulum; };

            types-retry = withPackages { pypackage = pysuper.types-retry; inherit pysuper; packages = [ pysuper.setuptools ]; };
            coolname = withPackages { pypackage = pysuper.coolname; inherit pysuper; packages = [ pysuper.setuptools ]; };
            griffe = withPackages { pypackage = pysuper.griffe; inherit pysuper; packages = [ pysuper.pdm-backend ]; };
            apprise = withPackages { pypackage = pysuper.apprise; inherit pysuper; packages = [ pysuper.babel ]; };
            asgi-lifespan = withPackages { pypackage = pysuper.asgi-lifespan; inherit pysuper; packages = [ pysuper.setuptools ]; };
            jinja2 = withPackages { pypackage = pysuper.jinja2; inherit pysuper; packages = [ pysuper.flit ]; };
            cloudpickle = withPackages { pypackage = pysuper.cloudpickle; inherit pysuper; packages = [ pysuper.hatchling pysuper.flit ]; };
            docker = withPackages { pypackage = pysuper.docker; inherit pysuper; packages = [ pysuper.hatchling pysuper.hatch-vcs ]; };
            fsspec = withPackages { pypackage = pysuper.fsspec; inherit pysuper; packages = [ pysuper.hatchling pysuper.hatch-vcs ]; };
            urllib3 = withPackages { pypackage = pysuper.urllib3; inherit pysuper; packages = [ pysuper.hatch-vcs ]; };
            prefect = withPackages { pypackage = pysuper.prefect; inherit pysuper; packages = [ pysuper.setuptools ]; };
            jinja2-humanize-extension = withPackages { pypackage = pysuper.jinja2-humanize-extension; inherit pysuper; packages = [ pysuper.setuptools ]; };
          });
        });
      })

    ];
  };

  # niv add NixOS/nixpkgs -b release-23.11 -n nixpkgs_2311
  # niv drop nixpkgs_2305
  # niv update
  channels = {
    pkgs_2305 = setup_overlay { pkgs = sources.nixpkgs_2305; };
    pkgs_2311 = setup_overlay { pkgs = sources.nixpkgs_2311; };
    pkgs_2405 = setup_overlay { pkgs = sources.nixpkgs_2405; };
    pkgs_master = setup_overlay { pkgs = sources.nixpkgs_master; };
  };

  pkgs = channels.pkgs_2405;

  utils = pkgs.callPackage (import ./utils.nix) { inherit gitignore; };

in
rec {
  inherit pkgs channels gitignore;
  tools = {
    format-shell = pkgs.callPackage (import ./format-shell.nix) { };
  };

  learning = {
    cpp_crash_course = pkgs.callPackage (import ../learning/cpp_crash_course/shell.nix) { };
    leetcode = pkgs.callPackage (import ../learning/leetcode/leetcode.nix) { inherit naersk; inherit utils; inherit gitignore; };
  };

  playground = {
    p_1_nats = channels.pkgs_2305.callPackage (import ../services/playground/1_nats/nats.nix) { };
    p_2_conditional_funcs = utils.mkPoetryApp { projectDir = ../services/playground/2_conditional_funcs/.; doCheck = false; };
    p_3_multiples_pricing = pkgs.callPackage (import ../services/playground/3_multiples_pricing/multiples_pricing.nix) { inherit naersk; };
    p_4_glassdoor = pkgs.callPackage (import ../services/playground/4_glassdoor/glassdoor.nix) { };
    p_5_acyclic_graph = pkgs.callPackage (import ../services/playground/5_acyclic_graph/acyclic_graph.nix) { };
  };

  python_projects = {
    python_linter = utils.mkPoetryApp { projectDir = ../packages/python_linter/.; doCheck = false; };
    core_python = utils.mkPoetryApp { projectDir = ../packages/core_python/.; };
    deployment = utils.mkPoetryApp { projectDir = ../deployments/.; };

    # TODO: need to find some way to add ^ apps/packages back into the poetry2nix overlay as they're created
    prefect = utils.mkPoetryApp { projectDir = ../services/data_ingest/prefect/.; propagatedBuildInputs = [ python_projects.core_python ]; };
    arctic2prometheus = utils.mkPoetryApp { projectDir = ../services/arctic2prometheus/.; propagatedBuildInputs = [ python_projects.core_python ]; };
  };

  images = {
    jenkins_with_docker-image = pkgs.callPackage (import ../services/base_images/jenkins_with_docker/jenkins_with_docker.nix) { };
    webhook-api-image = utils.mkImage { name = "webhook-api"; contents = [ webhook.api ]; };
    webhook-ingress-image = utils.mkImage { name = "webhook-ingress"; contents = [ webhook.ingress ]; };
    prefect-image = utils.mkImage { name = "prefect"; contents = [ python_projects.prefect.dependencyEnv pkgs.git pkgs.busybox ]; };
    prefect-arm64-image = utils.mkImage { name = "prefect-arm64"; contents = [ python_projects.prefect.dependencyEnv ]; pkgs_override = pkgs.pkgsCross.aarch64-multiplatform; };
    arctic2prometheus-image = utils.mkImage { name = "arctic2prometheus"; contents = [ python_projects.arctic2prometheus.dependencyEnv ]; };
    arctic2prometheus-arm64-image = utils.mkImage { name = "arctic2prometheus-arm64"; contents = [ python_projects.arctic2prometheus.dependencyEnv ]; pkgs_override = pkgs.pkgsCross.aarch64-multiplatform; };
  };

  webhook = {
    dev_shell = pkgs.callPackage (import ../services/webhook/dev_shell.nix) { };
    proto = utils.mkProtoSrc { name = "webhook"; src = ../services/webhook/proto; };
    api = utils.mkRustPackageWithProtos { inherit naersk; name = "webhook-api"; src = ../services/webhook/api; protos = webhook.proto; };
    ingress = utils.mkRustPackageWithProtos { inherit naersk; name = "webhook-ingress"; src = ../services/webhook/ingress; protos = webhook.proto; };
  };

  # TODO: expression to auto flatten this
  # {a = "drv"; b = { c = "drv"; d = { e= "drv";};};} -> {a = "drv", b_c = "drv", b_d_e = "drv"};
  all = images // python_projects // learning // playground // tools // webhook;
}
