import inspect
import logging
import os
from dataclasses import dataclass
from itertools import chain
from json import load
from typing import TYPE_CHECKING, Any, Dict, Iterable, Optional, Tuple, Type, TypeVar

from dacite import from_dict

if TYPE_CHECKING:
    from arcticdb.version_store.library import Library  # type: ignore
    from sqlalchemy import Engine
logging.basicConfig(level=logging.INFO)


@dataclass
class SentryConfig:
    dsn: str
    profiling: bool = False
    stacktraces: bool = True


@dataclass
class DatabaseConfig:
    url: str


@dataclass
class PyroscopeConfig:
    url: str


@dataclass
class ArcticConfig:
    url: str
    lib: str


@dataclass
class AppConfig:
    app_name: str
    environment: str
    sentry: SentryConfig
    database: Optional[DatabaseConfig]
    pyroscope: Optional[PyroscopeConfig]
    arctic: Optional[ArcticConfig]


T = TypeVar("T", bound=AppConfig)


def load_config(file_path: str, target_class: Type[T]) -> T:
    with open(file_path, "r") as f:
        data = load(f)
        config: T = from_dict(data_class=target_class, data=data)
    return config


def init_sentry(config: AppConfig) -> None:
    import sentry_sdk

    params: Dict[str, Any] = {
        "dsn": config.sentry.dsn,
        "environment": config.environment,
        "attach_stacktrace": config.sentry.stacktraces,
    }
    if config.sentry.profiling:
        params["traces_sample_rate"] = 1.0
        params["profiles_sample_rate"] = 1.0
    sentry_sdk.init(**params)


def init_pyroscope(config: AppConfig) -> None:
    assert config.pyroscope is not None
    import pyroscope  # type: ignore

    pyroscope.configure(
        application_name=config.app_name,
        server_address=config.pyroscope.url,
        tags={"environment": config.environment},
    )


def init_db(config: AppConfig) -> "Engine":
    assert config.database is not None
    from sqlalchemy import create_engine

    return create_engine(config.database.url)


def init_arctic(config: AppConfig) -> "Library":
    from arcticdb import Arctic  # type: ignore

    assert config.arctic is not None
    db = Arctic(config.arctic.url)
    if not db.has_library(config.arctic.lib):
        db.create_library(config.arctic.lib)
    return db.get_library(config.arctic.lib)


def migrate_db(db: "Engine", path: str) -> None:
    from sqlalchemy import text

    with db.begin() as cur:
        for migration_file in sorted(os.listdir(path)):
            with open(f"{path}/{migration_file}", "r") as f:
                logging.info("Applying Migration from %s", migration_file)
                query = text(" ".join(f.readlines()))
                cur.execute(query)
                logging.info("Applied!")


def load_config_from_prefect(namespace: str, target_class: Type[T]) -> T:
    from prefect import variables
    from prefect.blocks.system import Secret

    # Variables support "_"
    # Secrets support "-"
    # TODO: long term, load json, then detect parameters
    #   Load secrets for each parameter, render template
    #   Then pass that into from_dict()

    def _var_path(parts: Iterable[str]) -> str:
        return "_".join(chain([namespace], parts))

    def _sec_path(parts: Iterable[str]) -> str:
        return "-".join(chain([namespace], parts))

    data = {
        "app_name": variables.get(_var_path(["appname"])),
        "environment": variables.get(_var_path(["environment"])),
        "sentry": {
            "dsn": Secret.load(_sec_path(["sentry", "dsn"])).get(),
            "attach_stacktrace": variables.get(_var_path(["sentry", "stacktraces"])),
            "profiling": bool(variables.get(_var_path(["sentry", "profiling"]))),
        },
    }

    db_path = _sec_path(["database", "url"])
    try:
        data["database"] = {"url": Secret.load(db_path).get()}
    except ValueError:
        pass

    pyro_path = _var_path(["pyroscope", "url"])
    try:
        data["pyroscope"] = {"url": variables.get(pyro_path)}
    except ValueError:
        pass

    try:
        data["arctic"] = {
            "url": Secret.load(_sec_path(["arctic", "url"])).get(),
            "lib": variables.get(_var_path(["arctic", "lib"])),
        }
    except ValueError:
        pass

    return from_dict(data_class=target_class, data=data)


def setup_app(
    config_path: str, target_class: Type[T], use_prefect: bool = False
) -> Tuple[T, Optional["Engine"], Optional["Library"]]:
    config: T = (
        load_config_from_prefect(config_path, target_class) if use_prefect else load_config(config_path, target_class)
    )

    init_sentry(config)
    if config.pyroscope is not None:
        init_pyroscope(config)

    db = None
    if config.database:
        db = init_db(config)
        migrate_db(db, path=os.path.dirname(inspect.stack()[1][1]) + "/migrations")

    arctic = None
    if config.arctic:
        arctic = init_arctic(config)

    return config, db, arctic
