from core_python.base_app import AppConfig, load_config


def test_load_config() -> None:
    load_config("sample_config.json", AppConfig)


def test_pyroscope() -> None:
    import pyroscope  # type: ignore

    pyroscope.configure(
        application_name="unit_test",
        server_address="127.0.0.1:4040",
        tags={"environment": "dev"},
    )
