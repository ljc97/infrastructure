data "cloudflare_ip_ranges" "cloudflare" {}

resource "hcloud_firewall" "unifi-controller-fw" {
  name = "unifi-controller-fw"

  rule {
    direction = "in"
    protocol  = "udp"
    port      = "3478"
    source_ips = [
      var.wan_ip
    ]
  }

  rule {
    direction = "in"
    protocol  = "tcp"
    port      = "8080"
    source_ips = [
      var.wan_ip
    ]
  }
  rule {
    direction = "in"
    protocol  = "tcp"
    port      = "8443"
    source_ips = [
      var.wan_ip
    ]
  }
  rule {
    direction = "in"
    protocol  = "tcp"
    port      = "8880"
    source_ips = [
      var.wan_ip
    ]
  }
  rule {
    direction = "in"
    protocol  = "tcp"
    port      = "8843"
    source_ips = [
      var.wan_ip
    ]
  }
  rule {
    direction = "in"
    protocol  = "tcp"
    port      = "6789"
    source_ips = [
      var.wan_ip
    ]
  }
  rule {
    direction  = "in"
    protocol   = "tcp"
    port       = "443"
    source_ips = data.cloudflare_ip_ranges.cloudflare.cidr_blocks
  }
}

resource "hcloud_server" "unifi-controller" {
  name         = "unifi-controller"
  server_type  = "cx11"
  image        = "ubuntu-20.04"
  location     = "hel1"
  backups      = false
  ssh_keys     = [hcloud_ssh_key.infra.id]
  firewall_ids = [hcloud_firewall.default-fw.id, hcloud_firewall.unifi-controller-fw.id]
  user_data    = file("./templates/docker.yaml")

  network {
    network_id = hcloud_network.default-net.id
    ip         = "10.32.64.3"
  }

  depends_on = [
    hcloud_network_subnet.default-subnet
  ]
}

resource "hcloud_volume_attachment" "unifi-controller" {
  volume_id = hcloud_volume.unifi-controller.id
  server_id = hcloud_server.unifi-controller.id
  automount = true
}

resource "hcloud_volume" "unifi-controller" {
  name     = "unifi-controller"
  location = "hel1"
  size     = 10
  format   = "ext4"
}

resource "cloudflare_record" "unifi" {
  zone_id = cloudflare_zone.arietis.id
  name    = "unifi"
  value   = hcloud_server.unifi-controller.ipv4_address
  type    = "A"
  ttl     = 1
  proxied = true
}

resource "cloudflare_record" "unifi_devices" {
  zone_id = cloudflare_zone.arietis.id
  name    = "devices.unifi"
  value   = hcloud_server.unifi-controller.ipv4_address
  type    = "A"
  ttl     = 3600
  proxied = false
}

resource "cloudflare_record" "unifiv6" {
  zone_id = cloudflare_zone.arietis.id
  name    = "unifi"
  value   = hcloud_server.unifi-controller.ipv6_address
  type    = "AAAA"
  ttl     = 1
  proxied = true
}


resource "unifi_site" "nw6" {
  description = "NW6"
}

data "unifi_ap_group" "default" {
}

resource "unifi_user_group" "default" {
  name = "Default"
}

resource "unifi_network" "net" {
  name    = "Net"
  purpose = "corporate"
  # vlan_id = 0
  subnet         = "192.168.1.1/24"
  domain_name    = "localdomain"
  dhcp_enabled   = false
  dhcp_start     = "192.168.1.6"
  dhcp_stop      = "192.168.1.254"
  ipv6_ra_enable = false
  wan_gateway    = "192.168.1.1"
  lifecycle {
    ignore_changes = [
      wan_gateway,
      ipv6_pd_start,
      ipv6_pd_stop
    ]
  }
}

resource "unifi_network" "net10" {
  name           = "Net10"
  purpose        = "corporate"
  vlan_id        = 10
  subnet         = "192.168.2.1/24"
  domain_name    = "net10"
  dhcp_enabled   = false
  dhcp_start     = "192.168.2.6"
  dhcp_stop      = "192.168.2.254"
  ipv6_ra_enable = false
  wan_gateway    = "192.168.2.1"
  lifecycle {
    ignore_changes = [
      wan_gateway,
      ipv6_pd_start,
      ipv6_pd_stop
    ]
  }
}


resource "unifi_network" "net30" {
  name           = "Net30"
  purpose        = "corporate"
  vlan_id        = 30
  subnet         = "192.168.3.1/24"
  domain_name    = "net30"
  dhcp_enabled   = false
  dhcp_start     = "192.168.3.6"
  dhcp_stop      = "192.168.3.254"
  ipv6_ra_enable = true
  wan_gateway    = "192.168.3.1"
  lifecycle {
    ignore_changes = [
      wan_gateway,
      ipv6_pd_start,
      ipv6_pd_stop
    ]
  }
}

resource "unifi_wlan" "wlan1" {
  name       = var.unifi_wlan1_name
  passphrase = var.unifi_wlan1_password
  security   = "wpapsk"

  minimum_data_rate_2g_kbps = 6000
  minimum_data_rate_5g_kbps = 6000

  wpa3_support      = true
  wpa3_transition   = true
  pmf_mode          = "optional"
  no2ghz_oui        = true
  wlan_band         = "both"
  radius_profile_id = null

  network_id    = unifi_network.net.id
  ap_group_ids  = [data.unifi_ap_group.default.id]
  user_group_id = unifi_user_group.default.id
}

resource "unifi_wlan" "wlan2" {
  name       = var.unifi_wlan2_name
  passphrase = var.unifi_wlan2_password
  security   = "wpapsk"

  minimum_data_rate_2g_kbps = 6000
  minimum_data_rate_5g_kbps = 6000

  wpa3_support      = true
  wpa3_transition   = true
  pmf_mode          = "optional"
  no2ghz_oui        = true
  wlan_band         = "both"
  radius_profile_id = null

  network_id    = unifi_network.net10.id
  ap_group_ids  = [data.unifi_ap_group.default.id]
  user_group_id = unifi_user_group.default.id

}

resource "unifi_wlan" "wlan3" {
  name       = var.unifi_wlan3_name
  passphrase = var.unifi_wlan3_password
  security   = "wpapsk"

  minimum_data_rate_2g_kbps = 6000
  minimum_data_rate_5g_kbps = 6000

  wpa3_support      = true
  wpa3_transition   = true
  pmf_mode          = "optional"
  no2ghz_oui        = true
  wlan_band         = "both"
  radius_profile_id = null

  network_id    = unifi_network.net30.id
  ap_group_ids  = [data.unifi_ap_group.default.id]
  user_group_id = unifi_user_group.default.id
}


resource "betteruptime_monitor" "unifi" {
  provider = better-uptime

  url          = "https://unifi.arietis.uk"
  monitor_type = "status"
}
