# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/betterstackhq/better-uptime" {
  version     = "0.3.5"
  constraints = "0.3.5"
  hashes = [
    "h1:0ZTb2n3xe230WJOtLWZrNoILsrzbdrZqvarRw/KiVWs=",
    "zh:0eb8f8c54a3898c0e4a1ef00ecf76f9fad5d120f183c26096b81ee89e24c151b",
    "zh:21758fb54fc4c34c7db61237575bd3795783644b01adeabd6f504aea166611e8",
    "zh:280b04e97ba9ca7ac14647bb7aeef549b8a68367fa35106b8c2602192c5f3f96",
    "zh:2da610f3950e28687a432b7ccc230a4196e64d12ab09b195f2e7125706fd7f10",
    "zh:46d987419ff86727991abe0eacf5ef97c0a571b063711d564aac2956d474bf20",
    "zh:4a182ca5e3d514c6da8c0fb680cef431559ac3603d1951a6c29d76fc46f8d4ae",
    "zh:4f168ce429f7abeae7a6483122f7c434a472f6c9839a5e68c64c4b546615bd69",
    "zh:61024d46d3ab1434830bbeb3e0fcf17c550abada0aa9504e45def943061dee7b",
    "zh:61deadbe74bfa8ed51b59fe24544c06678886f1d02dcb64f2dcfef046f1058d7",
    "zh:b254429fdd99d0106f06d9215a5af8c946cd57cfa917dd48cd27b03044b65251",
    "zh:ba5a11437bfe214602f4cfef9d8b8e9deccad4340e8939cbb2874631c53e72c5",
    "zh:bfdeabbadfae41f492363023bc2bdb1ed2aeee3de44f92bcfa8c76c1ac695ada",
    "zh:ffedc17ad970a15ba58038d6e519676544c818b444bea64bf323eeac89a22486",
  ]
}

provider "registry.terraform.io/cloudflare/cloudflare" {
  version     = "2.26.1"
  constraints = "2.26.1"
  hashes = [
    "h1:P1ktP+9KnBoqJUnoGbGzOVRbMef1jXL282sJIJ5KlS4=",
    "h1:v7U7t1T640aYSovCMiV8ehRwVDN8ylnSZNh3KtRQn3s=",
    "zh:0fc9e723b7f012870b32f82c094794b5c5debde00d5dc48dbd25beb8e8117171",
    "zh:19a1eb85da263488f766cd2b0e30a4cebb8416c8f928fcb89df3dd12bf6dabef",
    "zh:1fc097f4d471c553e2effc547d0af07d8d1cfb8cf70b0a1e9c7b87db28d823bc",
    "zh:6a33b565397501a6c21095cf773418311a1c192aa8fc1389d258583417b8da8a",
    "zh:6f0b9f88c150d01d30a8c3aa5fe77183a4e43ff344e4b5efec2f12a422222e72",
    "zh:8bf32bbcd88f9078c2b06d5d260cba69da641f2df359ab2e6bd953c6de01610f",
    "zh:91f82ad1e3a880399d1e866200304d259fd599f02c086ddb62402b5501f1ec73",
    "zh:aa0d76d7a3d541452228a40b36947c94cf3481411e80327d70b7a3b4f62eb097",
    "zh:acc4703c44122228c1dd8cda1b6b051d53d6433c708c1bf7f9c27faa6371f8df",
    "zh:d05eb3a268cdb656def53db47fd4e623b060944aa4865a3f2bbf645fdbe7d102",
    "zh:f6032d4a3ef0fe6c034c59d0c75134bc738c5a6f9e16cc9fac9649633b94f674",
    "zh:fa626e82dbee19f997a4e5d1d6b3ddf27c91fee54b219f50ec91fea9a15c7508",
    "zh:fbf97c0025baa0fb7937ccc0782158c8531b9c965b32a4bc8930418e5520c6a8",
  ]
}

provider "registry.terraform.io/digitalocean/digitalocean" {
  version     = "2.11.1"
  constraints = "2.11.1"
  hashes = [
    "h1:/vPcLUkdXevyzqVi2KdygASF2PhzCq+/R49rgoz164U=",
    "h1:6PNfB68DDo5RkXhepEUSt95bZgT4i8J2eJNcaQTxlCs=",
    "zh:0b379be98de2927eb751db4e88e69b14c979b97f33ff5b3f7840e27bbb3b10c4",
    "zh:28cdc581a684681eee6c871da76f0a6e9a99c4ae7dfabb0ffbc62c2566299a3b",
    "zh:41c332b015af4b0b08c7f34c57bd1f6e687ab78134d8fa891aba2357e2b0ced5",
    "zh:537da9f53842fe9c3ee3d8ae7ff8ee5a847a879ec881a9cf087e677d36e642c0",
    "zh:621782735bb0fa0e9b12750a6e91c2ef0d144185201435f0f70a95b66aa08dd1",
    "zh:7f59913181a0fb6d78ccfdac19f17d4b693f67776baf8ffb6a9cd07088804be8",
    "zh:9b164e204085691876535233eb57cd6fdf223a0eff529ceb072b61f4fea2f950",
    "zh:9d3c87e40b11f19bb0e59b369663d05b03e37d3095200a7b40e7bc7e04e3a303",
    "zh:9fe3ec149fcbe25f987b02e59d810a8fd836a9f4b4b7a9ed7273800c8020cc87",
    "zh:a426e765a4df74b21196d2faf4786bec2a7fdc2c21bf1137a7fb9a1c7e1cf1fc",
    "zh:aae6361881b90802d5fad20ca2cd7330e118be09cf583b79b9276e033b09beb4",
    "zh:c760535090ec85194e8248c823439a1692be72e4f337916f63c742ee8cb1f7cd",
    "zh:eca7a79f7c3611b79a1013b3390ac917043ac19cd648bda8c87e4b7714b27b00",
    "zh:f221a20df002cffac09dba1734a1f77217dd546705fd1b123951d03c0fff6592",
    "zh:f62b7120420cc0d3ff9d291aa1aae20b5868dd47d453cb3a33b915615f5cdb29",
  ]
}

provider "registry.terraform.io/hashicorp/oci" {
  version     = "4.45.0"
  constraints = "4.45.0"
  hashes = [
    "h1:jmyc6DRAzYOXuY9pTdYWnFos3kBHovcZFaTvipxxmoc=",
    "zh:0c63842b2c4b623ca817beced8ae1f049d23adac2dd74eb1706e594d8d9c18c8",
    "zh:44120c1aedf84761219f58946dee84a48799a076e8b91542bb3103142c5eb61e",
    "zh:61e2027554fb7bce3cca40cfc564b8300a93767c830e775ea75e92d3f9cbe4a0",
    "zh:726e20e36c2c84494a0d68ca65aec446ec8edb206f3a4b4c40dc98b3474fb99b",
    "zh:7a924b1cd6bb55815a2f263752072ade1b7a8e127aecf59d7fe0d16dc67f59a4",
    "zh:80b8ab2fc3e0dab5dfdcf1dac754abf7e088711d51f34ce9246ee3a24987c654",
    "zh:8daf477de51e18e27d62ce84cbda3d60f41817adbd84bd0db461d3689ab4e4a5",
    "zh:a7866832b35b54523311d81c6ca5cb16f7f7553f6b8d8700e1815e943563d762",
    "zh:b563e0a6cdeeb2c2ab772b70d7dfd4df09000f2f9a188e94599377b091f1f782",
    "zh:be18fba9f678ab33e800a00f12a5c6e19b2994c5e6708cf51f1de9611bcfe440",
  ]
}

provider "registry.terraform.io/hetznercloud/hcloud" {
  version     = "1.31.0"
  constraints = "1.31.0"
  hashes = [
    "h1:Rq5+dfJ1pmj10IzDMF9kYb16rY3nZJlN1X4Rqhrm8YA=",
    "h1:shbFAm28wU02uVDJTjeLMvdAQsKVvVT/J5yBjE3kTtI=",
    "zh:096d73e7cf6512473436a08c626fae239332db39b0c123865b8a5f3f1d56621a",
    "zh:1ecbddbba5e49738003d775a0278c06ed0dddc86dbd07295b3f44d19e094eded",
    "zh:25c7e62862deea7d7e34a0470ff13e689e8783f96af90f489b4c8654c54e9090",
    "zh:2cf9619ee89a9efcf0d0f5853aff7cc84f39ad11bd9c8396b2b689e098b37cc6",
    "zh:3d48783fd9a59369b26c6cc0247f39838146a776adc93f3f48be71cae21c98dc",
    "zh:5e8ac869bc4290afb84f1165599d17b10a87d48d14c59b83bc25910accf92fa3",
    "zh:5fe75c40699050b07ee0d22e82449830975a8fed5f1eab24c6540a43368a4569",
    "zh:7504aa082259fbf5cfff95793c7d6bcae4254435dc51842faae5a81731b621a5",
    "zh:8fa26eb700450f582eb1a9c7723a8fc6cb80f99d981e6db57021d27171c5c83d",
    "zh:9996cbc655f5a25439ef2240de6ba6b89e8625ff7520f0b439f157b6dc612443",
    "zh:ad91f6c1df70d656f25620ecf2d66218d4d3e0c3ca45177e8019e0b8e94d2ed4",
    "zh:b0ce15a4cdf24599008022cbcbbd20f873412cdaa252e8bf9856d31f97cffdb6",
    "zh:ba0f20dde35a7e79f9e2eb8c88bc5d4dd2a98bb1d3b5dc5992e76abe6b2f695f",
    "zh:bd02fd6e554f2281e6e4fbbb41e11d5cd77c67319af3ec0782d16464f40104ac",
  ]
}

provider "registry.terraform.io/jianyuan/sentry" {
  version     = "0.6.0"
  constraints = "0.6.0"
  hashes = [
    "h1:IjpYZ9ZApyVPyYcyY73wfEkZn93OmEQlczVAO4IEADo=",
    "h1:PGQg2+PoEnIJXKSVDZqsTvwDlbM/Fs717AXdjILc+Zw=",
    "zh:03d13e3c8a4b24ff6306b38d114ff91d0f27294cea41b83c159804cd147e4dce",
    "zh:2937e485d49b61840fe434f37a14ca4309dde87f0bcf8464b0b0ab86832c7289",
    "zh:2e2d78fa406eb39f5893b3c78488d59393702c7d1c5fee25c766052a8b3181c9",
    "zh:48e5c1340f99644636c3f31673677f54be038e9014d64b4f827d1a65f807ff02",
    "zh:6a048557bbf32e8da051d0bbc9d429b295f72d71572323e511f55e3589c5acfe",
    "zh:7720b2304fae82e6fc8b352d282b088369fa6b914220f2d11b92b0f8b6e776b4",
    "zh:853372a035b6b680a26135657dda3eb3c0589f6b5405a4d957d55d2d3b54b2e8",
    "zh:89582701df3da0d9b13c079b48134e819976f61bd37ef5b21b5a0718630253b7",
    "zh:9be65e82bf5feeadd4e6dac282bc89ee5259e510da1c5beaa2472875551ad36b",
    "zh:a0fd270c75fc25aa7c0d81d2ed09963bb9f5ef97cc0e6af78e15023c2440a1e9",
    "zh:a50a6a1bd5b92859378c80dff1e58876460511f7413faa2fb08269078b10bac0",
    "zh:b143fc51028278475b2c030331346c24a31f3bd37fea5f562bb0b9576217a2de",
    "zh:c37a1964370fd3deec1ad20927dc0b86e8dfd13e49e57761df98b28cfc6b5d87",
    "zh:f8514bafc5d16de06fb7f9b91ccacc7e5f6f974bc2ed4cf3d3db8c47ebc68bd2",
  ]
}

provider "registry.terraform.io/paultyng/unifi" {
  version     = "0.39.0"
  constraints = "0.39.0"
  hashes = [
    "h1:1PTumXroFry/Zz9RI7szbZwN7wa/rex+ffjoim2HsvU=",
    "zh:32b784a77021eb789ff1c25c9845bf78e479729447ecdc4d0ca646e12bc62394",
    "zh:356fa769d80dac4e05f455f8adc8e6490457c35a2f814b7fc19c8b389f0ed23c",
    "zh:5e230873f2075cee957d705db824f3d15b94dfa95fb08ea39e53c9645e815c50",
    "zh:83ddbb485f93cfd71350d5fe95275f97ce680ae5586054e8d40aed59864e4bfa",
    "zh:950128885522285471a5bb7bdc78a08b1dca48aad163472e41327fa061a60b6d",
    "zh:9fc3aa4b25ee7376c634dce554a49b275d589eabd6a7211f284ac08cd15f9a2c",
    "zh:ae26ec75bd2c977859ad651220eb6e6800ba5eeb8bed89af1d338a34cbe91547",
    "zh:b264c6b518f17a0e6178a3578705a1c0eab3cdf6f55ea8895f17130c06e29cdf",
    "zh:bea770d1640612d54cb8dadc23a529a31dfed3af924be03a9677f163d1a204a6",
    "zh:befb1fb37a731e54fed87b7075115793688667f5166cd7856a0cb2cb21c4b825",
    "zh:c0f687d245f4be2f1eac78fc9b20a713271ea2b821e961e2cd45e4db5c7fae8d",
    "zh:c38c0ac81248aa92de70c5037969660ea15391da6ed9a40aad77036a627a6c68",
    "zh:dd718e79b2ef8b25314e94ab01f3f8de5dd38e08f65e67d33e357167d8ea746c",
    "zh:e84cffc25404af668e6b023bec3941f20eb6f1380e4143a8c57019fe55b052bc",
  ]
}
