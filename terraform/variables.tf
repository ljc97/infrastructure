variable "cloudflare_api_token" {
  type      = string
  sensitive = true
}

variable "cloudflare_email" {
  type      = string
  sensitive = true
}

variable "do_token" {
  type      = string
  sensitive = true
}

variable "hcloud_token" {
  type      = string
  sensitive = true
}

variable "sentry_token" {
  type      = string
  sensitive = true
}

variable "oci_region" {
  type      = string
  sensitive = true
}
variable "oci_tenancy_ocid" {
  type      = string
  sensitive = true
}
variable "oci_compartment_ocid" {
  type      = string
  sensitive = true
}
variable "oci_user_ocid" {
  type      = string
  sensitive = true
}
variable "oci_token" {
  type      = string
  sensitive = true
}

variable "unifi_username" {
  type      = string
  sensitive = true
}

variable "unifi_password" {
  type      = string
  sensitive = true
}

variable "unifi_api_url" {
  type      = string
  sensitive = true
}

variable "unifi_wlan1_name" {
  type      = string
  sensitive = false
}

variable "unifi_wlan1_password" {
  type      = string
  sensitive = true
}

variable "unifi_wlan2_name" {
  type      = string
  sensitive = false
}

variable "unifi_wlan2_password" {
  type      = string
  sensitive = true
}

variable "unifi_wlan3_name" {
  type      = string
  sensitive = false
}

variable "unifi_wlan3_password" {
  type      = string
  sensitive = true
}

variable "better_uptime_api_key" {
  type      = string
  sensitive = true
}

variable "cloudflare_google_oauth_id" {
  type      = string
  sensitive = true
}
variable "cloudflare_google_oauth_secret" {
  type      = string
  sensitive = true
}

variable "wan_ip" {
  type      = string
  sensitive = true
}
