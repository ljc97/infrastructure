provider "sentry" {
  token = var.sentry_token
}

resource "sentry_organization" "default" {
  name        = "infra-org"
  slug        = "infra-org"
  agree_terms = true
}

resource "sentry_team" "default" {
  organization = sentry_organization.default.slug
  name         = "team"
  slug         = "team"
}
