provider "hcloud" {
  token = var.hcloud_token
}

resource "hcloud_ssh_key" "infra" {
  name       = "Infra"
  public_key = file("../keys/id_rsa.pub")
}

resource "hcloud_firewall" "default-fw" {
  name = "default-fw"

  rule {
    direction = "in"
    protocol  = "tcp"
    port      = "22"
    source_ips = [
      "0.0.0.0/0",
      "::/0"
    ]
  }
}

resource "hcloud_network" "default-net" {
  name     = "default-net"
  ip_range = "10.32.64.0/24"
}

resource "hcloud_network_subnet" "default-subnet" {
  network_id   = hcloud_network.default-net.id
  type         = "cloud"
  network_zone = "eu-central"
  ip_range     = "10.32.64.0/24"
}
