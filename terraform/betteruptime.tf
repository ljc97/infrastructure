provider "better-uptime" {
  api_token = var.better_uptime_api_key
}

resource "betteruptime_status_page" "arietis" {
  provider = better-uptime

  company_name = "Arietis"
  subdomain    = "arietis"
  company_url  = "https://arietis.uk"
  timezone     = "London"

  custom_domain = "status.arietis.uk"
  subscribable  = false
}

resource "betteruptime_status_page_section" "main" {
  provider = better-uptime

  status_page_id = betteruptime_status_page.arietis.id

  name = "Current Status"
}

resource "betteruptime_status_page_resource" "unifi" {
  provider = better-uptime

  public_name   = "Unifi"
  resource_id   = betteruptime_monitor.unifi.id
  resource_type = "Monitor"
  history       = true

  status_page_id         = betteruptime_status_page.arietis.id
  status_page_section_id = betteruptime_status_page_section.main.id
}
