resource "sentry_project" "eurostar-scraper" {
  organization = sentry_organization.default.slug
  team         = sentry_team.default.slug
  name         = "eurostar-scraper"
  slug         = "eurostar-scraper"
  platform     = "python"
  resolve_age  = 720
}

resource "sentry_rule" "eurostar" {
  name         = "eurostar-rule"
  organization = sentry_organization.default.slug
  project      = sentry_project.eurostar-scraper.slug
  action_match = "any"
  frequency    = 30

  conditions = [
    {
      id   = "sentry.rules.conditions.first_seen_event.FirstSeenEventCondition"
      name = "A new issue is created"
    }
  ]

  filters = [
    {
      id         = "sentry.rules.filters.assigned_to.AssignedToFilter"
      targetType = "Unassigned"
    }
  ]

  actions = [
    {
      id               = "sentry.mail.actions.NotifyEmailAction"
      name             = "Send an email to IssueOwners"
      targetIdentifier = ""
      targetType       = "IssueOwners"
    }
  ]
}