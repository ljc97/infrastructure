terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/19002352/terraform/state/tf-state"
    lock_address   = "https://gitlab.com/api/v4/projects/19002352/terraform/state/tf-state/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/19002352/terraform/state/tf-state/lock"
    username       = "ljc97"
    lock_method    = "POST"
    unlock_method  = "DELETE"
  }
  required_providers {
    better-uptime = {
      source  = "BetterStackHQ/better-uptime"
      version = "0.3.5"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "2.26.1"
    }
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.11.1"
    }
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "1.31.0"
    }
    oci = {
      source  = "hashicorp/oci"
      version = "4.45.0"
    }
    sentry = {
      source  = "jianyuan/sentry"
      version = "0.6.0"
    }
    unifi = {
      source  = "paultyng/unifi"
      version = "0.39.0"
    }
  }
}
