provider "cloudflare" {
  email     = var.cloudflare_email
  api_token = var.cloudflare_api_token
}

resource "cloudflare_zone" "arietis" {
  zone   = "arietis.uk"
  paused = false
  type   = "full"
}

resource "cloudflare_record" "spf_arietis" {
  zone_id = cloudflare_zone.arietis.id
  name    = "arietis.uk"
  value   = "v=spf1 -all"
  type    = "TXT"
  proxied = false
}

resource "cloudflare_record" "spf_dmarc" {
  zone_id = cloudflare_zone.arietis.id
  name    = "_dmarc"
  value   = "v=DMARC1; p=reject; sp=reject; adkim=s; aspf=s;"
  type    = "TXT"
  proxied = false
}

resource "cloudflare_record" "spf_domainkey" {
  zone_id = cloudflare_zone.arietis.id
  name    = "*._domainkey"
  value   = "v=DKIM1; p="
  type    = "TXT"
  proxied = false
}

resource "cloudflare_record" "txt_google" {
  zone_id = cloudflare_zone.arietis.id
  name    = "@"
  value   = "google-site-verification=IPBRjG8H85ee0IWA2KxyFxnngyp_16ZPQrlkOs1QtBA"
  type    = "TXT"
}

resource "cloudflare_record" "mx_1" {
  zone_id  = cloudflare_zone.arietis.id
  name     = "@"
  priority = 1
  ttl      = 3600
  value    = "aspmx.l.google.com"
  type     = "MX"
}

resource "cloudflare_record" "mx_5" {
  zone_id  = cloudflare_zone.arietis.id
  name     = "@"
  priority = 5
  ttl      = 3600
  value    = "alt1.aspmx.l.google.com"
  type     = "MX"
}

resource "cloudflare_record" "mx_5_2" {
  zone_id  = cloudflare_zone.arietis.id
  name     = "@"
  priority = 5
  ttl      = 3600
  value    = "alt2.aspmx.l.google.com"
  type     = "MX"
}

resource "cloudflare_record" "mx_10" {
  zone_id  = cloudflare_zone.arietis.id
  name     = "@"
  priority = 10
  ttl      = 3600
  value    = "alt3.aspmx.l.google.com"
  type     = "MX"
}

resource "cloudflare_record" "mx_10_2" {
  zone_id  = cloudflare_zone.arietis.id
  name     = "@"
  priority = 10
  ttl      = 3600
  value    = "alt4.aspmx.l.google.com"
  type     = "MX"
}

# resource "cloudflare_access_identity_provider" "google" {
#   zone_id = cloudflare_zone.arietis.id
#   name    = "Google"
#   type    = "google-apps"
#   config {
#     client_id     = var.cloudflare_google_oauth_id
#     client_secret = var.cloudflare_google_oauth_secret
#     apps_domain = "arietis.uk"
#   }
# }

resource "cloudflare_record" "status" {
  zone_id         = cloudflare_zone.arietis.id
  name            = "status"
  ttl             = 3600
  proxied         = false
  allow_overwrite = false
  value           = "statuspage.betteruptime.com"
  type            = "CNAME"
}