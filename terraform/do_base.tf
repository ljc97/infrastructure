provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_ssh_key" "infra" {
  name       = "Infra"
  public_key = file("../keys/id_rsa.pub")
}

# resource "digitalocean_floating_ip" "nebula-lighthouse" {
#   region = "lon1"
# }

# resource "digitalocean_firewall" "ssh-lh" {
#   name = "ssh-lh"

#   droplet_ids = [digitalocean_droplet.nebula-lighthouse.id]

#   inbound_rule {
#     protocol         = "tcp"
#     port_range       = "22"
#     source_addresses = ["0.0.0.0/0", "::/0"]
#   }

#   inbound_rule {
#     protocol         = "udp"
#     port_range       = "4242"
#     source_addresses = ["0.0.0.0/0", "::/0"]
#   }
# }

resource "digitalocean_vpc" "default-vpc" {
  name     = "default-vpc"
  region   = "lon1"
  ip_range = "10.32.128.0/24"
}