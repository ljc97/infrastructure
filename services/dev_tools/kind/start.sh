#! /bin/bash
kind create cluster --config manifests/kind.yaml
kubectl cluster-info --context kind-kind
kind export kubeconfig >~/.kube/config

# Ingress
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml

# Helm Repos
helm repo add openfaas https://openfaas.github.io/faas-netes/
helm repo update

# Linkerd
linkerd install | kubectl apply -f -
linkerd jaeger install | kubectl apply -f -
linkerd viz install --set jaegerUrl=jaeger.linkerd-jaeger:16686 | kubectl apply -f -

# Openfaas
kubectl apply -f manifests/openfaas.yaml
kubectl create secret -n openfaas-fn generic regcred \
  --from-file=.dockerconfigjson=/home/lewis/.docker/config.json \
  --type=kubernetes.io/dockerconfigjson

helm upgrade openfaas --install openfaas/openfaas \
  --namespace openfaas \
  --set functionNamespace=openfaas-fn \
  --set generateBasicAuth=true

kubectl port-forward svc/gateway 8080:8080 >/dev/null 2>&1 &
linkerd viz dashboard >/dev/null 2>&1 &
linkerd jaeger dashboard >/dev/null 2>&1 &

OPENFAAS_PASSWORD=$(kubectl -n openfaas get secret basic-auth -o jsonpath="{.data.basic-auth-password}" | base64 --decode)
export OPENFAAS_PASSWORD
export OPENFAAS_URL=http://127.0.0.1:8080

# emojivoto
# linkerd inject https://run.linkerd.io/emojivoto.yml | kubectl apply -f -
# kubectl -n emojivoto set env --all deploy OC_AGENT_HOST=collector.linkerd-jaeger:55678

# spinnaker
# kubectl apply -f manifests/spinnaker.yaml
# mkdir -p spinnaker-operator && cd spinnaker-operator
# curl -L https://github.com/armory/spinnaker-operator/releases/latest/download/manifests.tgz | tar -xz
# kubectl apply -f deploy/crds/
# kubectl -n spinnaker-operator apply -f deploy/operator/cluster
# kubectl -n spinnaker apply -f deploy/spinnaker/basic
# cd ../
# rm -rf spinnaker-operator
