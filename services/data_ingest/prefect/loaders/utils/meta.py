import traceback
from dataclasses import dataclass, field
from datetime import datetime
from enum import Enum
from typing import Any, Dict, Optional, Tuple

import pandas as pd


def _construct_meta_symbol(data: Any) -> str:
    return f"{data._sym_prefix}_{data.run_time}:{';'.join(f'{k}/{v}' for k,v in data.additional_keys.items())};"


@dataclass(eq=False)
class LoaderSnapshot:
    symbol: str = field(init=False)
    df: Optional[pd.DataFrame]
    run_time: datetime
    additional_keys: Dict[str, str] = field(default_factory=dict)
    metadata: Optional[Dict[str, Any]] = None

    _sym_prefix = "_snapshot"

    def __post_init__(self) -> None:
        self.symbol = _construct_meta_symbol(self)
        self.additional_keys = {} if self.additional_keys is None else self.additional_keys

    @staticmethod
    def _unpack(data: Any) -> pd.DataFrame:
        assert isinstance(data, pd.DataFrame)
        return data

    @staticmethod
    def _pack(df: pd.DataFrame) -> Any:
        return df

    def __eq__(self, other: object) -> bool:  # TODO: generalized comparison allowing arb. nested df
        if not isinstance(other, type(self)):
            return False
        if self.df is None != other.df is None:
            return False
        return all(
            [
                self.symbol == other.symbol,
                (
                    ((self.df.empty and other.df.empty) or self.df.equals(other.df))
                    if self.df is not None and other.df is not None
                    else self.df == other.df
                ),
                self.run_time == other.run_time,
                self.additional_keys == other.additional_keys,
                self.metadata == other.metadata,
            ]
        )


@dataclass(eq=False)
class LoaderException(Exception):
    symbol: str = field(init=False)
    payload: Any
    exception: Optional[
        Exception
    ]  # input should be !none ideally, will be returned as None in favour of str representation
    traceback: str = field(init=False)
    run_time: datetime
    additional_keys: Dict[str, str] = field(default_factory=dict)
    metadata: Optional[Dict[str, Any]] = None

    _sym_prefix = "_exception"

    def __post_init__(self) -> None:
        self.symbol = self.symbol = _construct_meta_symbol(self)
        self.additional_keys = {} if self.additional_keys is None else self.additional_keys
        # aiming to avoid lib specific errors and pickling sadness
        self.traceback = "".join(traceback.format_exception(self.exception)) if self.exception is not None else ""

    @staticmethod
    def _unpack(data: Any) -> Tuple[Any, Any]:
        return (data[0], data[1])

    @staticmethod
    def _pack(payload: Any, exception: Any) -> Any:
        return (payload, exception)

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, type(self)):
            return False
        if self.exception is None != other.exception is None:
            return False
        return all(
            [
                self.symbol == other.symbol,
                self.payload == other.payload,
                (
                    type(self.exception) is type(other.exception)  # noqa: E721
                    and self.exception.args == other.exception.args
                    if self.exception is not None and other.exception is not None
                    else self.exception == other.exception
                ),
                self.traceback == other.traceback,
                self.run_time == other.run_time,
                self.additional_keys == other.additional_keys,
                self.metadata == other.metadata,
            ]
        )


class LoaderMetaType(Enum):
    SNAPSHOT = ("SNAPSHOT", LoaderSnapshot._sym_prefix, LoaderSnapshot)
    EXCEPTION = ("EXCEPTION", LoaderException._sym_prefix, LoaderException)


class LoaderError(Exception):
    def __init__(self, message: str, loader_exception: LoaderException):
        super().__init__(message)
        self.loader_exception = loader_exception
