import asyncio
import logging

from prefect.blocks.system import Secret
from prefect.client.orchestration import PrefectClient
from prefect.client.schemas.objects import Variable
from prefect.exceptions import ObjectNotFound

log = logging.getLogger("prefect-admin")
log.setLevel(logging.INFO)


SERVICES = ["xkcd", "lccwalls", "eurostar", "companieshouse", "bbcsport", "preowned", "nationalgrid"]
CONCURRENCY_LIMITS = [
    ("eurostar_max_calls", 15),
    ("arctic_parallel_writes", 15),
    ("arctic_parallel_defragment", 15),
    ("bbcsport_max_calls", 10),
    ("preowned_max_calls", 5),
    ("national_grid_max_calls", 5),
]
PREFECT_ENVS = [("prod", "http://prefect.media.arietis.uk/api")]


EXPECTED_VARIABLES = [
    (lambda ctx: f"{ctx['service']}_environment", lambda ctx: ctx["environment"]),
    (lambda ctx: f"{ctx['service']}_pyroscope_url", lambda _: "http://media.arietis.uk:4040"),
    (lambda ctx: f"{ctx['service']}_sentry_profiling", lambda _: "False"),
    (lambda ctx: f"{ctx['service']}_sentry_stacktraces", lambda _: "True"),
    (lambda ctx: f"{ctx['service']}_appname", lambda ctx: ctx["service"]),
    (lambda ctx: f"{ctx['service']}_arctic_lib", lambda ctx: ctx["service"]),
]
EXPECTED_SECRETS = [lambda ctx: f"{ctx['service']}-arctic-url", lambda ctx: f"{ctx['service']}-sentry-dsn"]


class SuperPrefectClient(PrefectClient):
    def __init__(self, *args, **kwargs):  # type: ignore
        super().__init__(*args, **kwargs)

    async def create_variable_by_name(self, variable: Variable) -> None:
        await self._client.post(
            "/variables/", json={"name": variable.name, "value": variable.value, "tags": variable.tags}
        )


async def main() -> None:
    for env, url in PREFECT_ENVS:
        client = SuperPrefectClient(url)
        for service in SERVICES:
            log.info("Processing %s in %s", service, env)
            ctx = {
                "service": service,
                "environment": env,
            }
            for path_fn, var_fn in EXPECTED_VARIABLES:
                path = path_fn(ctx)
                expected = var_fn(ctx)
                actual = await client.read_variable_by_name(path)
                if actual is None:
                    log.info("Creating environment var %s with value %s", path, expected)
                    await client.create_variable_by_name(Variable(name=path, value=expected))
                elif actual.value != expected:
                    log.info("Updating environment var %s with value %s to %s", path, actual, expected)
                    await client.delete_variable_by_name(path)
                    await client.create_variable_by_name(Variable(name=path, value=expected))
            for path_fn in EXPECTED_SECRETS:
                path = path_fn(ctx)
                try:
                    # TODO: use actual client to load block
                    from prefect.settings import PREFECT_API_URL, temporary_settings

                    with temporary_settings(updates={PREFECT_API_URL: url}):
                        actual = await Secret.load(path)
                except ValueError:
                    actual = None
                if actual is None:
                    log.warning("Secret does not exist at path %s", path)
        for path, expected in CONCURRENCY_LIMITS:
            try:
                actual = await client.read_concurrency_limit_by_tag(path)
            except (ValueError, ObjectNotFound):
                actual = None
            if actual is None:
                log.info("Creating concurrency limit %s with value %s", path, expected)
                await client.create_concurrency_limit(path, expected)
            elif actual.concurrency_limit != expected:  # type: ignore
                log.info("Updating concurrency limit %s with value %s to %s", path, actual, expected)
                await client.delete_concurrency_limit_by_tag(path)
                await client.create_concurrency_limit(path, expected)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
