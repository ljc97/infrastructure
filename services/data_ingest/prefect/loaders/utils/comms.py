import requests
from prefect import flow, get_run_logger
from prefect.blocks.system import Secret


@flow
def submit_slack_webhook(channel_id: str, title: str, body: str) -> None:
    logger = get_run_logger()
    logger.info("Submitting webhook for %s, with content (%s, %s)", channel_id, title, body)
    channel_hook = Secret.load("-".join([channel_id, "slack"])).get()

    payload = {
        "text": title,
        "blocks": [
            {"type": "section", "text": {"type": "mrkdwn", "text": body}},
        ],
    }
    logger.info("Submitting payload to slack")
    logger.info(payload)
    requests.post(url=channel_hook, json=payload)


@flow
def submit_discord_webhook(channel_id: str, title: str, body: str) -> None:
    logger = get_run_logger()
    logger.info("Submitting webhook for %s, with content (%s, %s)", channel_id, title, body)
