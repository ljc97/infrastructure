from datetime import date, datetime, timedelta
from typing import Any, Dict, Generator, List, Optional, Tuple, Union

import pandas as pd
from arcticdb.version_store.library import (  # type: ignore
    Library,
    ReadRequest,
    WritePayload,
)
from prefect import get_run_logger, task

from loaders.utils.meta import LoaderException, LoaderMetaType, LoaderSnapshot


# TODO: some sort of hang in this function
@task(tags=["arctic_parallel_defragment"], timeout_seconds=60.0, retries=3, retry_delay_seconds=5)
def _defragment_symbol(lib: Library, symbol: str) -> None:
    get_run_logger().info("Defragging %s", symbol)
    lib.defragment_symbol_data(symbol)


def defragment_symbol(lib: Library, symbol: str) -> None:
    if lib.is_symbol_fragmented(symbol):
        _defragment_symbol.submit(lib, symbol)


# TODO: some sort of hang in this function
@task(tags=["arctic_parallel_writes"], timeout_seconds=1800.0, retries=3, retry_delay_seconds=30)
def write_spliced_symbols(
    lib: Library,
    symbol_df_pairs: List[Tuple[str, pd.DataFrame]],
    dedupe: bool = False,
    overwrite: bool = False,
    symbol_list: Optional[List[str]] = None,
) -> None:
    symbol_list = symbol_list or lib.list_symbols()
    writes = []
    updates = []
    appends = []
    # TODO: split symbols first to build lists

    if dedupe:
        dedupe_data = lib.read_batch(
            symbols=[ReadRequest(symbol, date_range=(min(df.index),)) for symbol, df in symbol_df_pairs]
        )
    else:
        dedupe_data = None

    for idx, (symbol, df) in enumerate(symbol_df_pairs):
        if overwrite or symbol not in symbol_list:
            writes.append(WritePayload(symbol, df))
        elif dedupe:
            existing_df = dedupe_data[idx].data
            if len(existing_df) > 0:
                df = (
                    pd.concat(
                        [df, existing_df[(existing_df.index >= min(df.index)) & (existing_df.index <= max(df.index))]]
                    )
                    .sort_index()
                    .drop_duplicates()
                )
                updates.append((symbol, df))
            else:
                appends.append(WritePayload(symbol, df))
        else:
            last_existing_dt = max(lib.tail(symbol, n=1).data.index)
            if min(df.index) > last_existing_dt:
                appends.append(WritePayload(symbol, df))
            else:
                updates.append((symbol, df))
    lib.write_batch(writes)
    lib.append_batch(appends)
    for symbol, df in updates:
        lib.update(symbol, df)


def _form_highwater_symbol(path: str) -> str:
    return f"_meta_highwater_dt_{path}"


@task
def set_highwater_dt(lib: Library, path: str, dt: Union[datetime, date]) -> None:
    lib.write_pickle(_form_highwater_symbol(path), dt)


@task
def get_highwater_dt(lib: Library, path: str) -> Optional[Union[datetime, date]]:
    symbol = _form_highwater_symbol(path)
    if symbol not in lib.list_symbols():
        return None
    dt = lib.read(symbol).data
    assert isinstance(dt, datetime) or isinstance(dt, date)
    return dt


def write_snapshot(lib: Library, snapshot: LoaderSnapshot) -> None:
    assert snapshot.df is not None
    assert snapshot.symbol is not None
    metadata: Dict[str, Any] = {}
    metadata["_created_dt"] = datetime.utcnow()
    metadata["_run_time"] = snapshot.run_time
    metadata["_additional_keys"] = snapshot.additional_keys
    if snapshot.metadata is not None:
        metadata["_metadata"] = snapshot.metadata

    lib.write(snapshot.symbol, snapshot.df, metadata=metadata)


def retrieve_snapshots(lib: Library, thin: bool = True) -> Generator[LoaderSnapshot, None, None]:
    snap_symbols = [s for s in lib.list_symbols() if s.startswith(LoaderMetaType.SNAPSHOT.value[1])]
    if thin:
        metadata_list = lib.read_metadata_batch(snap_symbols)
        for metadata in metadata_list:
            snap = LoaderSnapshot(
                None,
                metadata.metadata["_run_time"],
                metadata.metadata["_additional_keys"],
                metadata.metadata.get("_metadata", None),
            )
            snap.symbol = metadata.symbol
            yield snap
    else:
        # no batch, maybe memory heavy
        for symbol in snap_symbols:
            result = lib.read(symbol)
            snap = LoaderSnapshot(
                result.data,
                result.metadata["_run_time"],
                result.metadata["_additional_keys"],
                result.metadata.get("_metadata", None),
            )
            snap.symbol = symbol
            yield snap


def cleanup_meta_symbols(lib: Library, max_age: timedelta, target: LoaderMetaType) -> None:
    items: Generator[Union[LoaderException, LoaderSnapshot], None, None]
    match target:
        case LoaderMetaType.SNAPSHOT:
            items = retrieve_snapshots(lib, thin=True)
        case LoaderMetaType.EXCEPTION:
            items = retrieve_exceptions(lib, thin=True)
        case _:
            raise ValueError()

    dt_threshold = datetime.utcnow() - max_age
    # batch delete?
    # aged_out = [itm for itm in items if itm.run_time < dt_threshold]
    for itm in items:
        if itm.run_time > dt_threshold:
            continue
        lib.delete(itm.symbol)


def write_exception(lib: Library, exception: LoaderException) -> None:
    assert exception.payload is not None
    assert isinstance(exception.exception, Exception)
    assert exception.symbol is not None
    metadata: Dict[str, Any] = {}
    metadata["_created_dt"] = datetime.utcnow()
    metadata["_run_time"] = exception.run_time
    metadata["_additional_keys"] = exception.additional_keys
    if exception.metadata is not None:
        metadata["_metadata"] = exception.metadata

    lib.write_pickle(exception.symbol, (exception.payload, exception.traceback), metadata=metadata)


def retrieve_exceptions(lib: Library, thin: bool = True) -> Generator[LoaderException, None, None]:
    snap_symbols = [s for s in lib.list_symbols() if s.startswith(LoaderMetaType.EXCEPTION.value[1])]
    if thin:
        metadata_list = lib.read_metadata_batch(snap_symbols)
        for metadata in metadata_list:
            exc = LoaderException(
                None,
                None,
                metadata.metadata["_run_time"],
                metadata.metadata["_additional_keys"],
                metadata.metadata.get("_metadata", None),
            )
            exc.symbol = metadata.symbol
            yield exc
    else:
        # no batch, maybe memory heavy
        for symbol in snap_symbols:
            result = lib.read(symbol)
            data, traceback = result.data
            exc = LoaderException(
                data,
                None,
                result.metadata["_run_time"],
                result.metadata["_additional_keys"],
                result.metadata.get("_metadata", None),
            )
            exc.traceback = traceback
            exc.symbol = symbol
            yield exc
