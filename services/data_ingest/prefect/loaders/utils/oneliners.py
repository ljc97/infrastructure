from typing import Any, Generator, List


def chunks(items: List[Any], n: int) -> Generator[List[Any], None, None]:
    assert n > 0
    for i in range(0, len(items), n):
        yield items[i : i + n]
