import json
import logging
from dataclasses import dataclass
from datetime import date, datetime, timedelta
from typing import Any, Dict, List, Optional

import pandas as pd
import requests
from arcticdb.version_store.library import (  # type: ignore
    DataError,
    Library,
    QueryBuilder,
)
from core_python.base_app import AppConfig, setup_app
from dateutil.relativedelta import relativedelta
from prefect import flow, get_run_logger, task
from prefect.task_runners import ConcurrentTaskRunner
from pytz import timezone

from loaders.utils.arctic import (
    defragment_symbol,
    get_highwater_dt,
    set_highwater_dt,
    write_exception,
    write_spliced_symbols,
)
from loaders.utils.comms import submit_slack_webhook
from loaders.utils.meta import LoaderException

logging.basicConfig(level=logging.INFO)

URLs = [
    (
        "London",
        "https://portal.rockgympro.com/portal/public/a67951f8b19504c3fd14ef92ef27454d/occupancy",
    ),
    (
        "Lakeland",
        "https://portal.rockgympro.com/portal/public/c770b48889885c4843912a6bea432680/occupancy",
    ),
]

NAME_OVERRIDES = {("LNW", "Current"): "LancasterWall"}
NOTIFY_HIGHWATER_KEY = "last_notify"


@dataclass
class ClimbingWall:
    ts: datetime
    name: str
    current: int
    capacity: int


def _extract_json_from_html(text: str) -> Dict[str, Any]:
    s_idx = text.find("var data = ") + len("var data = ")
    e_idx = text[s_idx:].find("};") + s_idx
    text = text[s_idx:e_idx].replace("'", '"').strip()
    text = text[:-1] + "}"
    data = json.loads(text)
    assert isinstance(data, dict)
    return data


def _extract_last_update(text: str) -> Optional[datetime]:
    if not ("(" in text and ")" in text):
        return None
    last_str = text[text.find("(") + 1 : text.find(")")]
    hour, minute = [int(substr) for substr in last_str[:-3].split(":")]
    if last_str.endswith("PM") and hour < 12:
        hour += 12
    today = date.today()
    yesterday = today - timedelta(days=1)

    uk_tz = timezone("Europe/London")
    utc = timezone("UTC")

    dt = (
        uk_tz.localize(
            datetime(
                today.year,
                today.month,
                today.day,
                hour,
                minute,
                0,
            )
        )
        .astimezone(utc)
        .replace(tzinfo=None)
    )
    if dt > datetime.utcnow() + timedelta(minutes=5):
        return (
            uk_tz.localize(datetime(yesterday.year, yesterday.month, yesterday.day, hour, minute, 0))
            .astimezone(utc)
            .replace(tzinfo=None)
        )
    return dt


def _store_walls(lib: Library, chain: str, walls: List[ClimbingWall]) -> List[str]:
    df = pd.DataFrame(
        {
            "ts": [wall.ts for wall in walls],
            "name": [wall.name for wall in walls],
            "current": [wall.current for wall in walls],
            "capacity": [wall.capacity for wall in walls],
        }
    )
    df = df.set_index("ts").sort_index()

    snap_symbol = f"snapshot-{chain}-{datetime.utcnow()}-{min(df.index)}-{max(df.index)}"
    symbols = [snap_symbol]
    symbol_dfs = [(snap_symbol, df)]
    for name in {wall.name for wall in walls}:
        symbol = f"{chain}.{name}"
        symbols.append(symbol)
        symbol_dfs.append((symbol, df[df.name == name]))
    write_spliced_symbols(lib, symbol_dfs, dedupe=True)
    return symbols


@task
def _fetch_center(url: str) -> List[ClimbingWall]:
    logger = get_run_logger()
    resp = requests.get(url)
    data = _extract_json_from_html(resp.text)
    logger.info("Parsing Response")
    walls = []
    for gym_code, d in data.items():
        name = d["subLabel"].split(" ")[0]
        name = NAME_OVERRIDES.get((gym_code, name), name)
        last_updated = _extract_last_update(d["lastUpdate"])
        if not last_updated:
            logger.warning("Discarding update missing time for %s (%s)", name, d["lastUpdate"])
            continue
        walls.append(
            ClimbingWall(
                last_updated,
                name,
                int(d["count"]),
                int(d["capacity"]),
            )
        )
    logger.info(
        "Pulled stats for %s wall(s) - [%s]",
        len(walls),
        ",".join([wall.name for wall in walls]),
    )
    return walls


@task(retries=5, retry_delay_seconds=5)
def _load_latest_snapshot(lib: Library) -> List[ClimbingWall]:
    walls = []
    # not a CENTER.WALL symbol
    symbols = [symbol for symbol in lib.list_symbols() if "." in symbol and "snapshot" not in symbol]
    lookback = datetime.utcnow() - relativedelta(minutes=35)
    data = lib.read_batch(symbols, QueryBuilder().date_range((lookback, None)))
    for result in data:
        if isinstance(result, DataError):
            exc = LoaderException(data, ValueError("DataError in batch_read()"), datetime.utcnow())
            write_exception(lib, exc)
            get_run_logger().warning(exc)
            raise exc
        if result.data is None:
            continue
        for idx, row in result.data.tail(1).iterrows():
            walls.append(ClimbingWall(idx.to_pydatetime(), row["name"], row["current"], row["capacity"]))
            break
    walls.sort(key=lambda wall: (wall.name, wall.ts))
    return walls


@flow(task_runner=ConcurrentTaskRunner())
def fetch_lcc_walls() -> None:
    _, _, lib = setup_app("lccwalls", AppConfig, use_prefect=True)
    assert lib is not None

    logger = get_run_logger()

    logger.info("Loading Wall Stats")
    stats = [(center, _fetch_center.submit(url)) for center, url in URLs]
    for chain, walls in stats:
        logger.info("Storing retrieved data for %s", chain)
        symbols = _store_walls(lib, chain, walls.result())
        logger.info(lib.tail(next(s for s in symbols), n=5).data.tail(5))
        logger.info("Degrafmenting symbols if required (%s)", symbols)
        assert symbols is not None
        for symbol in symbols:
            defragment_symbol(lib, symbol)


@flow
def notify_lcc_walls() -> None:
    _, _, lib = setup_app("lccwalls", AppConfig, use_prefect=True)
    assert lib is not None

    logger = get_run_logger()

    highwater = get_highwater_dt(lib, NOTIFY_HIGHWATER_KEY)
    if highwater is not None and highwater > datetime.utcnow() - timedelta(minutes=10):
        logger.info("Skipping notify as running too frequently")
        return

    logger.info("Dispatching summary of occupancy to slack")
    walls = _load_latest_snapshot(lib)
    logger.info("Retrieved wall data")
    logger.info(walls)
    assert walls is not None
    if len(walls) > 0:
        submit_slack_webhook(
            channel_id="lccwalls",
            title="LCC Gym Status",
            body="Gym Status:\n"
            + "\n".join([f"\t{wall.name}: {wall.current}/{wall.capacity} @ {wall.ts.isoformat()}" for wall in walls]),
        )
        set_highwater_dt(lib, NOTIFY_HIGHWATER_KEY, datetime.utcnow())


if __name__ == "__main__":
    fetch_lcc_walls()
    # notify_lcc_walls()
