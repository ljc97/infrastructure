import httpx
from prefect import flow
from prefect.blocks.system import Secret


@flow(retries=5, retry_delay_seconds=5)
def get_request(secret_path: str) -> None:
    res = httpx.get(Secret.load(secret_path).get(), timeout=10.0)
    assert 200 <= res.status_code < 400


if __name__ == "__main__":
    get_request("heartbeat-hourly")
