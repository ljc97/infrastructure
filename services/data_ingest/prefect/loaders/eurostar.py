import logging
from collections import defaultdict
from dataclasses import asdict, dataclass, field
from datetime import date, datetime, timedelta
from enum import Enum
from itertools import chain
from json import dumps
from typing import Any, Dict, Iterator, List, Optional, Set, Tuple

import httpx
import pandas as pd
from arcticdb.version_store.library import Library  # type: ignore
from core_python.base_app import AppConfig, setup_app
from dateutil.parser import parse
from prefect import flow, get_run_logger, task
from prefect.futures import PrefectFuture
from prefect.task_runners import ConcurrentTaskRunner

from loaders.utils.arctic import (
    defragment_symbol,
    write_exception,
    write_spliced_symbols,
)
from loaders.utils.meta import LoaderException
from loaders.utils.oneliners import chunks

logging.basicConfig(level=logging.INFO)

PROD_URL = "https://api.prod.eurostar.com/gateway"


@dataclass
class APIRequest:
    operationName: str
    query: str
    variables: Dict[str, Any]

    def as_request(self) -> Dict[str, Any]:
        return {
            "operationName": self.operationName,
            "query": self.query,
            "variables": self.variables,
        }


@dataclass
class Station:
    name: str
    city: str
    uic: str


class Stations(Enum):
    LONDON = Station("London St Pancras Int'l", "London", "7015400")
    PARIS = Station("Paris Gare du Nord", "Paris", "8727100")
    AMSTERDAM = Station("Amsterdam Central", "Amsterdam", "8400058")
    BRUSSELS = Station("Brussels Midi", "Brussels", "8814001")


UIC_TO_STATION = {s.value.uic: s for s in Stations}


def _vars_to_dict(variables: "NewBookingSearchVariables") -> Dict[str, Any]:
    data = asdict(variables)
    for key in data:
        if isinstance(data[key], date):
            data[key] = data[key].strftime("%Y-%m-%d")
        elif isinstance(data[key], Stations):
            data[key] = data[key].value.uic
        elif isinstance(data[key], Station):
            data[key] = data[key].uic
        elif isinstance(data[key], Dict) and "uic" in data[key]:
            data[key] = data[key]["uic"]  # flatten stations to uic
    return {k: v for k, v in data.items() if v is not None}


@dataclass
class NewBookingSearchVariables:
    origin: Station
    destination: Station
    outbound: date
    hasInbound: bool
    adult: int
    inbound: Optional[date] = None
    adults16Plus: int = 0
    adultsWheelchair: int = 0
    child: int = 0
    children4Only: int = 0
    children5Toll: int = 0
    contractCode: str = "EIL_ALL"
    currency: str = "GBP"
    filteredClassesOfAccommodation: List[str] = field(default_factory=lambda: ["STD", "SP", "BP"])
    guideDogs: int = 0
    infant: int = 0
    isAftersales: bool = False
    multipleFlexibility: bool = False
    nonWheelchairCompanions: int = 0
    productFamilies: List[str] = field(default_factory=lambda: ["PUB"])
    wheelchairCompanions: int = 0
    youth: int = 0


@dataclass
class NewBookingSearch(APIRequest):
    def __init__(self, variables: NewBookingSearchVariables):
        self.operationName = "NewBookingSearch"
        self.variables = _vars_to_dict(variables)
        self.query = r"""query NewBookingSearch($origin: String!, $destination: String!, $outbound: String!, $inbound: String, $productFamilies: [String] = ["PUB"], $contractCode: String = "EIL_ALL", $adult: Int, $child: Int, $infant: Int, $youth: Int, $adults16Plus: Int = 0, $children4Only: Int = 0, $children5To11: Int = 0, $adultsWheelchair: Int = 0, $childrenWheelchair: Int = 0, $guideDogs: Int = 0, $wheelchairCompanions: Int = 0, $nonWheelchairCompanions: Int = 0, $filteredClassesOfAccommodation: [ClassEnum], $currency: Currency!, $isAftersales: Boolean = false, $hasInbound: Boolean = false, $multipleFlexibility: Boolean = true) {
  journeySearch(
    outboundDate: $outbound
    inboundDate: $inbound
    origin: $origin
    destination: $destination
    adults: $adult
    productFamilies: $productFamilies
    contractCode: $contractCode
    adults16Plus: $adults16Plus
    adults12To15: $youth
    children: $child
    children4Only: $children4Only
    children5To11: $children5To11
    infants: $infant
    adultsWheelchair: $adultsWheelchair
    childrenWheelchair: $childrenWheelchair
    guideDogs: $guideDogs
    wheelchairCompanions: $wheelchairCompanions
    nonWheelchairCompanions: $nonWheelchairCompanions
    isAftersales: $isAftersales
    currency: $currency
    multipleFlexibility: $multipleFlexibility
  ) {
    outbound {
      ...searchBound
      __typename
    }
    inbound @include(if: $hasInbound) {
      ...searchBound
      __typename
    }
    __typename
  }
}

fragment searchBound on Offer {
  origin {
    name
    city
    uic
    __typename
  }
  destination {
    name
    city
    uic
    __typename
  }
  journeys(
    hideIndirectTrainsWhenDisruptedAndCancelled: true
    hideDepartedTrains: true
  ) {
    ...journey
    __typename
  }
  __typename
}

fragment journey on Journey {
  timing {
    date
    departureTime: departs
    arrivalTime: arrives
    duration
    __typename
  }
  fares(filteredClassesOfAccommodation: $filteredClassesOfAccommodation) {
    class {
      name
      code
      __typename
    }
    prices {
      displayPrice
      total
      child
      adult
      __typename
    }
    seats
    legs {
      products {
        passengerId
        passengerAgeGroup {
          type
          name
          __typename
        }
        isFareAddOn
        tariffCode
        bucketCode
        loyaltyPoints
        name
        code
        price
        __typename
      }
      origin {
        name
        uic
        __typename
      }
      destination {
        name
        uic
        __typename
      }
      serviceName
      serviceType {
        name
        code
        __typename
      }
      serviceIdentifier
      timing {
        date
        departureTime: departs
        arrivalTime: arrives
        duration
        isoDate
        __typename
      }
      __typename
    }
    __typename
  }
  __typename
}
"""


@dataclass
class FareClass:
    name: str
    code: str


class FareClasses(Enum):
    STANDARD = FareClass("Standard", "STD")
    STANDARD_PREMIER = FareClass("Standard Premier", "SP")
    BUSINESS_PREMIER = FareClass("Business Premier", "BP")


FARE_CODE_TO_CLASS = {fc.value.code: fc for fc in FareClasses}


@dataclass
class FareResponse:
    fareClass: FareClasses
    displayPrice: int
    total: int
    seats: int

    def __init__(self, data: Dict[str, Any]):
        self.fareClass = FARE_CODE_TO_CLASS[data["class"]["code"]]
        self.displayPrice = data["prices"]["displayPrice"]
        self.total = data["prices"]["total"]
        self.seats = data["seats"]


@dataclass
class TimingResponse:
    date: date
    departureTime: datetime
    arrivalTime: datetime
    duration: int

    def __init__(self, data: Dict[str, Any]):
        self.date = parse(data["date"])
        parts = data["departureTime"].split(":")
        self.departureTime = datetime(self.date.year, self.date.month, self.date.day, int(parts[0]), int(parts[1]))
        parts = data["arrivalTime"].split(":")
        self.arrivalTime = datetime(self.date.year, self.date.month, self.date.day, int(parts[0]), int(parts[1]))
        self.duration = int(data["duration"])


@dataclass
class NewJourneyOptionResponse:
    timing: TimingResponse
    fares: List[FareResponse]

    def __init__(self, data: Dict[str, Any]):
        self.timing = TimingResponse(data["timing"])
        self.fares = [FareResponse(fare) for fare in data["fares"]]


@dataclass
class NewJourneyLegResponse:
    origin: Stations
    destination: Stations
    journeys: List[NewJourneyOptionResponse]

    def __init__(self, data: Dict[str, Any]):
        self.origin = UIC_TO_STATION[data["origin"]["uic"]]
        self.destination = UIC_TO_STATION[data["destination"]["uic"]]
        self.journeys = [NewJourneyOptionResponse(journey) for journey in data["journeys"]]


@dataclass
class NewJourneySearchResponse:
    outbound: NewJourneyLegResponse
    inbound: Optional[NewJourneyLegResponse]
    fetched: datetime

    def __init__(self, data: Dict[str, Any]):
        payload = data["data"]["journeySearch"]
        self.outbound = NewJourneyLegResponse(payload["outbound"])
        self.inbound = NewJourneyLegResponse(payload["inbound"]) if "inbound" in payload else None
        self.fetched = datetime.utcnow()


@task(retries=2, retry_delay_seconds=5, tags=["eurostar_max_calls"])
def get_fares_for(
    lib: Library,
    adults: int,
    outbound: date,
    origin: Stations,
    destination: Stations,
    inbound: Optional[date] = None,
) -> Optional[NewJourneySearchResponse]:
    variables = NewBookingSearchVariables(
        origin=origin.value,
        destination=destination.value,
        outbound=outbound,
        adult=adults,
        hasInbound=False,
    )
    if inbound is not None:
        variables.inbound = inbound
        variables.hasInbound = True
    req = NewBookingSearch(variables)
    try:
        res = httpx.post(
            url=PROD_URL,
            data=dumps(req.as_request()),  # type: ignore
            headers={"Content-Type": "application/json"},
            timeout=10.0,
        )
        assert res.status_code == 200, (res.status_code, res.content)
        data = NewJourneySearchResponse(res.json())
        return data
    except Exception as exception:
        exc = LoaderException(
            (asdict(req), res),
            ValueError(str(exception)),
            datetime.utcnow(),
            metadata={"outbound": outbound, "origin": origin.name, "destination": destination.name, "inbound": inbound},
        )
        write_exception(lib, exc)
        get_run_logger().warning("Unable to fetch Journey Data from API", exc_info=True)
        return None


def _generate_day_ranges(
    min_outbound_date: date, max_outbound_date: date
) -> Tuple[Set[date], Set[date], Dict[date, List[date]]]:
    day = min_outbound_date
    output: Tuple[Set[date], Set[date], Dict[date, List[date]]] = (
        set(),  # outbounds
        set(),  # inbounds
        defaultdict(list),  # return journeys
    )
    output[2][min_outbound_date].append(min_outbound_date)
    while day <= max_outbound_date:
        output[0].add(day)
        output[1].add(day)

        # given that return pricing doesn't appear to be dependent on the specific dates
        #   instead just on it being a return, we can cheat and just check (0,0), [(0,1), (1,2), ... (N, N+1)]
        output[2][day].append(day + timedelta(days=1))
        day += timedelta(days=1)
    return output


def find_trip(
    lib: Library,
    origin: Stations,
    destination: Stations,
    min_outbound_date: date,
    max_outbound_date: date,
) -> List[PrefectFuture[Optional[NewJourneySearchResponse], Any]]:
    dates = _generate_day_ranges(min_outbound_date, max_outbound_date)
    get_run_logger().info(
        "Generated %s outbound, %s inbound, %s returns",
        len(dates[0]),
        len(dates[1]),
        sum(1 for k in dates[2].keys() for _ in dates[2][k]),
    )

    requests = (
        [
            (lib, 1, outbound, origin, destination, inbound)
            for outbound in dates[2].keys()
            for inbound in dates[2][outbound]
        ]
        + [(lib, 1, outbound, origin, destination) for outbound in dates[0]]
        + [(lib, 1, inbound, destination, origin) for inbound in dates[1]]
    )
    results: List[PrefectFuture[Optional[NewJourneySearchResponse], Any]] = []

    for chunk in chunks(requests, 250):  # Limit open requests
        parallel = [get_fares_for.submit(*request) for request in chunk]
        for future in parallel:
            future.wait()
        results.extend(parallel)

    return results


def _search_to_df(searches: Iterator[PrefectFuture[Optional[NewJourneySearchResponse], Any]]) -> pd.DataFrame:
    output = []
    for future in searches:
        search = future.result()
        if search is None:
            continue
        for journeys, origin, destination in [
            (search.outbound.journeys, search.outbound.origin, search.outbound.destination),
            (
                search.inbound.journeys if search.inbound is not None else [],
                search.inbound.origin if search.inbound is not None else None,
                search.inbound.destination if search.inbound is not None else None,
            ),
        ]:
            for journey in journeys:
                for fare in journey.fares:
                    assert origin is not None
                    assert destination is not None
                    output.append(
                        {
                            "fetch_ts": search.fetched,
                            "date": journey.timing.date,
                            "departure_time": journey.timing.departureTime,
                            "arrival_time": journey.timing.arrivalTime,
                            "origin": origin.value.name,
                            "destination": destination.value.name,
                            "price_type": "return" if search.inbound is not None else "single",
                            "fare_type": fare.fareClass.value.name,
                            "total_price": fare.total,
                            "seats_remaining": fare.seats,
                        }
                    )
    return pd.DataFrame.from_dict(output).set_index("fetch_ts").sort_index()  # type: ignore


@flow(task_runner=ConcurrentTaskRunner())
def fetch_eurostar(min_days: int, max_days: int) -> None:
    logger = get_run_logger()
    logger.info("Days = (%s,%s)", min_days, max_days)

    _, _, lib = setup_app("eurostar", AppConfig, use_prefect=True)
    assert lib is not None

    futures = chain(
        find_trip(
            lib,
            Stations.LONDON,
            Stations.PARIS,
            date.today() + timedelta(days=min_days),
            date.today() + timedelta(days=max_days),
        ),
        find_trip(
            lib,
            Stations.LONDON,
            Stations.AMSTERDAM,
            date.today() + timedelta(days=min_days),
            date.today() + timedelta(days=max_days),
        ),
        find_trip(
            lib,
            Stations.LONDON,
            Stations.BRUSSELS,
            date.today() + timedelta(days=min_days),
            date.today() + timedelta(days=max_days),
        ),
    )
    logger.info("Submitted requests, building DF")
    df = _search_to_df(futures)
    logger.info("DF Realized, grouping and writing")
    symbols = []
    symbol_writes = []

    symbol = f"snapshot-{min(df.index)}-{max(df.index)}-{min_days}-{max_days}"
    symbols.append(symbol)
    symbol_writes.append(write_spliced_symbols.submit(lib, [(symbol, df)]))
    # Snapshot of the symbol list to reduce calls
    # Introduces edge case of multiple loaders writing first observation
    symbol_list = lib.list_symbols()
    symbol_df = []
    for grouping, data in df.groupby(["departure_time", "origin", "destination"]):
        departure_time, origin, destination = grouping
        symbol = f"{departure_time}/{origin}/{destination}"
        symbols.append(symbol)
        symbol_df.append((symbol, data))
    symbol_writes.append(write_spliced_symbols.submit(lib, symbol_df, dedupe=True, symbol_list=symbol_list))
    logger.info("Submitted writes, collecting futures")
    for future in symbol_writes:
        future.result()

    logger.info("Writes completed, defragmenting symbols if required")
    for symbol in symbols:
        defragment_symbol(lib, symbol)
    lib.list_symbols()


if __name__ == "__main__":
    fetch_eurostar(min_days=1, max_days=5)
