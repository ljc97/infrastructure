from collections import defaultdict
from dataclasses import dataclass
from datetime import date
from enum import Enum
from typing import Any, Dict, List, Optional, Tuple

import httpx
import numpy as np
import pandas as pd
from arcticdb.version_store.library import Library  # type: ignore
from bs4 import BeautifulSoup
from core_python.base_app import AppConfig, setup_app
from dateutil.parser import parse as dt_parse
from dateutil.relativedelta import relativedelta
from prefect import flow, get_run_logger, task
from prefect.task_runners import ConcurrentTaskRunner

from loaders.utils.arctic import (
    defragment_symbol,
    get_highwater_dt,
    set_highwater_dt,
    write_spliced_symbols,
)

URL = "https://www.bbc.co.uk/sport/{sport}/{league}/scores-fixtures/{period}"
TEAM_SPORTS = {
    "rugby-league": [
        "super-league",
        "league-one",
        "nrl-premiership",
        "championship",
        "womens-super-league",
        "international-match",
        "national-conference-premier",
    ],
    "rugby-union": ["english-premiership", "premiership-rugby-cup"],
    "football": ["premier-league", "league-one", "league-two", "fa-cup"],
}


class FixtureStatus(Enum):
    UPCOMING = "UPCOMING"
    FT = "FT"
    CANCELLED = "CANCELLED"
    MATCH_CANCELLED = "MATCH CANCELLED"
    MATCH_POSTPONED_OTHER = "MATCH POSTPONED - OTHER"
    MATCH_POSTPONED_WEATHER = "MATCH POSTPONED - WEATHER"
    MATCH_POSTPONED_INTERNATIONAL_CALL_UPS = "MATCH POSTPONED - INTERNATIONAL CALL-UPS"
    AET = "AET"

    @staticmethod
    def from_value(value: str) -> "FixtureStatus":
        for e in FixtureStatus:
            if e.value == value:
                return e
        raise ValueError("FixtureStatus not found", value)


@dataclass
class TeamFixtureInformation:
    sport: str
    league: str
    match_date: date
    home_team: str
    home_score: Optional[int]
    away_team: str
    away_score: Optional[int]
    status: FixtureStatus
    match_link: str

    def to_pandas_dict(self) -> Dict[str, Any]:
        return {
            "match_date": self.match_date,
            "home_team": self.home_team,
            "home_score": self.home_score if self.home_score else np.nan,
            "away_team": self.away_team,
            "away_score": self.away_score if self.away_score else np.nan,
            "status": str(self.status.value),
            "match_link": self.match_link if self.match_link else "",
        }


def _write_fixtures(lib: Library, df: pd.DataFrame, sport: str, league: str) -> str:
    symbol = f"{sport}/{league}"
    write_spliced_symbols(lib, [(symbol, df)], overwrite=True)
    return symbol


def _build_highwater_path(sport: str, league: str) -> str:
    return f"{sport}/{league}"


@task(retries=2, retry_delay_seconds=5, tags=["bbcsport_max_calls"])
def _load_team_sports_fixtures(sport: str, league: str, period: str) -> List[TeamFixtureInformation]:
    logger = get_run_logger()
    url = URL.format(sport=sport, league=league, period=period)
    resp = httpx.get(url, timeout=15.0)
    if resp.status_code != 200:
        raise ValueError("Unable to load data", url, resp.status_code)

    try:
        page = BeautifulSoup(resp.text)
        fixture_dates = page.find_all(class_="qa-fixture-block") or page.find_all(class_="qa-match-block")
        results = []
        for fixture_date in fixture_dates:
            text_date = (
                fixture_date.find(class_="sp-c-fixture-block-heading")
                or fixture_date.find(class_="sp-c-match-list-heading")
            ).text
            match_date = dt_parse(text_date)
            for fixture in fixture_date.find_all("li", class_="gs-o-list-ui__item"):
                status_block = fixture.find(class_="sp-c-fixture__status")
                if fixture.find(class_="sp-c-fixture__number--cancelled"):
                    status = FixtureStatus.CANCELLED
                else:
                    status = (
                        FixtureStatus.UPCOMING
                        if status_block is None
                        else FixtureStatus.from_value(status_block.text.upper())
                    )
                link = (fixture.find("a", class_="sp-c-fixture__block-link") or {}).get("href", "")
                if link is not None and not link.startswith("https://www.bbc.co.uk"):
                    link = "https://www.bbc.co.uk" + link

                if status in (FixtureStatus.UPCOMING, FixtureStatus.MATCH_CANCELLED):
                    team_names = fixture.find_all(
                        class_="gs-u-display-none gs-u-display-block@m qa-full-team-name sp-c-fixture__team-name-trunc"
                    )
                    home_team = team_names[0].text
                    away_team = team_names[1].text
                    home_score, away_score = None, None
                elif status in (FixtureStatus.CANCELLED,) or "MATCH POSTPONED" in status.value:
                    home_team = (
                        fixture.find(class_="sp-c-fixture__team-name sp-c-fixture__team-name--home").find("abbr").text
                    )
                    away_team = (
                        fixture.find(class_="sp-c-fixture__team-name sp-c-fixture__team-name--away").find("abbr").text
                    )
                    home_score, away_score = None, None
                else:
                    home_team = (
                        fixture.find(class_="sp-c-fixture__team-name sp-c-fixture__team-name--home").find("abbr").text
                    )
                    away_team = (
                        fixture.find(class_="sp-c-fixture__team-name sp-c-fixture__team-name--away").find("abbr").text
                    )
                    home_score = int(
                        fixture.find(
                            class_="sp-c-fixture__number sp-c-fixture__number--home sp-c-fixture__number--ft"
                        ).text
                    )
                    away_score = int(
                        fixture.find(
                            class_="sp-c-fixture__number sp-c-fixture__number--away sp-c-fixture__number--ft"
                        ).text
                    )
                result = TeamFixtureInformation(
                    sport, league, match_date, home_team, home_score, away_team, away_score, status, link
                )
                results.append(result)
        return results
    except Exception as exc:
        logger.error("Unable to parse page %s", url, exc)
        return []


@flow(task_runner=ConcurrentTaskRunner())
def fetch_bbc_team_sports_fixtures() -> None:
    logger = get_run_logger()
    _, _, lib = setup_app("bbcsport", AppConfig, use_prefect=True)
    assert lib is not None

    min_available_date = date.today().replace(day=1) - relativedelta(years=1)
    max_available_date = date.today() + relativedelta(years=1)

    min_available_date += relativedelta(months=9)
    max_available_date = date.today() + relativedelta(years=1)

    args: List[Tuple[str, str, str]] = []
    for sport, leagues in TEAM_SPORTS.items():
        for league in leagues:
            logger.info("Running for %s/%s", sport, league)
            last_loaded_date = get_highwater_dt(lib, _build_highwater_path(sport, league))
            start_load_date = (
                min_available_date
                if last_loaded_date is None or min_available_date > last_loaded_date
                else last_loaded_date
            )
            logger.info(
                "Selected start_load_date of %s from (min_available_date=%s, last_loaded_date=%s)",
                start_load_date,
                min_available_date,
                last_loaded_date,
            )
            load_month = start_load_date
            months = []
            while load_month < max_available_date:
                months.append(f"{load_month.year}-{load_month.month:02}")
                load_month += relativedelta(months=1)
            logger.info("Loading months %s", months)
            args.extend(zip([sport] * len(months), [league] * len(months), months))

    futures = [(arg, _load_team_sports_fixtures.submit(*arg)) for arg in args]

    accumulator = defaultdict(list)
    last_complete_period = {}
    for (sport, league, period), future in futures:
        result = future.result()
        accumulator[(sport, league)].extend(result)
        statuses = {r.status for r in result}
        if FixtureStatus.FT in statuses and FixtureStatus.UPCOMING not in statuses:
            if (sport, league) not in last_complete_period:
                last_complete_period[(sport, league)] = period
            if period > last_complete_period[(sport, league)]:
                last_complete_period[(sport, league)] = period
    logger.info("Calculated last_complete_period(s) of %s", last_complete_period)
    for (sport, league), data in accumulator.items():
        logger.info("Loaded %s fixture(s) for %s/%s", len(data), sport, league)
        if len(data) == 0:
            continue
        df = pd.DataFrame([d.to_pandas_dict() for d in data])
        symbol = _write_fixtures(lib, df, sport, league)
        defragment_symbol(lib, symbol)
        if (sport, league) not in last_complete_period:
            continue

        year, month = period.split("-")
        logger.info("Setting highwater for %s/%s to %s", sport, league, period)
        set_highwater_dt(lib, _build_highwater_path(sport, league), date(int(year), int(month), 1))


if __name__ == "__main__":
    fetch_bbc_team_sports_fixtures()
