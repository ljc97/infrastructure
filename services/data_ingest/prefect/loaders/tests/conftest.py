from datetime import datetime
from tempfile import TemporaryDirectory
from typing import Generator
from uuid import uuid4

import pytest
from arcticdb import Arctic  # type: ignore
from arcticdb.version_store.library import Library  # type:ignore


@pytest.fixture(scope="session")
def db() -> Generator[Arctic, None, None]:
    with TemporaryDirectory() as tmp_dir:
        yield Arctic(f"lmdb:///{tmp_dir}")


@pytest.fixture(scope="function")
def lib(db: Arctic) -> Generator[Library, None, None]:
    lib_name = f"{datetime.utcnow().isoformat()}_{uuid4()}"
    db.create_library(lib_name)
    yield db.get_library(lib_name)
    db.delete_library(lib_name)
