from typing import Union

import numpy as np
import pytest

from loaders.batch.preowned.common import parse_price_str


@pytest.mark.parametrize(
    "example, expected",
    [
        ("1.0", 1.0),
        ("0.0", 0.0),
        ("£15.0", 15.0),
        ("£15", 15.0),
        ("£1,300", 1300),
        ("£1,578.53", 1578.53),
        ("1000,00", 1000.0),
        ("PricePending", np.nan),
        ("$1000.00", 1000.0),
    ],
)
def test_parse_price_str(example: str, expected: Union[float, np.float64]) -> None:
    res = parse_price_str(example)
    if np.isnan(expected):
        assert np.isnan(res)
    else:
        assert res == expected


@pytest.mark.parametrize("example", ["1.000,00"])
def test_parse_price_str_errors(example: str) -> None:
    with pytest.raises(ValueError):
        parse_price_str(example)
