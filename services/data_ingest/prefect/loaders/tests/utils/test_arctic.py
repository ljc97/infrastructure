from datetime import datetime, timedelta
from typing import Any, Dict

import pandas as pd
import pytest
from arcticdb.version_store.library import Library  # type: ignore

from loaders.utils.arctic import (
    cleanup_meta_symbols,
    retrieve_exceptions,
    retrieve_snapshots,
    write_exception,
    write_snapshot,
)
from loaders.utils.meta import LoaderException, LoaderMetaType, LoaderSnapshot


@pytest.mark.parametrize(
    "df, dt, additional_keys, metadata",
    [
        (pd.DataFrame({}), datetime.utcnow(), {}, None),
        (pd.DataFrame({}), datetime.utcnow(), {"a": "b", "test": "c"}, None),
        (
            pd.DataFrame({}),
            datetime.utcnow(),
            {"a": "b", "test": "c"},
            {},
        ),
        (
            pd.DataFrame({}),
            datetime.utcnow(),
            {"a": "b", "test": "c"},
            {"a": 1, "b": [0, 1, 2], "_dt": datetime.utcnow()},
        ),
    ],
)
@pytest.mark.parametrize("thin", [True, False])
def test_snapshots_write(
    lib: Library,
    df: pd.DataFrame,
    dt: datetime,
    additional_keys: Dict[str, str],
    metadata: Dict[str, Any],
    thin: bool,
) -> None:
    assert list(retrieve_snapshots(lib)) == []

    snap = LoaderSnapshot(df, dt, additional_keys, metadata)
    write_snapshot(lib, snap)

    snaps = list(retrieve_snapshots(lib, thin=thin))
    assert len(snaps) == 1
    if thin:
        snap.df = None
    assert snaps[0] == snap


def test_snapshots_read_multiple(lib: Library) -> None:
    snaps = [
        LoaderSnapshot(pd.DataFrame({}), datetime(1970, 1, 1), {}, None),
        LoaderSnapshot(pd.DataFrame({}), datetime(1980, 1, 1), {"a": "b"}, {}),
        LoaderSnapshot(pd.DataFrame({}), datetime(1990, 1, 1), {"a": "b"}, {"test": 7.0}),
    ]
    for snap in snaps:
        write_snapshot(lib, snap)

    retrieved = list(retrieve_snapshots(lib, thin=False))
    assert snaps == sorted(retrieved, key=lambda snap: snap.run_time)


def test_cleanup_meta_symbols_snapshot(lib: Library) -> None:
    snaps = [
        LoaderSnapshot(pd.DataFrame({}), datetime(1970, 1, 1), {}, None),
        LoaderSnapshot(pd.DataFrame({}), datetime(1980, 1, 1), {"a": "b"}, {}),
        LoaderSnapshot(pd.DataFrame({}), datetime(1990, 1, 1), {"a": "b"}, {"test": 7.0}),
        LoaderSnapshot(pd.DataFrame({}), datetime.utcnow(), {}, None),
    ]
    assert len(list(retrieve_snapshots(lib, thin=True))) == 0
    cleanup_meta_symbols(lib, timedelta(hours=24), LoaderMetaType.SNAPSHOT)
    for snap in snaps:
        write_snapshot(lib, snap)

    assert len(list(retrieve_snapshots(lib, thin=True))) == 4
    cleanup_meta_symbols(lib, timedelta(hours=24), LoaderMetaType.SNAPSHOT)
    assert len(list(retrieve_snapshots(lib, thin=True))) == 1


def test_cleanup_meta_symbols_exception(lib: Library) -> None:
    exceptions = [
        LoaderException([0], ValueError("a"), datetime(1970, 1, 1), {}, None),
        LoaderException([1], ValueError("b"), datetime(1980, 1, 1), {"a": "b"}, {}),
        LoaderException([2], ValueError("c"), datetime(1990, 1, 1), {"a": "b"}, {"test": 7.0}),
        LoaderException([3], ValueError("d"), datetime.utcnow(), {}, None),
    ]
    assert len(list(retrieve_exceptions(lib, thin=True))) == 0
    cleanup_meta_symbols(lib, timedelta(hours=24), LoaderMetaType.EXCEPTION)
    for exc in exceptions:
        write_exception(lib, exc)

    assert len(list(retrieve_exceptions(lib, thin=True))) == 4
    cleanup_meta_symbols(lib, timedelta(hours=24), LoaderMetaType.EXCEPTION)
    assert len(list(retrieve_exceptions(lib, thin=True))) == 1


@pytest.mark.parametrize(
    "payload, exception, dt, additional_keys, metadata",
    [
        ([0], ValueError("0"), datetime.utcnow(), {}, None),
        ([1], ValueError("1"), datetime.utcnow(), {"a": "b", "test": "c"}, None),
        (
            [2],
            ValueError("2"),
            datetime.utcnow(),
            {"a": "b", "test": "c"},
            {},
        ),
        (
            [3],
            ValueError("3"),
            datetime.utcnow(),
            {"a": "b", "test": "c"},
            {"a": 1, "b": [0, 1, 2], "_dt": datetime.utcnow()},
        ),
    ],
)
@pytest.mark.parametrize("thin", [True, False])
def test_exception_write(
    lib: Library,
    payload: Any,
    exception: Exception,
    dt: datetime,
    additional_keys: Dict[str, str],
    metadata: Dict[str, Any],
    thin: bool,
) -> None:
    assert list(retrieve_exceptions(lib)) == []

    exc = LoaderException(payload, exception, dt, additional_keys, metadata)
    write_exception(lib, exc)

    exceptions = list(retrieve_exceptions(lib, thin=thin))
    assert len(exceptions) == 1
    if thin:
        exc.payload = None
        exc.exception = None
        exc.traceback = ""
    else:
        assert str(exc.exception) in exceptions[0].traceback
        exc.exception = None

    assert exceptions[0] == exc


def test_exception_read_multiple(lib: Library) -> None:
    exceptions = [
        LoaderException([0], ValueError("0"), datetime.utcnow(), {}, None),
        LoaderException([1], ValueError("1"), datetime.utcnow(), {"a": "b", "test": "c"}, None),
        LoaderException(
            [2],
            ValueError("2"),
            datetime.utcnow(),
            {"a": "b", "test": "c"},
            {},
        ),
        LoaderException(
            [3],
            ValueError("3"),
            datetime.utcnow(),
            {"a": "b", "test": "c"},
            {"a": 1, "b": [0, 1, 2], "_dt": datetime.utcnow()},
        ),
    ]
    for exc in exceptions:
        write_exception(lib, exc)
        exc.exception = None

    retrieved = list(retrieve_exceptions(lib, thin=False))
    assert exceptions == sorted(retrieved, key=lambda exc: exc.run_time)
