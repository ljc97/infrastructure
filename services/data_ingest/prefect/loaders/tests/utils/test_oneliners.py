from typing import Any, List

import pytest

from loaders.utils.oneliners import chunks


@pytest.mark.parametrize(
    "n, data, expected", [(5, [], []), (4, [1, 2, 3, 4, 5], [[1, 2, 3, 4], [5]]), (5, [1, 2, 3], [[1, 2, 3]])]
)
def test_chunks(n: int, data: List[Any], expected: List[List[Any]]) -> None:
    assert expected == list(chunks(data, n))
