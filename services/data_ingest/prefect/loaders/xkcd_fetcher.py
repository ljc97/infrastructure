import re
import time
from dataclasses import dataclass
from datetime import date
from typing import List, Optional, Set

import httpx
import pandas as pd
from arcticdb.version_store.library import Library  # type: ignore
from core_python.base_app import AppConfig, setup_app
from dateutil.parser import parse
from prefect import flow, get_run_logger, task

from loaders.utils.arctic import defragment_symbol
from loaders.utils.comms import submit_slack_webhook


@dataclass
class ArchiveEntry:
    idx: int
    published: date
    title: str


@task
def _find_all_ids() -> Optional[List[ArchiveEntry]]:
    archive_page = "https://xkcd.com/archive/"
    resp = httpx.get(archive_page)
    if resp.status_code != 200:
        get_run_logger().warning("Unable to fetch IDs from archive - status code ", resp.status_code)
        return None

    # <a href="/5/" title="2006-1-1">Blown apart</a><br/>
    matches = re.finditer(r'<a href="/([0-9]+)/" title="([0-9]+-[0-9]+-[0-9]+)">(.*)</a><br/>', resp.text)
    if matches is None:
        return None

    return [ArchiveEntry(int(search.group(1)), parse(search.group(2)), str(search.group(3))) for search in matches]


@task
def _store_xkcd_artifacts(lib: Library, xkcds: List[ArchiveEntry]) -> Set[str]:
    df = (
        pd.DataFrame(
            {
                "id": [xkcd.idx for xkcd in xkcds],
                "link": [f"https://xkcd.com/{xkcd.idx}/" for xkcd in xkcds],
                "title": [xkcd.title for xkcd in xkcds],
                "published": [xkcd.published for xkcd in xkcds],
            }
        )
        .set_index("id")
        .sort_index()
    )
    symbol = "all"
    lib.write(symbol, df)
    return {symbol}


@flow
def fetch_xkcd_ids() -> None:
    _, _, lib = setup_app("xkcd", AppConfig, use_prefect=True)
    assert lib is not None

    logger = get_run_logger()
    all_xkcds = _find_all_ids()
    if all_xkcds is None:
        logger.info("Failed to find any XKCDs :sadness:")
    assert all_xkcds is not None
    logger.info("Loaded %s XKCDs", len(all_xkcds))
    existing_max_idx = max(lib.read("all").data.index) if "all" in lib.list_symbols() else -1
    new_comics = sorted([comic for comic in all_xkcds if comic.idx > existing_max_idx], key=lambda comic: comic.idx)
    for xkcd in new_comics:
        submit_slack_webhook(
            channel_id="xkcd",
            title=f"New XKCD - {xkcd.idx}",
            body=f"{xkcd.title} <https://xkcd.com/{xkcd.idx}|{xkcd.idx}>",
        )
        time.sleep(5.0)
    symbols = _store_xkcd_artifacts(lib, all_xkcds)
    for symbol in symbols:
        defragment_symbol(lib, symbol)


if __name__ == "__main__":
    fetch_xkcd_ids()
