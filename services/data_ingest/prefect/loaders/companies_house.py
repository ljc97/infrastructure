import re
import tempfile
import zipfile
from dataclasses import dataclass
from datetime import date
from typing import Optional

import httpx
import pandas as pd
from arcticdb.version_store.library import Library  # type: ignore
from bs4 import BeautifulSoup
from core_python.base_app import AppConfig, setup_app
from prefect import flow, get_run_logger, task

URL = "https://download.companieshouse.gov.uk/en_output.html"
FILE_PATTERN = r"BasicCompanyDataAsOneFile\-([0-9]+)\-([0-9]+)\-([0-9]+).zip"
META_PROGRESS_SYMBOL = "_last_loaded_file"


@dataclass
class CompanyDataFile:
    name: str
    published_date: date
    url: str


@task
def fetch_file_link(url: str) -> CompanyDataFile:
    logger = get_run_logger()
    resp = httpx.get(url, timeout=10.0)
    if resp.status_code != 200:
        logger.error("Unable to fetch download.companieshouse", resp.status_code)
        raise ValueError("Status %s", resp.status_code)
    page = BeautifulSoup(resp.text, "html.parser")
    links = page.find_all("a")
    for link in links:
        search = re.search(FILE_PATTERN, (link.text or ""))
        if search is None:
            continue
        print(link)
        published_date = date(int(search.group(1)), int(search.group(2)), int(search.group(3)))
        return CompanyDataFile(
            search.group(0), published_date, f"http://download.companieshouse.gov.uk/{link.get('href')}"
        )
    raise ValueError("Found no file from available links %s", [link.text for link in links])


@task
def download_file(destination: str, company_data_file: CompanyDataFile) -> None:
    logger = get_run_logger()
    logger.info("Downloading from %s", company_data_file.url)
    logger.info("Writing into %s", destination)
    with open(destination, "wb") as f:
        f.write(httpx.get(company_data_file.url, timeout=30.0, follow_redirects=True).content)
    logger.info("Written data file")


@task
def extract_file(path: str, to: str) -> None:
    logger = get_run_logger()
    logger.info("Extracting from %s to %s", path, to)
    with zipfile.ZipFile(path, "r") as zip_f:
        zip_f.extractall(to)
    logger.info("Extracted file(s)")


@task
def read_df(path: str) -> pd.DataFrame:
    return pd.read_csv(path)


@task
def get_last_completed_file_date(lib: Library) -> Optional[date]:
    return lib.read(META_PROGRESS_SYMBOL).data if META_PROGRESS_SYMBOL in lib.list_symbols() else None


@task
def write_last_completed_file_date(lib: Library, published_date: date) -> None:
    lib.write_pickle(META_PROGRESS_SYMBOL, published_date)


@task
def write_data(lib: Library, df: pd.DataFrame, company_data_file: CompanyDataFile) -> None:
    lib.write(
        "companies_raw",
        df,
        metadata={
            "name": company_data_file.name,
            "url": company_data_file.url,
            "published_date": company_data_file.published_date,
        },
    )


@flow
def load_companies_house() -> None:
    _, _, lib = setup_app("companieshouse", AppConfig, use_prefect=True)
    assert lib is not None

    logger = get_run_logger()
    company_data_file = fetch_file_link(URL)
    last_completed = get_last_completed_file_date(lib)

    logger.info("Identified file %s", company_data_file)
    if last_completed is not None and last_completed >= company_data_file.published_date:
        logger.info("Last completed %s - ending", last_completed)
        return

    with tempfile.TemporaryDirectory() as tmpdir:
        path = f"{tmpdir}/{company_data_file.name}"
        download_file(path, company_data_file)
        extract_file(path, tmpdir)

        csv_path = path.replace(".zip", ".csv")
        df = read_df(csv_path)
        logger.info("Loaded DF of shape %s", df.shape)

    logger.info("Writing data into arctic")
    write_data(lib, df, company_data_file)
    logger.info("Data written, updating watermark")

    write_last_completed_file_date(lib, company_data_file.published_date)
    logger.info("Watermark updated")


if __name__ == "__main__":
    load_companies_house()
