import re
from collections import defaultdict
from dataclasses import dataclass
from datetime import timedelta
from typing import Any, Callable, Dict, Tuple, Union

import httpx
import numpy as np
import pandas as pd
from bs4 import BeautifulSoup, NavigableString, Tag
from core_python.base_app import AppConfig, setup_app
from dateutil.parser import parse
from prefect import flow, get_run_logger, task
from prefect.task_runners import ConcurrentTaskRunner

from loaders.utils.arctic import defragment_symbol, write_spliced_symbols


@dataclass
class ParkRunner:
    character: str
    number: int


@dataclass
class Course:
    name: str
    short_name: str


RUNNER_URL = "https://www.parkrun.org.uk/parkrunner/{runner_number}/all/"
COURSE_URL = "https://www.parkrun.org.uk/{short_name}/results/eventhistory/"

RUNNERS = [
    ParkRunner("A", 9117802),
]
COURSES = [
    Course("Hampstead Heath parkrun", "hampsteadheath"),
]
UA = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36"

EXPECTED_RUN_HEADERS = ["Event", "Run Date", "Run Number", "Pos", "Time", "AgeGrade", "PB?"]
EXPECTED_RUN_HEADERS_MAP_FN: Dict[str, Callable[[BeautifulSoup], Any]] = defaultdict(
    lambda: lambda x: x.text.replace("\n", "").strip().rstrip()
)


def _parse_time(x: Tag | NavigableString | BeautifulSoup) -> float:
    parts = [int(itm) for itm in x.text.strip().rstrip().split(":")]
    assert len(parts) == 2
    # arctic can't currently store timedelta64[ns], hence float
    # [arcticdb] [error] E_ASSERTION_FAILURE Invalid dtype 'UNKNOWN' in visit dim
    return timedelta(minutes=parts[0], seconds=parts[1]).total_seconds()


EXPECTED_RUN_HEADERS_MAP_FN.update(
    {
        "Run Date": lambda x: parse(x.text),
        "Run Number": lambda x: int(x.text),
        "Pos": lambda x: int(x.text),
        "AgeGrade": lambda x: float(x.text.strip().rstrip().replace("%", "")),
        "PB?": lambda x: x.text.replace("\n", "").strip().rstrip() == "PB" if x and x.text else False,
        "Time": _parse_time,
    }
)

EXPECTED_COURSE_HEADERS = [
    "Event ##",
    "Date",
    "Date/First Finishers",
    "Finishers",
    "Volunteers",
    "Male First Finisher",
    "Female First Finisher",
    "Male First Finisher",
    "Female First Finisher",
]


def _resolve_compact(td: BeautifulSoup) -> Tag | NavigableString:
    compact = td.find("div", class_="compact")
    return td if compact is None else compact


def _parse_runner(td: BeautifulSoup) -> str:
    text = _resolve_compact(td).text.strip().rstrip()
    search = re.search(r"^([^0-9:]+)([0-9:]+)$", text)
    assert search, text
    assert isinstance(search.group(1), str)
    return search.group(1)


def _parse_participants(td: BeautifulSoup) -> Union[float, np.float64]:
    text = (
        _resolve_compact(td).text.replace("finishers", "").replace("volunteers", "").replace("\n", "").strip().rstrip()
    )
    return np.nan if text == "Unknown" else float(text)


EXPECTED_COURSE_HEADERS_MAP_FN: Dict[int, Callable[[BeautifulSoup], Any]] = defaultdict(
    lambda: lambda x: _resolve_compact(x).text.replace("\n", "").strip().rstrip()
)
EXPECTED_COURSE_HEADERS_MAP_FN.update(
    {
        0: lambda x: int(_resolve_compact(x).text),
        1: lambda x: parse(_resolve_compact(x).text),
        2: _parse_participants,
        3: _parse_participants,
        4: _parse_runner,
        5: lambda x: _parse_time(_resolve_compact(x)),
        6: _parse_runner,
        7: lambda x: _parse_time(_resolve_compact(x)),
    }
)


@task(retries=3, retry_delay_seconds=5)
def _retrieve_url(url: str) -> BeautifulSoup:
    res = httpx.get(url, follow_redirects=True, headers={"User-Agent": UA}, timeout=30.0)
    assert res.status_code == 200, (res, url)
    return BeautifulSoup(res.content, "html.parser")


@flow(task_runner=ConcurrentTaskRunner(), validate_parameters=False)
def fetch_parkrunner(parkrunner: ParkRunner) -> Tuple[str, pd.DataFrame]:
    # async with concurrency("parkrun_max_calls", occupy=1):
    page = _retrieve_url.submit(RUNNER_URL.format(runner_number=parkrunner.number)).result()
    tables = page.findAll("table", id="results")
    all_results = next(tbl for tbl in tables if "All  Results" in tbl.find("caption").text)
    headers = [th.text for th in all_results.findAll("th")]
    assert headers == EXPECTED_RUN_HEADERS, headers
    results = defaultdict(list)
    for row in all_results.findAll("tr"):
        data = row.findAll("td")
        for idx, td in enumerate(data):
            header = EXPECTED_RUN_HEADERS[idx]
            results[header].append(EXPECTED_RUN_HEADERS_MAP_FN[header](td))
    df = pd.DataFrame(results).set_index("Run Date").sort_index()
    symbol = f"runner/{parkrunner.character}/{parkrunner.number}"
    return (symbol, df)


@flow(task_runner=ConcurrentTaskRunner(), validate_parameters=False)
def fetch_course(course: Course) -> Tuple[str, pd.DataFrame]:
    logger = get_run_logger()
    # async with concurrency("parkrun_max_calls", occupy=1):
    page = _retrieve_url.submit(COURSE_URL.format(short_name=course.short_name)).result()
    table = page.find("table", class_="Results-table")
    assert table is not None and isinstance(table, Tag)
    headers = [th.text for th in table.findAll("th")]
    assert headers == EXPECTED_COURSE_HEADERS, headers
    logger.info("Identified headers %s", headers)
    results = defaultdict(list)
    for row in table.findAll("tr"):
        data = row.findAll("td")
        for idx, td in enumerate(data):
            # use idx because headers are ambiguous
            results[idx].append(EXPECTED_COURSE_HEADERS_MAP_FN[idx](td))
    df = (
        pd.DataFrame(
            {
                "Event Number": results[0],
                "Date": results[1],
                "Finishers": results[2],
                "Volunteers": results[3],
                "Male First Finisher": results[4],
                "Male First Finisher Time": results[5],
                "Female First Finisher": results[6],
                "Female First Finisher Time": results[7],
            }
        )
        .set_index("Date")
        .sort_index()
    )
    symbol = f"course/{course.short_name}"
    return (symbol, df)


@flow(task_runner=ConcurrentTaskRunner())
def fetch_parkrun_stats() -> None:
    _, _, lib = setup_app("parkrun", AppConfig, use_prefect=True)
    assert lib is not None
    logger = get_run_logger()

    results = [fetch_parkrunner(runner) for runner in RUNNERS] + [fetch_course(course) for course in COURSES]
    logger.info("Loaded %s symbol(s)", len(results))

    symbols = {sym for sym, _ in results}
    assert len(symbols) == len(results)

    write_spliced_symbols(lib, results, dedupe=True)
    for sym in symbols:
        defragment_symbol(lib, sym)


if __name__ == "__main__":
    fetch_parkrun_stats()
