import re
from dataclasses import dataclass
from io import StringIO
from typing import Dict, List, Optional, Tuple

import httpx
import pandas as pd
from bs4 import BeautifulSoup
from core_python.base_app import AppConfig, setup_app
from prefect import flow, get_run_logger, task
from prefect.task_runners import ConcurrentTaskRunner

from loaders.utils.arctic import (
    defragment_symbol,
    write_spliced_symbols,
)

URL_TEMPLATE = "https://www.nationalgrideso.com/search-data?page=0%2C{page_id}"
TAB_HEADER_RE = r".*Datasets \(([0-9]+)\).*"
DATASETS_PER_PAGE = 10


@dataclass
class DatasetRequestSpecification:
    name: str
    base_symbol: str
    desired_format: str
    index_columns: List[str]


DATASET_REQUESTS = [
    DatasetRequestSpecification(
        "Historic Demand Data", "historic_demand_data/", "CSV", ["SETTLEMENT_DATE", "SETTLEMENT_PERIOD"]
    ),
    DatasetRequestSpecification(
        "Monthly Operational Metered Wind Output",
        "monthly_operational_metered_wind_output/",
        "CSV",
        ["Sett_Date", "Sett_Period"],
    ),
    DatasetRequestSpecification(
        "Daily & Weekly NRAPM (Negative Reserve Active Power Margin) Forecast",
        "daily_and_weekly_nrapm_forecast/",
        "CSV",
        ["Date"],
    ),
    DatasetRequestSpecification(
        "Day Ahead Half Hourly Demand Forecast Performance",
        "day_ahead_half_hourly_demand_forecast_performance/",
        "CSV",
        ["Datetime"],
    ),
]
DATASETS_TO_LOAD = {dataset.name: dataset for dataset in DATASET_REQUESTS}


@dataclass
class Dataset:
    name: str
    url: str
    organization: str
    formats: List[str]


@dataclass
class DownloadReference:
    title: str
    name: str
    url: str


def _fetch_page(url: str) -> BeautifulSoup:
    res = httpx.get(url, timeout=15.0, follow_redirects=True)
    assert res.status_code == 200, (res, url)
    return BeautifulSoup(res.content, "html.parser")


def _get_number_of_pages() -> Tuple[BeautifulSoup, int]:
    logger = get_run_logger()

    first_page = _fetch_page(URL_TEMPLATE.format(page_id=0))
    tab_headers = first_page.findAll("div", class_="content-title")
    total_datasets = None
    for tab in tab_headers:
        span = tab.find("span")
        if span is None or not hasattr(span, "text"):
            continue
        search = re.search(TAB_HEADER_RE, span.text)
        assert search is not None, span.text
        total_datasets = int(search.group(1))
    assert total_datasets is not None, tab_headers
    num_pages = total_datasets // DATASETS_PER_PAGE + 1

    logger.info("Identified %s dataset(s) for download, across %s page(s)", total_datasets, num_pages)

    return first_page, num_pages


@task(tags=["national_grid_max_calls"])
def _load_datasets_from_page(page_id: int, page_content: Optional[BeautifulSoup] = None) -> List[Dataset]:
    if page_content is None:
        page_content = _fetch_page(URL_TEMPLATE.format(page_id=page_id))

    datasets = []

    rows = page_content.findAll("div", class_="views-row")
    for row in rows:
        format_field = row.find("span", class_="dataset-format-field")
        formats = [field.text for field in format_field.findAll("li")] if format_field else []

        title = row.find("div", class_="views-field-title").find("span", class_="field-content").find("a")
        url = "https://www.nationalgrideso.com" + title["href"]
        name = title.text
        organization = row.find("div", class_="views-field-organization").find("span", class_="field-content").text

        datasets.append(Dataset(name, url, organization, formats))

    return datasets


def _identify_downloads_from_url(url: str, desired_format: str) -> List[DownloadReference]:
    page = _fetch_page(url)

    data_files_box = page.find("div", class_="views-element-container block-views-blockdockan-resources-block")
    assert data_files_box is not None
    table = data_files_box.find("table")
    assert table is not None and hasattr(table, "find")
    table_body = table.find("tbody")
    assert table_body is not None and hasattr(table_body, "findAll")
    table_rows = table_body.findAll("tr")

    downloads = []
    for row in table_rows:
        available_format = row.find("td", headers="view-format-target-id-table-column").text.strip().rstrip()
        if available_format != desired_format:
            continue
        title = row.find("td", headers="view-title-table-column").text.strip().rstrip()
        resource_download = row.find("a", class_="resource-download-link")
        resource_name = resource_download["data-resource"]
        resource_url = resource_download["href"]

        downloads.append(DownloadReference(title, resource_name, resource_url))
    return downloads


@task(tags=["national_grid_max_calls"])
def _retrieve_csv(reference: DownloadReference, specification: DatasetRequestSpecification) -> Tuple[str, pd.DataFrame]:
    res = httpx.get(reference.url, timeout=60.0, follow_redirects=True)
    assert res.status_code == 200, (res, reference.url)
    df = pd.read_csv(StringIO(res.content.decode("utf-8"))).set_index(specification.index_columns).sort_index()
    symbol = specification.base_symbol + reference.name
    return (symbol, df)


# TODO: https://github.com/PrefectHQ/prefect/issues/11360
@flow(task_runner=ConcurrentTaskRunner(), validate_parameters=False)
def load_national_grid_csv(dataset: Dataset) -> None:
    _, _, lib = setup_app("nationalgrid", AppConfig, use_prefect=True)
    assert lib is not None

    logger = get_run_logger()
    spec = DATASETS_TO_LOAD[dataset.name]
    logger.info("Loading %s in format %s to %s", dataset, spec.desired_format, spec.base_symbol)

    to_download = _identify_downloads_from_url(dataset.url, spec.desired_format)
    logger.info("Identified %s potential download(s) - %s", len(to_download), to_download)

    futures = [_retrieve_csv.submit(reference, spec) for reference in to_download]
    symbol_dfs = []
    for f in futures:
        symbol_dfs.append(f.result())
    write_spliced_symbols(lib, symbol_dfs, overwrite=True)
    for symbol, _ in symbol_dfs:
        defragment_symbol(lib, symbol)


@flow(task_runner=ConcurrentTaskRunner())
def fetch_national_grid_datasets() -> None:
    logger = get_run_logger()
    first_page, num_pages = _get_number_of_pages()

    futures = [_load_datasets_from_page.submit(0, first_page)] + [
        _load_datasets_from_page.submit(page_id) for page_id in range(1, num_pages)
    ]
    datasets = []
    for f in futures:
        datasets.extend(f.result())
    logger.info("Loaded %s dataset(s)", len(datasets))

    extracting: Dict[str, Dataset] = {}
    for dataset in datasets:
        if dataset.name not in DATASETS_TO_LOAD:
            continue
        if DATASETS_TO_LOAD[dataset.name].desired_format not in dataset.formats:
            continue
        if dataset.name in extracting:
            logger.error("Duplicate dataset identified!", dataset, extracting[dataset.name])
            raise ValueError("Duplicate dataset")
        extracting[dataset.name] = dataset

    logger.info("Identified %s dataset(s) of %s target(s)", len(extracting), len(DATASETS_TO_LOAD))
    for dataset in extracting.values():
        load_national_grid_csv(dataset)


if __name__ == "__main__":
    fetch_national_grid_datasets()
