from datetime import datetime
from typing import List, Tuple

from bs4 import BeautifulSoup
from prefect import flow
from prefect.task_runners import ConcurrentTaskRunner

from loaders.batch.preowned.common import (
    PreownedProductFetcher,
    ProductObservation,
    Store,
    StoreCache,
)

URL_TEMPLATE = "https://www.pmtonline.co.uk/pre-owned?p={page_id}&product_list_limit=48"
PRODUCTS_PER_PAGE = 48


def _parse_num_pages(page: BeautifulSoup) -> int:
    items = page.findAll("span", class_="toolbar-number")
    total_items = max(int(item.text) for item in items)
    return total_items // PRODUCTS_PER_PAGE + 1


def _get_products_from_page(
    page: BeautifulSoup, observation_dt: datetime, store_cache: StoreCache
) -> Tuple[List[ProductObservation], StoreCache]:
    product_grids = page.find_all("li", class_="item product product-item")

    products = []
    for grid in product_grids:
        product_item_link = grid.find("a", class_="product-item-link")
        product_name = (
            product_item_link.text.strip()
            .replace("B Stock ", "")
            .replace("B-Stock ", "")
            .replace("EX Demo", "")
            .replace("Pre-Loved", "")
        )
        product_price_text = grid.find("span", class_="price-wrapper").get("data-price-amount")
        price_boxes = grid.findAll("div", class_="price-final_price")
        product_ids = [prc["data-product-id"] for prc in price_boxes if prc.has_attr("data-product-id")]
        assert len(set(product_ids)) == 1, product_ids
        sku = product_ids[0]

        products.append(
            ProductObservation(
                Store.PMTONLINE,
                observation_dt,
                sku,
                product_name,
                float(product_price_text),
                product_item_link.get("href"),
                grid.find("img", class_="product-image-photo").get("data-src"),
            )
        )

    return (products, store_cache)


@flow(task_runner=ConcurrentTaskRunner())
def fetch_pmtonline_products() -> None:
    fetcher = PreownedProductFetcher(Store.PMTONLINE, URL_TEMPLATE, _parse_num_pages, _get_products_from_page)
    fetcher.run_loader(fetcher)


if __name__ == "__main__":
    fetch_pmtonline_products()
