from collections import defaultdict
from dataclasses import dataclass, field
from datetime import datetime
from enum import Enum
from typing import Any, Callable, Dict, List, Tuple, Union

import httpx
import numpy as np
import pandas as pd
from bs4 import BeautifulSoup
from core_python.base_app import AppConfig, setup_app
from prefect import flow, get_run_logger, task
from prefect.task_runners import ConcurrentTaskRunner

from loaders.utils.arctic import (
    defragment_symbol,
    write_snapshot,
    write_spliced_symbols,
)
from loaders.utils.meta import LoaderSnapshot


class Store(Enum):
    ANDERTONS = "Andertons"
    GIGGEAR = "GigGear"
    GUITARGUITAR = "GuitarGuitar"
    GUITARVILLAGE = "GuitarVillage"
    LIFEGUITARS = "LifeGuitars"
    PMTONLINE = "PMTOnline"
    SOUNDAFFECTSPREMIER = "SoundAffectsPremier"


@dataclass
class ProductObservation:
    store: Store
    observation_dt: datetime
    sku: str
    title: str
    price: float | np.float64
    url: str
    cover_image_url: str


def _product_observation_to_df(products: List[ProductObservation]) -> pd.DataFrame:
    return (
        pd.DataFrame(
            {
                "observation_dt": [p.observation_dt for p in products],
                "store": [p.store.value for p in products],
                "sku": [p.sku for p in products],
                "title": [p.title for p in products],
                "price": [p.price for p in products],
                "url": [p.url for p in products],
                "cover_image_url": [p.cover_image_url for p in products],
            }
        )
        .set_index("observation_dt")
        .drop_duplicates(keep="last")
    )


def _form_symbol(sku: str, store: Store) -> str:
    return f"{store.value}/{sku}"


def parse_price_str(price_str: str) -> Union[float, np.float64]:
    price_str = price_str.strip().rstrip()
    dot_locs = [pos for pos, char in enumerate(price_str, 0) if char == "."]
    comma_locs = [pos for pos, char in enumerate(price_str, 0) if char == ","]
    if len(dot_locs) > 1:
        raise ValueError("Multiple .")
    elif len(comma_locs) == 1 and not dot_locs:
        if comma_locs[0] == len(price_str) - 3:
            price_str = price_str.replace(",", ".")
    elif len(dot_locs) == 1 and len(comma_locs) >= 1:
        if any(idx > dot_locs[0] for idx in comma_locs):
            raise ValueError("Commas after dots unsupported")
    price_str = price_str.replace(" ", "")
    try:
        return float(price_str.replace("£", "").replace(",", "").replace("$", ""))
    except ValueError:
        return np.nan


StoreCache = Dict[Any, Any]


@dataclass
class PreownedProductFetcher:
    store: Store
    url_template: str
    find_num_pages_parser: Callable[[BeautifulSoup], int]
    fetch_page_parser: Callable[[BeautifulSoup, datetime, StoreCache], Tuple[List[ProductObservation], StoreCache]]
    store_cache: StoreCache = field(default_factory=dict)

    @staticmethod
    def _retrieve_url(url: str) -> BeautifulSoup:
        res = httpx.get(url, follow_redirects=True)
        assert res.status_code == 200, (res, url)
        return BeautifulSoup(res.content, "html.parser")

    def _retrieve_page(self, page_id: int) -> BeautifulSoup:
        return PreownedProductFetcher._retrieve_url(self.url_template.format(page_id=page_id))

    @task(retries=3, retry_delay_seconds=15, tags=["preowned_max_calls"])
    def find_number_of_pages(self) -> int:
        page = self._retrieve_page(1)
        return self.find_num_pages_parser(page)

    @task(retries=3, retry_delay_seconds=15, tags=["preowned_max_calls"])
    def fetch_page(self, page_id: int, observation_dt: datetime) -> Tuple[List[ProductObservation], StoreCache]:
        page = self._retrieve_page(page_id)
        return self.fetch_page_parser(page, observation_dt, self.store_cache)

    def gather_products(self, observation_dt: datetime) -> List[ProductObservation]:
        logger = get_run_logger()
        num_pages = self.find_number_of_pages(self)
        logger.info("Identified %s page(s)", num_pages)
        futures = [self.fetch_page.submit(self, page_id, observation_dt) for page_id in range(1, num_pages + 1)]
        products = []
        for future in futures:
            new_products, new_cache = future.result()
            products.extend(new_products)
            self.store_cache.update(new_cache)
        logger.info("Loaded %s product(s)", len(products))
        return products

    @flow(task_runner=ConcurrentTaskRunner(), name="Fetch Preowned Products")
    def run_loader(self) -> None:
        _, _, lib = setup_app("preowned", AppConfig, use_prefect=True)
        assert lib is not None
        cache_sym = f"_cache_{self.store.value}"
        self.store_cache = lib.read(cache_sym).data if cache_sym in lib.list_symbols() else self.store_cache
        logger = get_run_logger()
        logger.info("Cache Initialised with %s value(s)", len(self.store_cache))

        observation_dt = datetime.utcnow()
        products = self.gather_products(observation_dt)
        products_by_sku = defaultdict(list)
        # paranoia against duplicate results
        for p in products:
            products_by_sku[p.sku].append(p)
            if len(products_by_sku[p.sku]) > 1:
                logger.warning("Duplicate product observations - %s", products_by_sku[p.sku])

        symbol_df = [
            (_form_symbol(sku, self.store), _product_observation_to_df(observations))
            for sku, observations in products_by_sku.items()
        ]
        snap = LoaderSnapshot(
            pd.concat(df for _, df in symbol_df).sort_index(),
            observation_dt,
            additional_keys={"store": self.store.value},
            metadata={"observation_dt": observation_dt, "store": self.store.value},
        )

        logger.info("Writing output")
        lib.write_pickle(cache_sym, self.store_cache)
        logger.info("Cache written with %s value(s)", len(self.store_cache))
        write_snapshot(lib, snap)
        write_spliced_symbols(lib, symbol_df)

        logger.info("Defragmenting symbols if required")
        for symbol, _ in symbol_df:
            defragment_symbol(lib, symbol)
