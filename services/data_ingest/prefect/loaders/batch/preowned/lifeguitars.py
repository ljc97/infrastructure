from datetime import datetime
from typing import List, Tuple

import httpx
from bs4 import BeautifulSoup
from prefect import flow
from prefect.task_runners import ConcurrentTaskRunner

from loaders.batch.preowned.common import (
    PreownedProductFetcher,
    ProductObservation,
    Store,
    StoreCache,
    parse_price_str,
)


def _fetch_sku(url: str) -> str:
    res = httpx.get(url)
    assert res.status_code == 200, (res, url)
    page = BeautifulSoup(res.content, "html.parser")
    form = page.find("form", id="AddToCartForm-product-template")
    assert form is not None and hasattr(form, "findAll")
    options = form.findAll("option")
    for option in options:
        if hasattr(option, "data-sku") and hasattr(option, "value"):
            sku = option["value"]
            assert isinstance(sku, str)
            return sku
    raise ValueError()


def _parse_num_pages(page: BeautifulSoup) -> int:
    page_buttons = page.find_all("span", class_="page")
    return max(int(page.text) for page in page_buttons) if page_buttons else 1


def _get_products_from_page(
    page: BeautifulSoup, observation_dt: datetime, store_cache: StoreCache
) -> Tuple[List[ProductObservation], StoreCache]:
    products = []
    product_grids = (
        page.find("main", class_="main-content")  # type: ignore
        .find("div", class_="grid grid--no-gutters grid--uniform")
        .findAll("div", class_="grid__item")
    )
    for grid in product_grids:
        if grid.find("div", class_="product-card__availability") is not None:
            continue  # skip not avail.
        url = "https://lifeguitars.co.uk" + grid.find("a", class_="product-card")["href"]
        title = grid.find("div", class_="product-card__name").text
        price_text = grid.find("div", class_="product-card__price").text.replace("Regular price", "")
        price = parse_price_str(price_text)
        cover_image_url = "https:" + grid.find("img")["data-src"]
        sku = _fetch_sku(url) if (url, "sku") not in store_cache else store_cache[(url, "sku")]
        store_cache[(url, "sku")] = sku

        products.append(ProductObservation(Store.LIFEGUITARS, observation_dt, sku, title, price, url, cover_image_url))
    return products, store_cache


URL_TEMPLATES = [
    (
        "https://lifeguitars.co.uk/collections/used-electric-guitars?page={page_id}",
        _parse_num_pages,
        _get_products_from_page,
    ),
    (
        "https://lifeguitars.co.uk/collections/used-acoustic-guitars?page={page_id}",
        _parse_num_pages,
        _get_products_from_page,
    ),
    (
        "https://lifeguitars.co.uk/collections/used-bass-guitars?page={page_id}",
        _parse_num_pages,
        _get_products_from_page,
    ),
]


@flow(task_runner=ConcurrentTaskRunner())
def fetch_lifeguitars_products() -> None:
    for url, page_num, product in URL_TEMPLATES:
        fetcher = PreownedProductFetcher(Store.LIFEGUITARS, url, page_num, product)
        fetcher.run_loader(fetcher)


if __name__ == "__main__":
    fetch_lifeguitars_products()
