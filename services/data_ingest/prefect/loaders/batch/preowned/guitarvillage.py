from datetime import datetime
from typing import List, Tuple

import httpx
from bs4 import BeautifulSoup
from prefect import flow
from prefect.task_runners import ConcurrentTaskRunner

from loaders.batch.preowned.common import (
    PreownedProductFetcher,
    ProductObservation,
    Store,
    StoreCache,
    parse_price_str,
)

URL_TEMPLATE = "https://guitarvillage.co.uk/categories/pre-owned/page/{page_id}"


def _fetch_sku(url: str) -> str:
    res = httpx.get(url)
    assert res.status_code in (200, 500), (res, url)
    page = BeautifulSoup(res.content, "html.parser")
    sku_item = page.find("span", class_="sku")
    assert sku_item is not None and hasattr(sku_item, "text"), sku_item
    return sku_item.text


def _parse_num_pages(page: BeautifulSoup) -> int:
    page_numbers = page.findAll("span", class_="page-numbers") + page.findAll("a", class_="page-numbers")
    return max(int(num.text) for num in page_numbers if num and num.text and num.text.strip())


def _get_products_from_page(
    page: BeautifulSoup, observation_dt: datetime, store_cache: StoreCache
) -> Tuple[List[ProductObservation], StoreCache]:
    product_grids = page.find_all("div", class_="product")
    products = []
    for grid in product_grids:
        url = grid.find("a", class_="woocommerce-LoopProduct-link")["href"]
        name = grid.find("h3", class_="woocommerce-loop-product__title").text
        price_text = grid.find("span", class_="woocommerce-Price-amount amount").text
        price = parse_price_str(price_text)
        cover_image_url = grid.find("picture").find("img")["src"]
        sku = _fetch_sku(url) if (url, "sku") not in store_cache else store_cache[(url, "sku")]
        store_cache[(url, "sku")] = sku
        products.append(ProductObservation(Store.GUITARVILLAGE, observation_dt, sku, name, price, url, cover_image_url))
    return (products, store_cache)


@flow(task_runner=ConcurrentTaskRunner())
def fetch_guitarvillage_products() -> None:
    fetcher = PreownedProductFetcher(Store.GUITARVILLAGE, URL_TEMPLATE, _parse_num_pages, _get_products_from_page)
    fetcher.run_loader(fetcher)


if __name__ == "__main__":
    fetch_guitarvillage_products()
