from datetime import datetime
from typing import List, Tuple

import httpx
from bs4 import BeautifulSoup
from prefect import flow
from prefect.task_runners import ConcurrentTaskRunner

from loaders.batch.preowned.common import (
    PreownedProductFetcher,
    ProductObservation,
    Store,
    StoreCache,
    parse_price_str,
)

URL_TEMPLATE = "https://www.giggear.co.uk/secondhand/?0_0=0&newppp=100000"


def _parse_num_pages(page: BeautifulSoup) -> int:
    return 1


def _fetch_sku(url: str) -> str:
    res = httpx.get(url)
    assert res.status_code == 200, (res, url)
    page = BeautifulSoup(res.content, "html.parser")
    sku_item = page.find("div", class_="listing-sku")
    assert sku_item is not None and hasattr(sku_item, "text"), sku_item
    return sku_item.text.replace("SKU: ", "")


def _get_products_from_page(
    page: BeautifulSoup, observation_dt: datetime, store_cache: StoreCache
) -> Tuple[List[ProductObservation], StoreCache]:
    product_grids = page.find_all("div", class_="product-grid--item")

    products = []
    for grid in product_grids:
        product_title = grid.find("a", class_="product--title")
        product_name = product_title.text.strip().replace("SECONDHAND", "")
        link = "https://www.giggear.co.uk{path}".format(path=product_title.get("href"))

        product_price_text = grid.find("span", class_="product-price-web").text.strip()
        price = parse_price_str(product_price_text)
        cover_image_url = grid.find("img").get("src")
        sku = _fetch_sku(link) if (link, "sku") not in store_cache else store_cache[(link, "sku")]
        store_cache[(link, "sku")] = sku

        products.append(
            ProductObservation(Store.GIGGEAR, observation_dt, sku, product_name, price, link, cover_image_url)
        )
    return (products, store_cache)


@flow(task_runner=ConcurrentTaskRunner())
def fetch_giggear_products() -> None:
    fetcher = PreownedProductFetcher(Store.GIGGEAR, URL_TEMPLATE, _parse_num_pages, _get_products_from_page)
    fetcher.run_loader(fetcher)


if __name__ == "__main__":
    fetch_giggear_products()
