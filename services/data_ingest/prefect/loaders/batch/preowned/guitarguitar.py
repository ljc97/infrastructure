import re
from datetime import datetime
from typing import List, Tuple

from bs4 import BeautifulSoup
from prefect import flow
from prefect.task_runners import ConcurrentTaskRunner

from loaders.batch.preowned.common import (
    PreownedProductFetcher,
    ProductObservation,
    Store,
    StoreCache,
    parse_price_str,
)

URL_TEMPLATE = "https://www.guitarguitar.co.uk/pre-owned/page-{page_id}/?Ordering=2&MinPrice=&MaxPrice="
SKU_REGEX = r"https:\/\/www\.guitarguitar\.co\.uk\/product\/([A-Za-z]{2,5}[0-9]{10,24}(|\-[0-9]{5,24}))\-\-.*"


def _parse_num_pages(page: BeautifulSoup) -> int:
    current_page = page.find("div", class_="current-page")
    assert current_page is not None and hasattr(current_page, "text")
    pages_text = current_page.text
    return int(pages_text[pages_text.find(" of ") + len(" of ") :])


def _get_products_from_page(
    page: BeautifulSoup, observation_dt: datetime, store_cache: StoreCache
) -> Tuple[List[ProductObservation], StoreCache]:
    product_grids = page.find_all("a", class_="product")

    products = []
    for grid in product_grids:
        product_info = grid.find("div", class_="product-content-wrapper")
        product_name = product_info.find("h3").text.strip().replace(" (Pre-Owned)", "")
        link = "https://www.guitarguitar.co.uk{path}".format(path=grid.get("href"))

        product_price_text = product_info.find("span", class_="product-main-price").text.strip()
        cover_image_url = grid.find("img").get("data-src")
        sku_search = re.search(SKU_REGEX, link)
        assert sku_search is not None, (link, sku_search)
        sku = sku_search.group(1)

        products.append(
            ProductObservation(
                Store.GUITARGUITAR,
                observation_dt,
                sku,
                product_name,
                parse_price_str(product_price_text),
                link,
                cover_image_url,
            )
        )

    return (products, store_cache)


@flow(task_runner=ConcurrentTaskRunner())
def fetch_guitarguitar_products() -> None:
    fetcher = PreownedProductFetcher(Store.GUITARGUITAR, URL_TEMPLATE, _parse_num_pages, _get_products_from_page)
    fetcher.run_loader(fetcher)


if __name__ == "__main__":
    fetch_guitarguitar_products()
