import re
from datetime import datetime
from typing import List, Tuple

from bs4 import BeautifulSoup
from prefect import flow
from prefect.task_runners import ConcurrentTaskRunner

from loaders.batch.preowned.common import (
    PreownedProductFetcher,
    ProductObservation,
    Store,
    StoreCache,
    parse_price_str,
)

URL_TEMPLATE = r"https://www.andertons.co.uk/second-hand?pageSize=48&pageNumber={page_id}&facet=x_inStock%3A%28%7B0+TO+*%7D%29"  # noqa: E501
UPN_REGEX = r"https:\/\/andertons-productimages.imgix.net\/([A-Z]+\-[A-Z0-9a-z]+\-[A-Za-z0-9]+|[A-Z0-9]+\-[0-9]+|[A-Z]+\-[0-9]+\-[0-9]+|[A-Z0-9]{1,10})\-.*.(JPG|jpg|png|PNG).*"
BASE_URL = r"https://www.andertons.co.uk"


def _parse_num_pages(page: BeautifulSoup) -> int:
    page_links = page.findAll("a", class_="o-pagination__link")
    return max(int(link.text) for link in page_links if link.text not in ("Prev", "Next"))


def _get_products_from_page(
    page: BeautifulSoup, observation_dt: datetime, store_cache: StoreCache
) -> Tuple[List[ProductObservation], StoreCache]:
    tiles = page.find_all("div", class_="o-tile")

    products = []
    for tile in tiles:
        image = tile.find("img", class_="o-image__img")
        title = image["title"]
        upn_search = re.search(UPN_REGEX, image["src"])
        assert upn_search is not None, image["src"]
        price = parse_price_str(tile.find("span", class_="o-price").text)
        link = tile.find("a", class_="o-tile__link")
        products.append(
            ProductObservation(
                Store.ANDERTONS,
                observation_dt,
                upn_search.group(1),
                title,
                price,
                BASE_URL + link["href"],
                image["src"],
            )
        )

    return (products, store_cache)


@flow(task_runner=ConcurrentTaskRunner())
def fetch_andertons_products() -> None:
    fetcher = PreownedProductFetcher(Store.ANDERTONS, URL_TEMPLATE, _parse_num_pages, _get_products_from_page)
    fetcher.run_loader(fetcher)


if __name__ == "__main__":
    fetch_andertons_products()
