# Prefect

## Local Dev

```
prefect server start
prefect worker start --pool local

prefect deploy --all

prefect deployment run 'fetch-lcc-walls/lcc_walls'

# fixup for shell not having current code on pythonpath
nix-shell
export PYTHONPATH=$PWD:$PYTHONPATH
python3 loaders/..../....py
```

### Workflow

* Run everything as per above
* Flow Code Change
  * `prefect deploy --all`
* Package/Dep change
  * Exit Worker
  * Exit nix-shell
  * Enter nix-shell
  * Start Worker

## Server

```
PREFECT_API_URL="http://prefect.media.arietis.uk/api" python -c "from prefect.cli import app; app()" deploy --all
```