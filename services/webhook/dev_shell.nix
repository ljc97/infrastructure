{ pkgs }: pkgs.mkShell {
  buildInputs = with pkgs; [
    cargo
    cargo-rr
    cargo-edit
    protobuf
    grpcui
    nats-server
    nats-top
    k6
  ];
}
