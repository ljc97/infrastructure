use std::{env, path::PathBuf};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let out_dir = PathBuf::from(env::var("OUT_DIR").unwrap());
    let proto_dir = {
        match env::var("PROTO_PATH") {
            Ok(val) => PathBuf::from(val),
            _ => PathBuf::from("../proto"),
        }
    };
    let mut ingress_protos = proto_dir.clone();
    ingress_protos.push("ingress.proto");
    let mut api_protos = proto_dir.clone();
    api_protos.push("api.proto");

    tonic_build::configure()
        .file_descriptor_set_path(out_dir.join("ingress_descriptor.bin"))
        .compile(&[ingress_protos], &[proto_dir.clone()])
        .unwrap();
    tonic_build::configure()
        .file_descriptor_set_path(out_dir.join("webhook_api_descriptor.bin"))
        .compile(&[api_protos], &[proto_dir.clone()])
        .unwrap();
    Ok(())
}
