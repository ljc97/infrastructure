#[macro_use]
extern crate rocket;
use async_nats::Request;
use messages::{Header, Param, WebhookRequest};
use prost::Message;
use rand;
use retainer::Cache;
use rocket::data::{Data, ToByteUnit};
use rocket::tokio;
use rocket::State;
use rsa::pkcs1::DecodeRsaPublicKey;
use rsa::{Pkcs1v15Encrypt, PublicKey, RsaPublicKey};
use std::collections::HashMap;
use std::sync::Arc;
use std::time::Duration;
use tonic::transport::Channel;
use webhook_api::webhook_api_client::WebhookApiClient;
use webhook_api::{GetChannelInfoReq, GetChannelInfoResp};

pub mod messages {
    tonic::include_proto!("ingress");
}

pub mod webhook_api {
    tonic::include_proto!("webhook_api");
}

struct ChannelFetcher {
    api_client: WebhookApiClient<Channel>,
    cache: Arc<Cache<String, GetChannelInfoResp>>,
}

impl ChannelFetcher {
    pub fn new(api_client: WebhookApiClient<Channel>) -> ChannelFetcher {
        let fetcher = ChannelFetcher {
            api_client,
            cache: Arc::new(Cache::new()),
        };
        let clone = fetcher.cache.clone();
        let _monitor =
            tokio::spawn(async move { clone.monitor(16, 0.25, Duration::from_secs(5)).await });

        fetcher
    }

    pub async fn get_channel(&self, channel_id: &str) -> GetChannelInfoResp {
        let clone = self.cache.clone();

        let cached_v = clone.get(&(channel_id.clone()).to_string()).await;
        if cached_v.is_some() {
            return cached_v.unwrap().value().clone();
        }

        let mut api = self.api_client.clone();
        let channel_info = api
            .get_channel(tonic::Request::new(GetChannelInfoReq {
                channel: channel_id.to_string(),
            }))
            .await
            .unwrap()
            .get_ref()
            .clone();

        clone
            .insert(
                (channel_id.clone()).to_string(),
                channel_info.clone(),
                Duration::from_secs(30),
            )
            .await;
        channel_info
    }
}

#[post("/ingress/<channel_id>?<params..>", data = "<data>")]
async fn index(
    nc: &State<async_nats::Client>,
    channel_fetcher: &State<ChannelFetcher>,
    channel_id: &str,
    params: HashMap<&str, &str>,
    data: Data<'_>,
) -> std::io::Result<()> {
    let channel_info = channel_fetcher.get_channel(channel_id).await;
    let public_key = RsaPublicKey::from_pkcs1_pem(&channel_info.pub_key).unwrap();
    let mut body_buf = Vec::new();
    data.open(64.kibibytes()).stream_to(&mut body_buf).await?;
    let message = WebhookRequest {
        channel: channel_id.to_string(),
        headers: vec![],
        params: params
            .iter()
            .map(|(k, v)| Param {
                name: k.to_string(),
                value: v.to_string(),
            })
            .collect(),
        body: body_buf,
    };

    let enc_data = {
        let mut rng = rand::thread_rng();
        let mut buf = vec![];
        message.encode(&mut buf).unwrap();
        public_key.encrypt(&mut rng, Pkcs1v15Encrypt, &buf).unwrap()
    };

    match nc
        .send_request(
            channel_id.to_string(),
            Request::new()
                .payload(enc_data.into())
                .timeout(Some(std::time::Duration::from_secs(2))),
        )
        .await
    {
        Err(err) => (), //println!("error {:?}", err),
        Ok(_) => todo!(),
    }

    Ok(())
}

#[rocket::main]
async fn main() -> Result<(), rocket::Error> {
    // console_subscriber::init();
    let nc = async_nats::connect("127.0.0.1:4222").await.unwrap();
    let mut api_client = WebhookApiClient::connect("http://127.0.0.1:50051")
        .await
        .unwrap();
    let fetcher = ChannelFetcher::new(api_client);

    let _rocket = rocket::build()
        .manage(nc)
        .manage(fetcher)
        .mount("/", routes![index])
        .launch()
        .await?;

    Ok(())
}
