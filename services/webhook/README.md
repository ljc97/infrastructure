# Webhook

## Basic Arch

```mermaid
graph TD
    external_service-->|webhook|ingress;
    ingress-->|fetch channel info|api;
    api-->ingress;
    api-->|fetch channel info|postgres;
    ingress-->|push encrypted proto|nats;
    client-->|login and subscribe|egress
    egress-->|messages on channel|client
    client-->|decrypt, decode, forward|local_app
    nats-->|publish encrypted messages|egress
    egress-->|channel subscriptions|nats
    
    classDef new_svc fill:#b7410e,stroke:#333,stroke-width:4px;
    class ingress,api,client,egress new_svc;

```

## Invariants
* Multi subscription will cause undesired behaviour
* Messages will be published using request/reply semantics with no persistence
  * Egress must respond within a reasonable timeframe
  * Can utilise nats `no_responder` messages to detect 0 subs
* Ingress must support mirroring of
  * Path
  * Parameters
  * Specific Headers
* Ingress must allow CIDR filtering
* API to act as the SOR for channel information, pub keys, CIDRs etc
* Ingress to cache channel information retrieved from the API
* Performance to be measured at ~10k/s on:
  * Single Channel Load
  * Random Spread Load
  * Non Existent Channel Load
* Body is to be accepted as bytes, but with some limited size (64KB?)

## Open Questions
* Once egress has ack'd a message and replied - it will not be resent
  * Any failure between here, the client app, the local server will cause the message to be dropped, but 200'd to the third party
  * Alternative creates coupling from ingress->nats->egress->client->local_server
    * Desirable for consistency, but not performance - testing required here, compromise might be message persistence instead of request/reply