import http from 'k6/http';
import { check } from 'k6';
import { randomString } from 'https://jslib.k6.io/k6-utils/1.2.0/index.js'


export const options = {
  discardResponseBodies: true,
  scenarios: {
    contacts: {
      executor: 'constant-arrival-rate',
      duration: '30s',
      rate: 1000,
      timeUnit: '1s',
      preAllocatedVUs: 2,
      maxVUs: 1000,
    },
  },
};

export default function () {
//    const random_channel = randomString(32);
    const random_channel = "abcd";
  const res = http.post(
      'http://127.0.0.1:8000/ingress/' + random_channel,
      JSON.stringify({
        "some_payload": {
            "that_is": {
                "from_somewhere": [1,2,3]
            }
        }
      }),
      {
        headers: { 'Content-Type': 'application/json'}
      }
  );
  check(res, {
    'status is 200': (r) => r.status === 200,
  });
}