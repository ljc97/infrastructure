use tonic::{transport::Server, Request, Response, Status};

use webhook_api::webhook_api_server::{WebhookApi, WebhookApiServer};
use webhook_api::{GetChannelInfoReq, GetChannelInfoResp};

pub mod webhook_api {
    tonic::include_proto!("webhook_api");

    pub(crate) const FILE_DESCRIPTOR_SET: &[u8] =
        tonic::include_file_descriptor_set!("webhook_api_descriptor");
}

#[derive(Default)]
pub struct MyWebhookApi {}

#[tonic::async_trait]
impl WebhookApi for MyWebhookApi {
    async fn get_channel(
        &self,
        request: Request<GetChannelInfoReq>,
    ) -> Result<Response<GetChannelInfoResp>, Status> {
        println!("Got a request from {:?}", request.remote_addr());
        let reply = GetChannelInfoResp {
            channel: request.get_ref().channel.clone(),
            api_key: "".to_string(),
            username: "".to_string(),
            allowed_cidr: vec!["127.0.0.1/32".to_string()],
            headers_to_forward: vec![],
            pub_key: "-----BEGIN RSA PUBLIC KEY-----\r\nMIIBCgKCAQEApuDOqP244TZSVBkPTZR+DXBF40sE7qJSklTe+wUsImGunv4s1W4I\r\neUWVn8FUeGhx1Z5ug7P4eh1Hff8/AI4xUVDU5GQ1Php58RfsHMjCdmI5ahQsTnkc\r\nGsLS0+nfa9wrrPBE+1KoHOJPXIytp1+Hhli3okMk3yN9uZnOi0DvkJpwaDEYeMjm\r\nlodPn2Q433ifm9Kf+PFnQtgpiBDqp6Qzf+M9l+Kb8UayyDlLr4njR1OyO14ZNQHD\r\nN87oO7xwXirReTbtHklzPcoXUY9otoaKazx53lXCuiOZ6kSjwoBMm2BOZVHIs10+\r\nA6D4YUdd2qocmX2aNwtwfh2HWVaySo34XwIDAQAB\r\n-----END RSA PUBLIC KEY-----\r\n".to_string(),
        };
        Ok(Response::new(reply))
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let addr = "127.0.0.1:50051".parse().unwrap();
    let service = MyWebhookApi::default();
    let reflection_server = tonic_reflection::server::Builder::configure()
        .register_encoded_file_descriptor_set(webhook_api::FILE_DESCRIPTOR_SET)
        .build()?;
    println!("GreeterServer listening on {}", addr);

    Server::builder()
        .add_service(WebhookApiServer::new(service))
        .add_service(reflection_server)
        .serve(addr)
        .await?;

    Ok(())
}
