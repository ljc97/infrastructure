use std::{env, path::PathBuf};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let out_dir = PathBuf::from(env::var("OUT_DIR").unwrap());
    let proto_dir = {
        match env::var("PROTO_PATH") {
            Ok(val) => PathBuf::from(val),
            _ => PathBuf::from("../proto"),
        }
    };
    let mut protos = proto_dir.clone();
    protos.push("api.proto");

    tonic_build::configure()
        .file_descriptor_set_path(out_dir.join("webhook_api_descriptor.bin"))
        .compile(&[protos], &[proto_dir])
        .unwrap();
    Ok(())
}
