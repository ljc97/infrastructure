import json
from datetime import datetime

import freezegun
import pytest
from arcticdb.version_store.library import Library  # type: ignore
from flask import Flask
from pytest_unordered import unordered


@freezegun.freeze_time("2023-10-02T14:00:00Z")
def test_no_query(app: Flask) -> None:
    with pytest.raises(ValueError):
        app.test_client().get("/api/v1/query")


@freezegun.freeze_time("2023-10-02T14:00:00Z")
def test_single_symbol(app: Flask, library: Library) -> None:
    res = app.test_client().get("/api/v1/query?query=arctic_read{library='test_lib', symbol='simple', column='a'}")
    assert res.status_code == 200
    assert json.loads(res.text) == {
        "status": "success",
        "data": {
            "resultType": "vector",
            "result": [
                {
                    "metric": {"__name__": "arctic_read", "symbol": "simple", "library": "test_lib"},
                    "value": [datetime(2023, 10, 3, tzinfo=None).timestamp(), "2"],
                }
            ],
        },
    }


@freezegun.freeze_time("2023-10-02T14:00:00Z")
def test_multi_symbol(app: Flask, library: Library) -> None:
    res = app.test_client().get("/api/v1/query?query=arctic_read{library='test_lib', column='a'}")
    assert res.status_code == 200
    assert json.loads(res.text) == {
        "data": {
            "resultType": "vector",
            "result": unordered(
                [
                    {
                        "metric": {"__name__": "arctic_read", "symbol": "simple", "library": "test_lib"},
                        "value": [datetime(2023, 10, 3, 0, 0, 0, tzinfo=None).timestamp(), "2"],
                    },
                    {
                        "metric": {"__name__": "arctic_read", "symbol": "simple_clone", "library": "test_lib"},
                        "value": [datetime(2023, 10, 3, 0, 0, 0, tzinfo=None).timestamp(), "2"],
                    },
                ]
            ),
        },
        "status": "success",
    }
