from datetime import datetime
from typing import Dict, List, Optional, Set, Tuple

import freezegun
import polars as pl
import pytest
from arcticdb.version_store.library import Library  # type: ignore
from promql_parser import Matcher, MatchOp, parse  # type: ignore

from arctic2prometheus.utils.query import (
    RangeQueryContext,
    _handle_vector_range,
    are_matchers_valid,
    handle_symbol_match,
    render_results,
    resolve_ast,
)

SYMBOL_LIST = ("A", "B", "C", "D.E", "FGH")


# dirty hack to acquire read-only, no-constructor pyo3 based classes
def _build_matcher(op: MatchOp, field: str, target: str) -> Matcher:
    match op:
        case MatchOp.Equal:
            op_str = "="
        case MatchOp.NotEqual:
            op_str = "!="
        case MatchOp.Re:
            op_str = "=~"
        case MatchOp.NotRe:
            op_str = "!~"
        case _:
            assert False
    query = f"test{{{field}{op_str}'{target}'}}"
    return next(parse(query).label_matchers.__iter__())


@pytest.mark.parametrize(
    "symbols, matcher, expected",
    [
        (tuple(), None, []),
        (SYMBOL_LIST, None, list(SYMBOL_LIST)),
        (tuple(), _build_matcher(MatchOp.Equal, "symbol", "A"), []),
        (SYMBOL_LIST, _build_matcher(MatchOp.Equal, "symbol", "A"), ["A"]),
        (SYMBOL_LIST, _build_matcher(MatchOp.NotEqual, "symbol", "A"), ["B", "C", "D.E", "FGH"]),
        (SYMBOL_LIST, _build_matcher(MatchOp.Re, "symbol", ".*\\\\..*"), ["D.E"]),
        (SYMBOL_LIST, _build_matcher(MatchOp.NotRe, "symbol", ".*\\\\..*"), ["A", "B", "C", "FGH"]),
    ],
)
def test_handle_symbol_match(symbols: Tuple[str], matcher: Optional[Matcher], expected: List[str]) -> None:
    assert expected == handle_symbol_match(symbols, matcher)


@pytest.mark.parametrize(
    "matchers, valid",
    [
        (set(), False),
        ({_build_matcher(MatchOp.Equal, "library", "test_lib")}, False),
        ({_build_matcher(MatchOp.Equal, "library", "test_lib"), _build_matcher(MatchOp.Equal, "column", "A")}, True),
        ({_build_matcher(MatchOp.Equal, "library", "test_lib"), _build_matcher(MatchOp.Re, "column", "A")}, False),
        (
            {
                _build_matcher(MatchOp.Equal, "library", "test_lib"),
                _build_matcher(MatchOp.Equal, "column", "A"),
                _build_matcher(MatchOp.Re, "symbol", ".*"),
            },
            True,
        ),
    ],
)
def test_are_matchers_valid(matchers: Set[Matcher], valid: bool) -> None:
    assert valid == are_matchers_valid(matchers)


@freezegun.freeze_time("2023-10-02T14:00:00Z")
def test_handle_vector_range(libraries: Dict[str, Library]) -> None:
    ast = parse("arctic_read{library='test_lib', column='a'}")
    df = _handle_vector_range(
        ast,
        RangeQueryContext(start=datetime(2023, 10, 1, 0, 0, 0), end=datetime(2023, 10, 3, 23, 59, 59), step=None),
        libraries,
    ).collect()
    assert df.frame_equal(
        pl.DataFrame(
            {
                "idx": [
                    datetime(2023, 10, 1, 0, 0, 0),
                    datetime(2023, 10, 2, 0, 0, 0),
                    datetime(2023, 10, 3, 0, 0, 0),
                    datetime(2023, 10, 1, 0, 0, 0),
                    datetime(2023, 10, 2, 0, 0, 0),
                    datetime(2023, 10, 3, 0, 0, 0),
                ],
                "value": [0, 1, 2, 0, 1, 2],
                "symbol": ["simple_clone"] * 3 + ["simple"] * 3,
                "library": ["test_lib"] * 6,
            }
        )
    )


@freezegun.freeze_time("2023-10-02T14:00:00Z")
def test_resolve_ast_vector(libraries: Dict[str, Library]) -> None:
    ast = parse("arctic_read{library='test_lib', column='a'}")
    df = resolve_ast(
        ast,
        RangeQueryContext(start=datetime(2023, 10, 1, 0, 0, 0), end=datetime(2023, 10, 3, 23, 59, 59), step=None),
        libraries,
    ).collect()
    assert df.frame_equal(
        pl.DataFrame(
            {
                "idx": [
                    datetime(2023, 10, 1, 0, 0, 0),
                    datetime(2023, 10, 2, 0, 0, 0),
                    datetime(2023, 10, 3, 0, 0, 0),
                    datetime(2023, 10, 1, 0, 0, 0),
                    datetime(2023, 10, 2, 0, 0, 0),
                    datetime(2023, 10, 3, 0, 0, 0),
                ],
                "value": [0, 1, 2, 0, 1, 2],
                "symbol": ["simple_clone"] * 3 + ["simple"] * 3,
                "library": ["test_lib"] * 6,
            }
        )
    )


@freezegun.freeze_time("2023-10-02T14:00:00Z")
def test_handle_vector_range_offset_1d(libraries: Dict[str, Library]) -> None:
    ast = parse("arctic_read{library='test_lib', column='a'} offset 1d")
    df = _handle_vector_range(
        ast,
        RangeQueryContext(start=datetime(2023, 10, 1, 0, 0, 0), end=datetime(2023, 10, 3, 23, 59, 59), step=None),
        libraries,
    ).collect()
    # showing the points from the 1st and 2nd, on the 2nd and 3rd
    assert df.frame_equal(
        pl.DataFrame(
            {
                "idx": [
                    datetime(2023, 10, 2, 0, 0, 0),
                    datetime(2023, 10, 3, 0, 0, 0),
                    datetime(2023, 10, 2, 0, 0, 0),
                    datetime(2023, 10, 3, 0, 0, 0),
                ],
                "value": [0, 1, 0, 1],
                "symbol": ["simple_clone"] * 2 + ["simple"] * 2,
                "library": ["test_lib"] * 4,
            }
        )
    )


@freezegun.freeze_time("2023-10-02T14:00:00Z")
def test_resolve_ast_vector_op_max(libraries: Dict[str, Library]) -> None:
    ast = parse("max(arctic_read{library='test_lib', column='a'})")
    df = resolve_ast(
        ast,
        RangeQueryContext(start=datetime(2023, 10, 1, 0, 0, 0), end=datetime(2023, 10, 3, 23, 59, 59), step=None),
        libraries,
    ).collect()
    assert df.frame_equal(
        pl.DataFrame(
            {
                "idx": [
                    datetime(2023, 10, 1, 0, 0, 0),
                    datetime(2023, 10, 2, 0, 0, 0),
                    datetime(2023, 10, 3, 0, 0, 0),
                    datetime(2023, 10, 1, 0, 0, 0),
                    datetime(2023, 10, 2, 0, 0, 0),
                    datetime(2023, 10, 3, 0, 0, 0),
                ],
                "value": [2, 2, 2, 2, 2, 2],
                "symbol": ["simple_clone"] * 3 + ["simple"] * 3,
                "library": ["test_lib"] * 6,
            }
        )
    )


@freezegun.freeze_time("2023-10-02T14:00:00Z")
def test_resolve_ast_vector_op_avg(libraries: Dict[str, Library]) -> None:
    ast = parse("avg(arctic_read{library='test_lib', column='a'})")
    df = resolve_ast(
        ast,
        RangeQueryContext(start=datetime(2023, 10, 1, 0, 0, 0), end=datetime(2023, 10, 3, 23, 59, 59), step=None),
        libraries,
    ).collect()
    assert df.frame_equal(
        pl.DataFrame(
            {
                "idx": [
                    datetime(2023, 10, 1, 0, 0, 0),
                    datetime(2023, 10, 2, 0, 0, 0),
                    datetime(2023, 10, 3, 0, 0, 0),
                    datetime(2023, 10, 1, 0, 0, 0),
                    datetime(2023, 10, 2, 0, 0, 0),
                    datetime(2023, 10, 3, 0, 0, 0),
                ],
                "value": [1, 1, 1, 1, 1, 1],
                "symbol": ["simple_clone"] * 3 + ["simple"] * 3,
                "library": ["test_lib"] * 6,
            }
        )
    )


@freezegun.freeze_time("2023-10-02T14:00:00Z")
def test_resolve_ast_vector_op_min(libraries: Dict[str, Library]) -> None:
    ast = parse("min(arctic_read{library='test_lib', column='a'})")
    df = resolve_ast(
        ast,
        RangeQueryContext(start=datetime(2023, 10, 1, 0, 0, 0), end=datetime(2023, 10, 3, 23, 59, 59), step=None),
        libraries,
    ).collect()
    assert df.frame_equal(
        pl.DataFrame(
            {
                "idx": [
                    datetime(2023, 10, 1, 0, 0, 0),
                    datetime(2023, 10, 2, 0, 0, 0),
                    datetime(2023, 10, 3, 0, 0, 0),
                    datetime(2023, 10, 1, 0, 0, 0),
                    datetime(2023, 10, 2, 0, 0, 0),
                    datetime(2023, 10, 3, 0, 0, 0),
                ],
                "value": [0, 0, 0, 0, 0, 0],
                "symbol": ["simple_clone"] * 3 + ["simple"] * 3,
                "library": ["test_lib"] * 6,
            }
        )
    )


@freezegun.freeze_time("2023-10-02T14:00:00Z")
def test_resolve_ast_vector_op_max_sub_min(libraries: Dict[str, Library]) -> None:
    ast = parse(
        "max(arctic_read{library='test_lib', column='a'}) - max(arctic_read{library='test_lib', column='a'} offset 1d)"
    )
    df = resolve_ast(
        ast,
        RangeQueryContext(start=datetime(2023, 10, 1, 0, 0, 0), end=datetime(2023, 10, 3, 23, 59, 59), step=None),
        libraries,
    ).collect()
    assert df.frame_equal(
        pl.DataFrame(
            {
                "idx": [
                    datetime(2023, 10, 2, 0, 0, 0),
                    datetime(2023, 10, 3, 0, 0, 0),
                    datetime(2023, 10, 2, 0, 0, 0),
                    datetime(2023, 10, 3, 0, 0, 0),
                ],
                "value": [1, 1, 1, 1],
                "symbol": ["simple_clone"] * 2 + ["simple"] * 2,
                "library": ["test_lib"] * 4,
            }
        )
    )


@freezegun.freeze_time("2023-10-02T14:00:00Z")
def test_resolve_ast_constant_only(libraries: Dict[str, Library]) -> None:
    ast = parse("1")
    df = resolve_ast(
        ast,
        RangeQueryContext(start=datetime(2023, 10, 1, 0, 0, 0), end=datetime(2023, 10, 1, 0, 1, 0), step=None),
        libraries,
    ).collect()
    assert df.frame_equal(
        pl.DataFrame(
            {
                "idx": [datetime(2023, 10, 1, 0, 0, 0), datetime(2023, 10, 1, 0, 0, 30)],
                "library": [None] * 2,
                "symbol": [None] * 2,
                "value": [1.0, 1.0],
            }
        )
    )


@freezegun.freeze_time("2023-10-02T14:00:00Z")
def test_resolve_ast_multiple_constants(libraries: Dict[str, Library]) -> None:
    ast = parse("1+3")
    df = resolve_ast(
        ast,
        RangeQueryContext(start=datetime(2023, 10, 1, 0, 0, 0), end=datetime(2023, 10, 1, 0, 1, 0), step=None),
        libraries,
    ).collect()
    assert df.frame_equal(
        pl.DataFrame(
            {
                "idx": [datetime(2023, 10, 1, 0, 0, 0), datetime(2023, 10, 1, 0, 0, 30)],
                "library": [None] * 2,
                "symbol": [None] * 2,
                "value": [4.0, 4.0],
            }
        )
    )


def test_render_results() -> None:
    df = pl.DataFrame(
        {
            "idx": [
                datetime(2023, 10, 2, 0, 0, 0, tzinfo=None),
                datetime(2023, 10, 3, 0, 0, 0, tzinfo=None),
                datetime(2023, 10, 2, 0, 0, 0, tzinfo=None),
                datetime(2023, 10, 3, 0, 0, 0, tzinfo=None),
            ],
            "value": [0, 1, 0, 1],
            "symbol": ["simple_clone"] * 2 + ["simple"] * 2,
            "library": ["test_lib"] * 4,
        }
    ).lazy()
    ctx = RangeQueryContext(
        start=datetime(2023, 10, 2, 0, 0, 0, tzinfo=None), end=datetime(2023, 10, 4, 0, 0, 0, tzinfo=None), step=None
    )
    results = render_results(df, ctx)
    assert results == {
        "status": "success",
        "data": {
            "resultType": "matrix",
            "result": [
                {
                    "metric": {"__name__": "arctic_read", "symbol": "simple_clone", "library": "test_lib"},
                    "values": [
                        [datetime(2023, 10, 2, 0, 0, 0, tzinfo=None).timestamp(), "0"],
                        [datetime(2023, 10, 3, 0, 0, 0, tzinfo=None).timestamp(), "1"],
                    ],
                },
                {
                    "metric": {"__name__": "arctic_read", "symbol": "simple", "library": "test_lib"},
                    "values": [
                        [datetime(2023, 10, 2, 0, 0, 0, tzinfo=None).timestamp(), "0"],
                        [datetime(2023, 10, 3, 0, 0, 0, tzinfo=None).timestamp(), "1"],
                    ],
                },
            ],
        },
    }


@freezegun.freeze_time("2023-10-02T14:00:00Z")
def test_resolve_ast_vector_op_max_sub_min_plus_constant(libraries: Dict[str, Library]) -> None:
    ast = parse(
        "max(arctic_read{library='test_lib', column='a'}) - max(arctic_read{library='test_lib', column='a'} offset 1d) + 3"
    )
    df = resolve_ast(
        ast,
        RangeQueryContext(start=datetime(2023, 10, 1, 0, 0, 0), end=datetime(2023, 10, 3, 23, 59, 59), step=None),
        libraries,
    ).collect()
    assert df.frame_equal(
        pl.DataFrame(
            {
                "idx": [
                    datetime(2023, 10, 2, 0, 0, 0),
                    datetime(2023, 10, 3, 0, 0, 0),
                    datetime(2023, 10, 2, 0, 0, 0),
                    datetime(2023, 10, 3, 0, 0, 0),
                ],
                "value": [4.0, 4.0, 4.0, 4.0],
                "symbol": ["simple_clone"] * 2 + ["simple"] * 2,
                "library": ["test_lib"] * 4,
            }
        )
    )
