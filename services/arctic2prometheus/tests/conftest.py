import tempfile
from datetime import datetime
from typing import Any, Dict, Generator

import pandas as pd
import pytest
from arcticdb import Arctic  # type: ignore
from arcticdb.version_store.library import Library  # type: ignore
from dateutil.relativedelta import relativedelta
from flask import Flask

from arctic2prometheus.main import create_app


def _ensure_lib_has_basic_data(library: Library) -> None:
    df = pd.DataFrame(
        {
            "a": [0, 1, 2],
            "b": [0, 1, 2],
            "idx": [datetime(2023, 10, 1, 0, 0, 0, tzinfo=None) + relativedelta(days=day) for day in range(0, 3)],
        }
    ).set_index("idx")
    library.write("simple", df)
    library.write("simple_clone", df)


@pytest.fixture(name="arctic_path", scope="session")
def _arctic_path() -> Generator[str, None, None]:
    with tempfile.TemporaryDirectory() as tmpdir:
        yield f"lmdb://{tmpdir}/arctic_db"


@pytest.fixture(name="library", scope="session")
def _get_arcticdb_lib(arctic_path: str) -> Generator[Library, None, None]:
    db = Arctic(arctic_path)
    if "test_lib" not in db.list_libraries():
        db.create_library("test_lib")
    lib = db.get_library("test_lib")
    for symbol in lib.list_symbols():
        lib.delete(symbol)
    _ensure_lib_has_basic_data(lib)
    yield lib


@pytest.fixture(name="libraries", scope="session")
def _get_libs(library: Library) -> Generator[Dict[str, Any], None, None]:
    yield {"test_lib": library}


@pytest.fixture()
def app(arctic_path: str) -> Generator[Flask, None, None]:
    app = create_app(arctic_path)
    app.config.update(
        {
            "TESTING": True,
        }
    )
    yield app
