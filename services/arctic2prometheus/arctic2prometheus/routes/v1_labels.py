from typing import Any, Dict

from flask import Blueprint

v1_labels = Blueprint("v1_labels", __name__, url_prefix="/api/v1/")


@v1_labels.route("/label/<name>/values", methods=["GET"])
def single(name: str) -> Dict[str, Any]:
    # print(name)
    # print(request.args)
    # print(request.form)
    return {"status": "success", "data": ["arctic_read"]}


@v1_labels.route("/labels", methods=["GET", "POST"])
def multi() -> Dict[str, Any]:
    # print(request.args)
    # print(request.form)
    return {"status": "success", "data": ["__name__", "library", "symbol"]}
