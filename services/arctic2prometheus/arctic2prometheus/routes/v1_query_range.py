import json
from datetime import datetime
from time import perf_counter
from typing import Optional

import promql_parser  # type: ignore
from dateutil.parser import parse
from flask import Blueprint, current_app, request

from arctic2prometheus.utils.query import RangeQueryContext, render_results, resolve_ast

v1_query_range = Blueprint("v1_query_range", __name__, url_prefix="/api/v1/query_range")


class CatchTime:  # TODO: remove
    def __enter__(self) -> "CatchTime":
        self.start = perf_counter()
        return self

    def __init__(self, name: str) -> None:
        self.name = name

    def __exit__(self, type: str, value: str, traceback: str) -> None:
        self.time = perf_counter() - self.start
        self.readout = f"{self.name} Time: {self.time:.3f} seconds"
        print(self.readout)


@v1_query_range.route("/", methods=["GET", "POST"])
def handler() -> str:
    with CatchTime("parsing"):
        args = request.args
        form = request.form

        is_post = request.method == "POST"
        query: Optional[str] = form.get("query", None) if is_post else args.get("query", None)
        if query is None:
            raise ValueError()
        ast = promql_parser.parse(query)

        start: Optional[str] = form.get("start", None) if is_post else args.get("start", None)
        end: Optional[str] = form.get("end", None) if is_post else args.get("end", None)
        form.get("step", None) if is_post else args.get("step", None)

        if start is None or end is None:
            raise ValueError()

        start_dt = parse(start) if "T" in start else datetime.fromtimestamp(float(start))
        end_dt = parse(end) if "T" in end else datetime.fromtimestamp(float(end))

    from arctic2prometheus.main import Arctic2PrometheusFlask

    assert isinstance(current_app, Arctic2PrometheusFlask)  # TODO: properly type current_app

    with CatchTime("resolve_ast"):
        ctx = RangeQueryContext(start=start_dt, end=end_dt, step=None)
        df = resolve_ast(ast, ctx, current_app.ARCTIC_LIBS)
    with CatchTime("render"):
        results = render_results(df, ctx)

    return json.dumps(results)
