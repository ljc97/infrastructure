import json
from datetime import datetime
from typing import Optional

import promql_parser  # type: ignore
from dateutil.parser import parse
from flask import Blueprint, current_app, request

from arctic2prometheus.utils.query import (
    InstantQueryContext,
    render_results,
    resolve_ast,
)

v1_query = Blueprint("v1_query", __name__, url_prefix="/api/v1/query")


@v1_query.route("/", methods=["GET", "POST"])
def handler() -> str:
    args = request.args
    form = request.form
    is_post = request.method == "POST"
    query: Optional[str] = form.get("query", None) if is_post else args.get("query", None)
    if query is None:
        raise ValueError()
    ast = promql_parser.parse(query)

    timestamp = form.get("timestamp", None) if is_post else args.get("timestamp", None)
    timestamp_dt = (
        (parse(timestamp) if "T" in timestamp else datetime.fromtimestamp(float(timestamp)))
        if timestamp is not None
        else None
    )
    form.get("timeout", None) if is_post else args.get("timeout", None)

    from arctic2prometheus.main import Arctic2PrometheusFlask

    assert isinstance(current_app, Arctic2PrometheusFlask)  # TODO: properly type current_app
    ctx = InstantQueryContext(timestamp=timestamp_dt)
    df = resolve_ast(ast, ctx, current_app.ARCTIC_LIBS)
    results = render_results(df, ctx)
    return json.dumps(results)
