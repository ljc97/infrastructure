from typing import TYPE_CHECKING, Any, Dict, Tuple

import cachetools
from arcticdb import Arctic  # type: ignore
from arcticdb.version_store.library import Library  # type: ignore
from flask import Flask

from arctic2prometheus.routes.v1_labels import v1_labels
from arctic2prometheus.routes.v1_query import v1_query
from arctic2prometheus.routes.v1_query_range import v1_query_range

if TYPE_CHECKING:
    current_app: "Arctic2PrometheusFlask"
else:
    from flask import current_app

    current_app = current_app


class Arctic2PrometheusFlask(Flask):
    ARCTIC_LIBS: Dict[str, Library] = {}
    ARCTIC_DB: Arctic

    def __init__(self, import_name: str, s3_url: str):
        super().__init__(import_name)
        self.ARCTIC_DB = Arctic(s3_url)
        for lib in self.ARCTIC_DB.list_libraries():
            self.ARCTIC_LIBS[lib] = self.ARCTIC_DB.get_library(lib)

    def get_arctic_library(self, name: str) -> Library:
        if name not in self.ARCTIC_LIBS:
            self.ARCTIC_LIBS[name] = self.ARCTIC_DB.get_library(name)
        return self.ARCTIC_LIBS[name]

    @cachetools.cached(cache=cachetools.TTLCache(maxsize=128, ttl=60))
    def get_symbols(self, name: str) -> Tuple[str, ...]:
        symbol_list = self.get_arctic_library(name).list_symbols()
        assert isinstance(symbol_list, list)
        return tuple(symbol_list)


def create_app(s3_url: str) -> Arctic2PrometheusFlask:
    app = Arctic2PrometheusFlask(__name__, s3_url)  # TODO: load from config
    app.url_map.strict_slashes = False
    app.config["JSON_SORT_KEYS"] = False

    app.register_blueprint(v1_query)
    app.register_blueprint(v1_query_range)
    app.register_blueprint(v1_labels)

    @app.get("/api/v1/status/buildinfo")
    def build_info() -> Dict[str, Any]:  # TODO: shhhh
        return {
            "status": "success",
            "data": {
                "version": "2.13.1",
                "revision": "cb7cbad5f9a2823a622aaa668833ca04f50a0ea7",
                "branch": "master",
                "buildUser": "julius@desktop",
                "buildDate": "20191102-16:19:59",
                "goVersion": "go1.13.1",
            },
        }

    return app
