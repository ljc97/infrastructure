import re
from dataclasses import dataclass
from datetime import datetime, timedelta
from typing import Any, Dict, List, Optional, Set, Tuple, Union

import cachetools
import polars as pl
import promql_parser  # type: ignore
from arcticdb import QueryBuilder  # type: ignore
from arcticdb.version_store.library import Library  # type: ignore
from promql_parser import (
    AggregateExpr,
    BinaryExpr,
    Expr,
    Matcher,
    MatchOp,
    ParenExpr,
    SubqueryExpr,
    UnaryExpr,
    VectorSelector,
)

AstType = Union[AggregateExpr, BinaryExpr, Expr, ParenExpr, SubqueryExpr, UnaryExpr, VectorSelector]
ACCEPTABLE_MATCHER_NAMES = {"library", "symbol", "column"}
ACCEPTABLE_MATCHER_OPS: Dict[str, List[MatchOp]] = {
    "library": [MatchOp.Equal],
    "column": [MatchOp.Equal],
    "symbol": [MatchOp.Equal, MatchOp.NotEqual, MatchOp.Re, MatchOp.NotRe],
}


@dataclass
class InstantQueryContext:
    timestamp: Optional[datetime]


@dataclass
class RangeQueryContext:
    start: datetime
    end: datetime
    step: Optional[timedelta]


QueryContext = Union[RangeQueryContext, InstantQueryContext]


@cachetools.cached(
    cache=cachetools.LRUCache(maxsize=128)
)  # TODO: handle invalidation of symbol_list at a lib level on refresh?
def handle_symbol_match(symbols: Tuple[str, ...], matcher: Optional[Matcher]) -> List[str]:
    if matcher is None:
        return list(symbols)
    match matcher.op:
        case MatchOp.Re:
            pattern = matcher.value.replace("\\\\", "\\")
            return [s for s in symbols if re.match(pattern, s)]
        case MatchOp.NotRe:
            pattern = matcher.value.replace("\\\\", "\\")
            return [s for s in symbols if not re.match(pattern, s)]
        case MatchOp.NotEqual:
            return [s for s in symbols if s != matcher.value]
        case MatchOp.Equal:
            return [s for s in symbols if s == matcher.value]
        case _:
            raise ValueError()


def are_matchers_valid(matchers: Set[Matcher]) -> bool:
    if (
        len(matchers) > 3 or len(matchers) < 2
    ):  # TODO: properly validate set, need (lib,col) rest opt, multiple matchers?
        return False

    for match in matchers:
        if match.name not in ACCEPTABLE_MATCHER_NAMES:
            return False
        if match.op not in ACCEPTABLE_MATCHER_OPS[match.name]:
            return False
    return True


def _handle_vector_instant(
    vector: VectorSelector, context: InstantQueryContext, libs: Dict[str, Library]
) -> pl.LazyFrame:
    if vector.name != "arctic_read":
        raise ValueError("vector not arctic_read")

    matchers = vector.label_matchers
    if not are_matchers_valid(matchers):
        raise ValueError("matchers invalid")

    lib_to_query = next((m for m in matchers if m.name == "library"), None)
    symbol_query = next((m for m in matchers if m.name == "symbol"), None)
    column_query = next((m for m in matchers if m.name == "column"), None)

    if lib_to_query is None or column_query is None:
        raise ValueError("no lib or column")

    # TODO: use cache
    lib = libs[lib_to_query.value]
    symbol_list: Tuple[str, ...] = tuple(lib.list_symbols())
    symbols = handle_symbol_match(symbol_list, symbol_query)
    if len(symbols) == 0:
        pass  # TODO: exit pre-dragons

    data = [lib.tail(symbol, n=1, as_of=context.timestamp) for symbol in symbols]
    # TODO: kind of hacky, convert each DF to polars, lazy, then slice and add symbol column, then concat
    slices: List[pl.LazyFrame] = [
        pl.from_pandas(data[idx].data.rename_axis("idx", axis=0), include_index=True)
        .lazy()
        .select([pl.col("idx"), pl.col(column_query.value).alias("value")])
        .with_columns(pl.lit(symbols[idx]).alias("symbol"), pl.lit(lib_to_query.value).alias("library"))
        for idx in range(0, len(data))
    ]
    return pl.concat(slices)


def _handle_vector_range(vector: VectorSelector, context: RangeQueryContext, libs: Dict[str, Library]) -> pl.LazyFrame:
    offset = vector.offset or timedelta(seconds=0)
    start = context.start - offset
    end = context.end - offset

    if vector.name != "arctic_read":
        raise ValueError("vector not arctic_read")

    matchers = vector.label_matchers
    if not are_matchers_valid(matchers):
        raise ValueError("matchers invalid")

    lib_to_query = next((m for m in matchers if m.name == "library"), None)
    symbol_query = next((m for m in matchers if m.name == "symbol"), None)
    column_query = next((m for m in matchers if m.name == "column"), None)

    if lib_to_query is None or column_query is None:
        raise ValueError("no lib or column")

    # TODO: use cache
    lib = libs[lib_to_query.value]
    symbol_list: Tuple[str, ...] = tuple(lib.list_symbols())
    symbols = handle_symbol_match(symbol_list, symbol_query)
    if len(symbols) == 0:
        pass  # TODO: exit pre-dragons

    q = QueryBuilder().date_range((start, end))
    # list of results, matching order of symbols
    # pivot this into a flat table of (timestamp, symbol, value)
    data = lib.read_batch(symbols=symbols, query_builder=q)
    assert len(data) == len(symbols)  # TODO: less paranoid

    # TODO: kind of hacky, convert each DF to polars, lazy, then slice and add symbol column, then concat
    slices: List[pl.LazyFrame] = [
        pl.from_pandas(data[idx].data.rename_axis("idx", axis=0), include_index=True)
        .lazy()
        .select([pl.col("idx") + offset, pl.col(column_query.value).alias("value")])
        .with_columns(pl.lit(symbols[idx]).alias("symbol"), pl.lit(lib_to_query.value).alias("library"))
        for idx in range(0, len(data))
    ]
    return pl.concat(slices)


def _resolve_ast(
    ast: AstType, context: QueryContext, libs: Dict[str, Library]
) -> Union[pl.LazyFrame, promql_parser.NumberLiteral]:
    match type(ast):
        case promql_parser.AggregateExpr:
            df = _resolve_ast(ast.expr, context, libs)
            match str(ast.op):
                case "max" | "min" | "avg":
                    intermediate = df.group_by(["library", "symbol"])
                    match str(ast.op):
                        case "min":
                            intermediate_agg = intermediate.min()
                        case "max":
                            intermediate_agg = intermediate.max()
                        case "avg":
                            intermediate_agg = intermediate.mean()
                        case _:
                            raise ValueError()
                    for data in intermediate_agg.collect().iter_rows(named=True):
                        symbol = data["symbol"]
                        value = data["value"]
                        library = data["library"]
                        df = df.with_columns(
                            value=pl.when((pl.col("symbol") == symbol) & (pl.col("library") == library))
                            .then(value)
                            .otherwise(pl.col("value"))
                        )
                    return df
                case _:
                    raise ValueError("unsupported agg. op")
        case promql_parser.BinaryExpr:
            lhs = _resolve_ast(ast.lhs, context, libs)
            rhs = _resolve_ast(ast.rhs, context, libs)
            if isinstance(lhs, promql_parser.NumberLiteral) and isinstance(rhs, promql_parser.NumberLiteral):
                # can't make these classes myself, screw re-classing them all
                return promql_parser.parse(f"{lhs.val + rhs.val}")
            elif isinstance(lhs, promql_parser.NumberLiteral) or isinstance(rhs, promql_parser.NumberLiteral):
                frame = lhs if isinstance(lhs, pl.LazyFrame) else rhs
                numeric = lhs if isinstance(lhs, promql_parser.NumberLiteral) else rhs
                match str(ast.op):
                    case "-":
                        return frame.with_columns(value=pl.col("value") - numeric.val)
                    case "+":
                        return frame.with_columns(value=pl.col("value") + numeric.val)
                    case "*":
                        return frame.with_columns(value=pl.col("value") * numeric.val)
                    case "/":
                        return frame.with_columns(value=pl.col("value") / numeric.val)
                    case _:
                        raise ValueError()
            join = lhs.join(rhs, on=["idx", "library", "symbol"])
            match str(ast.op):
                case "-":
                    return join.with_columns(value=pl.col("value") - pl.col("value_right")).drop("value_right")
                case "+":
                    return join.with_columns(value=pl.col("value") + pl.col("value_right")).drop("value_right")
                case "*":
                    return join.with_columns(value=pl.col("value") * pl.col("value_right")).drop("value_right")
                case "/":
                    return join.with_columns(value=pl.col("value") / pl.col("value_right")).drop("value_right")
                case _:
                    raise ValueError()
        case promql_parser.Expr:
            raise ValueError(ast)
        case promql_parser.ParenExpr:
            raise ValueError(ast)
        case promql_parser.SubqueryExpr:
            raise ValueError(ast)
        case promql_parser.UnaryExpr:
            raise ValueError(ast)
        case promql_parser.VectorSelector:
            if isinstance(context, RangeQueryContext):
                return _handle_vector_range(ast, context, libs)
            elif isinstance(context, InstantQueryContext):
                return _handle_vector_instant(ast, context, libs)
        case promql_parser.NumberLiteral:
            return ast
        case _:
            raise ValueError("unknown op", type(ast), ast)
    raise ValueError()


def resolve_ast(ast: AstType, context: QueryContext, libs: Dict[str, Library]) -> pl.LazyFrame:
    # first let's figure out what we have, if we don't end up with a DF, make one up
    result = _resolve_ast(ast, context, libs)
    if isinstance(result, pl.LazyFrame):
        return result
    elif isinstance(result, promql_parser.NumberLiteral):
        # make a fake frame in interval
        if isinstance(context, InstantQueryContext):
            return pl.DataFrame(
                {
                    "idx": [datetime.utcnow() if context.timestamp is None else context.timestamp],
                    "library": [None],
                    "symbol": [None],
                    "value": [result.val],
                }
            ).lazy()
        elif isinstance(context, RangeQueryContext):
            step = timedelta(seconds=30) if context.step is None else context.step
            current = context.start
            values: Dict[str, Any] = {
                "idx": [],
                "library": [],
                "symbol": [],
                "value": [],
            }
            while current < context.end:
                values["idx"].append(current)
                values["library"].append(None)
                values["symbol"].append(None)
                values["value"].append(result.val)
                current += step
            return pl.DataFrame(values).lazy()
    raise ValueError("unhandled non-df AST", type(ast), ast)


def render_results(frame: pl.LazyFrame, context: QueryContext) -> Dict[str, Any]:
    frames = frame.collect().partition_by(["library", "symbol"])
    if isinstance(context, RangeQueryContext):
        return {
            "status": "success",
            "data": {
                "resultType": "matrix",
                "result": [
                    {
                        "metric": {
                            "__name__": "arctic_read",
                            "symbol": df.select("symbol")[0].item(),  # TODO: safety, less horrible
                            "library": df.select("library")[0].item(),
                        },
                        "values": [[row["idx"].timestamp(), str(row["value"])] for row in df.iter_rows(named=True)],
                    }
                    for df in frames
                ],
            },
        }
    elif isinstance(context, InstantQueryContext):
        return {
            "status": "success",
            "data": {
                "resultType": "vector",
                "result": [
                    {
                        "metric": {
                            "__name__": "arctic_read",
                            "symbol": df.select("symbol")[0].item(),  # TODO: safety, less horrible
                            "library": df.select("library")[0].item(),
                        },
                        "value": [df.select("idx")[0].item().timestamp(), str(df.select("value")[0].item())],
                    }
                    for df in frames
                ],
            },
        }
    else:
        raise ValueError()
