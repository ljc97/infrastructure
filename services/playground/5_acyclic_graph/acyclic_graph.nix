{ pkgs }: pkgs.mkShell {
  buildInputs = with pkgs; [
    openjdk17
    gradle
  ];
}
