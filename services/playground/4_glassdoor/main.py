import logging
import os
import pickle
import sys
from functools import partial
from typing import Any, Dict

import requests
from bs4 import BeautifulSoup, PageElement
from dateutil.parser import parse

URL = "https://www.glassdoor.co.uk/Reviews/Smarkets-Reviews-E730464{page}.htm?sort.sortType=RD&sort.ascending=false&filter.iso3Language=eng"

logging.basicConfig(stream=sys.stdout, level=logging.INFO)
log = logging.getLogger(__name__)


def _map_rating(element: PageElement) -> Dict[str, Any]:
    return {"rating": float(element.get_text())}


def _map_text_with_key(element: PageElement, *, key: str) -> Dict[str, Any]:
    return {key: element.get_text()}


def _map_date_title(element: PageElement) -> Dict[str, Any]:
    text = element.get_text().split("-")
    return {"review_date": parse(text[0]), "job_title": text[1].strip()}


def _debug_map(element: PageElement) -> Dict[str, Any]:
    print(element)
    return {}


SPAN_CLASS_TARGETS = {
    "ratingNumber": _map_rating,
    "common__EiReviewDetailsStyle__newUiJobLine": _map_date_title,
    "pt-xsm": partial(_map_text_with_key, key="employee_status"),
}
SPAN_DATA_TARGETS = {
    "pros": partial(_map_text_with_key, key="pros"),
    "cons": partial(_map_text_with_key, key="cons"),
}

ALL_REVIEWS = []
if os.path.exists("data.pickle"):
    with open("data.pickle", "rb") as f:
        ALL_REVIEWS = pickle.load(f)

if __name__ == "__main__":
    resp = requests.get(URL.format(page=""))

    if resp.status_code != 200:
        log.error("Loading Page failed", resp, resp.text)
        exit(-1)

    data = BeautifulSoup(resp.content, features="lxml")

    log.info("Loaded Page, parsing")

    reviews = data.find_all("div", class_="gdReview")
    log.info("Identified %s review(s)", len(reviews))

    for review in reviews:
        review_link = review.find_all("a", class_="reviewLink")
        review_data = {"link": "https://glassdoor.co.uk" + review_link[0]["href"]}

        spans = review.find_all("span")
        for span in spans:
            for cls in span.get("class", []):
                parse_fn = next((v for k, v in SPAN_CLASS_TARGETS.items() if k == cls), None)
                if parse_fn is not None:
                    review_data.update(parse_fn(span))
                break
            data_test = span.get("data-test")
            if data_test is not None:
                parse_fn = next((v for k, v in SPAN_DATA_TARGETS.items() if k == data_test), None)
                if parse_fn is not None:
                    review_data.update(parse_fn(span))

        if review_data not in ALL_REVIEWS:
            ALL_REVIEWS.append(review_data)
            with open("data.pickle", "wb") as f:
                pickle.dump(ALL_REVIEWS, f)
            log.info("Added %s to the cache", review_data)
log.info("Fin")
