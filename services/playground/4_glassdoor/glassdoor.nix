{ pkgs }:
pkgs.mkShell {
  nativeBuildInputs = [
    pkgs.python310
    pkgs.python310Packages.beautifulsoup4
    pkgs.python310Packages.requests
    pkgs.python310Packages.dateutil
  ];
}
