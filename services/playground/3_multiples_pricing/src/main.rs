// use futures::prelude::*;
// use futures::stream::FuturesUnordered;
use rand::Rng;
use rayon::prelude::*;
use std::collections::{BTreeSet, HashMap};
use std::sync::{Arc, RwLock};
use std::time::Instant;
// use std::{thread, time};
use tokio;
// use tokio::time::{sleep, Duration};

#[tokio::main(flavor = "multi_thread", worker_threads = 2)]
async fn main() {
    let mut rng = rand::thread_rng();

    let single_pricing: Arc<RwLock<HashMap<Arc<i64>, u8>>> = Arc::new(RwLock::new(HashMap::new()));
    let multiple_pricing: Arc<RwLock<HashMap<Arc<BTreeSet<i64>>, u64>>> =
        Arc::new(RwLock::new(HashMap::new()));
    let mut single_to_multiple_map: HashMap<i64, Vec<Arc<BTreeSet<i64>>>> = HashMap::new();

    for contract_id in 1..100000 {
        single_pricing
            .write()
            .unwrap()
            .insert(Arc::new(contract_id), generate_random_price());
    }

    // println!("Singles Pricing {:?}", single_pricing);

    for _ in 1..15000 {
        let multiple_contract_id = generate_multiple();
        for contract_id in multiple_contract_id.iter() {
            match single_to_multiple_map.get_mut(contract_id) {
                Some(vec) => {
                    vec.push(Arc::clone(&multiple_contract_id));
                }
                None => {
                    let vec = vec![Arc::clone(&multiple_contract_id)];
                    single_to_multiple_map.insert(*contract_id, vec);
                }
            };
        }
        let (_, price) = generate_multiple_pricing(
            Arc::clone(&single_pricing),
            Arc::clone(&multiple_contract_id),
        );
        multiple_pricing
            .write()
            .unwrap()
            .insert(Arc::clone(&multiple_contract_id), price);
    }

    // println!("Single->Multiple Map {:?}", single_to_multiple_map);
    // println!("Multiples Pricing {:?}", multiple_pricing);
    let mut overall_total = 0;
    let overall_start = Instant::now();
    for _ in 1..25 {
        let start = Instant::now();
        let contract_updates = rng.gen_range(1..30000);
        println!("Triggered {:?} contract update(s)", contract_updates);
        let mut updated_multiples_total = 0;
        let mut multiples: Vec<Arc<BTreeSet<i64>>> = Vec::new();
        for _ in 1..contract_updates {
            let contract_id = Arc::new(rng.gen_range(1..100));
            let new_price = generate_random_price();
            single_pricing
                .write()
                .unwrap()
                .insert(Arc::clone(&contract_id), new_price);
            multiples.extend(
                single_to_multiple_map
                    .get(&contract_id)
                    .unwrap_or(&Vec::new())
                    .clone(),
            );
            updated_multiples_total += single_to_multiple_map
                .get(&contract_id)
                .unwrap_or(&Vec::new())
                .clone()
                .len();
        }
        update_price_book(
            Arc::clone(&single_pricing),
            Arc::clone(&multiple_pricing),
            multiples,
        )
        .await;
        // println!("Multiples Pricing {:?}", multiple_pricing);
        let duration = start.elapsed();
        println!("Updates {:?} multiple(s)", updated_multiples_total);
        println!("Took {:?}", duration);
        let avg = duration.as_millis() as f64 / updated_multiples_total as f64;
        println!("Averaged {:?}", avg);
        overall_total += updated_multiples_total;
    }
    let duration = overall_start.elapsed();
    println!("Updates {:?} multiple(s)", overall_total);
    println!("Took {:?}", duration);
    let avg = duration.as_millis() as f64 / overall_total as f64;
    println!("Averaged {:?}", avg);
}

async fn update_price_book(
    single_pricing: Arc<RwLock<HashMap<Arc<i64>, u8>>>,
    multiple_pricing: Arc<RwLock<HashMap<Arc<BTreeSet<i64>>, u64>>>,
    multiples: Vec<Arc<BTreeSet<i64>>>,
) {
    let chunk_size = (multiples.len() as f64 / 128.0) as usize;
    if chunk_size == 0 {
        return;
    };
    multiples
        .par_chunks(chunk_size)
        .for_each(|multiples_chunk| {
            let mut price_updates: Vec<(Arc<BTreeSet<i64>>, u64)> = Vec::new();
            for multiple_contract_id in multiples_chunk {
                let (_, price) = generate_multiple_pricing(
                    Arc::clone(&single_pricing),
                    Arc::clone(multiple_contract_id),
                );
                price_updates.push((Arc::clone(multiple_contract_id), price));
            }
            {
                let mut write_multiples = multiple_pricing.write().unwrap();
                for (multiple_contract_id, price) in price_updates {
                    write_multiples.insert(multiple_contract_id, price);
                }
            }
        });
    // let mut future_price_pairs = multiples
    //     .into_iter()
    //     .map(|multiple_contract_id| {
    //         tokio::spawn(generate_multiple_pricing(
    //             Arc::clone(&single_pricing),
    //             Arc::clone(&multiple_pricing),
    //             multiple_contract_id,
    //         ))
    //     })
    //     .collect::<FuturesUnordered<_>>();

    // while let Some(res) = future_price_pairs.next().await {
    //     match res {
    //         Ok(_) => (),
    //         Err(_) => (),
    //     };
    // }
}
fn generate_random_price() -> u8 {
    let mut rng = rand::thread_rng();
    rng.gen::<u8>()
}

fn generate_multiple() -> Arc<BTreeSet<i64>> {
    let mut rng = rand::thread_rng();
    let num_legs = rng.gen_range(2..5);
    let mut multiple_contract_id: BTreeSet<i64> = BTreeSet::new();
    for _ in 1..num_legs {
        multiple_contract_id.insert(rng.gen_range(1..1000));
    }
    Arc::new(multiple_contract_id)
}

fn generate_multiple_pricing(
    single_pricing: Arc<RwLock<HashMap<Arc<i64>, u8>>>,
    multiple_contract_id: Arc<BTreeSet<i64>>,
) -> (Arc<BTreeSet<i64>>, u64) {
    let mut product: u64 = 1;
    {
        let read_pricing = single_pricing.read().unwrap();
        for contract_id in multiple_contract_id.iter() {
            product *= (*read_pricing.get(contract_id).unwrap()) as u64;
        }
    }
    // multiple_pricing
    //     .write()
    //     .unwrap()
    //     .insert(Arc::clone(&multiple_contract_id), product);
    // sleep(Duration::from_millis(1)).await;
    // thread::sleep(time::Duration::from_millis(1));
    (multiple_contract_id, product)
}
