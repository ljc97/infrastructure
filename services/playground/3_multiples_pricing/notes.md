# Notes

- Generate random(ly) priced singles contracts
  - Then derive a number of multiples from those, each with 2-5 legs
- Iteratively generate new price updates
  - For each single contract update, re-calculate the current price of each multiple
    - No Margin
- Test with Rayon for parallel computation, Tokio calculating them async
  - Doesn't really make a difference given overhead of it, versus quick calculations
- Future improvements
  - Separate price generator -> worker threads
  - Distribute prices using SPMC style channel subscriptions
  - Assign multiples to worker threads - randomly, by "locality" (i.e existing price sub)
  - Coordinator to manage which multiples to price, discontinue etc based on: user subscription, settled/voided status
