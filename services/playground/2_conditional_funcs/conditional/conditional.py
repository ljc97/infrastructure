import inspect
from collections import defaultdict
from functools import wraps
from typing import Callable, List, Optional, Tuple, TypeVar, cast

import pytest
from typing_extensions import ParamSpec

P = ParamSpec("P")
R = TypeVar("R")

FUNCTION_MAPPINGS: defaultdict[Callable[P, R], List[Tuple[List[Callable[P, bool]], Callable[P, R]]]] = defaultdict(list)


def conditional(
    conditions: Optional[List[Callable[P, bool]]] = None,
    replacing: Optional[Callable[P, R]] = None,
) -> Callable[[Callable[P, R]], Callable[P, R]]:
    # print("args", conditions, replacing)
    if conditions is None is not replacing is None:
        raise Exception("lolno")

    def decorator(function: Callable[P, R]) -> Callable[P, R]:
        if conditions is None or replacing is None:
            # print("default map", function, " to ", function)
            FUNCTION_MAPPINGS[function].append(([], function))
        else:
            # print("map", inspect.unwrap(replacing), " to ", function)
            FUNCTION_MAPPINGS[inspect.unwrap(replacing)].append((conditions, function))

        @wraps(function)
        def wrapper(*args: P.args, **kwargs: P.kwargs) -> R:
            # print("applicable for ", function, FUNCTION_MAPPINGS[function][1:])
            for lambdas, func in FUNCTION_MAPPINGS[function][1:]:
                _func: Callable[P, R] = cast(Callable[P, R], func)
                if all(fn(args, kwargs) for fn in lambdas):
                    return _func(*args, **kwargs)
            return function(*args, **kwargs)

        return wrapper

    return decorator


@conditional()
def f(a: int, b: int) -> int:
    return a * b


@conditional([lambda args, _: args[0] == 1], f)
def f2(a: int, b: int) -> int:
    return 2 * a * b


@conditional([lambda args, _: args[0] == 0 and args[1] == 0], f)
def f3(a: int, b: int) -> int:
    return 1


@pytest.mark.parametrize(
    "a, b, exp",
    [
        (3, 4, 12),
        (1, 4, 8),
        (0, 0, 1),
    ],
)
def test_conditional(a: int, b: int, exp: int) -> None:
    assert f(a, b) == exp


if __name__ == "__main__":
    print(f(3, 4))
    print(f(1, 4))
    print(f(0, 0))
