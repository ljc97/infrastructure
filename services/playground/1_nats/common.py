SUBJECT = "foo"
PORTS = [4222, 4223, 4224]
SERVERS = [f"nats://127.0.0.1:{port}" for port in PORTS]
BATCH_SIZE = 4096
