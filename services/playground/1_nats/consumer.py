import asyncio

import nats
from common import SERVERS, SUBJECT
from data_pb2 import Person


async def run(loop):
    nc = await nats.connect(servers=SERVERS)

    async def message_handler(msg):
        # subject = msg.subject
        # reply = msg.reply
        p = Person()
        p.ParseFromString(msg.data)
        # print(p.__str__())
        # print("Received a message on '{subject} {reply}': {data}".format(
        # subject=subject, reply=reply, data=data))

    await nc.subscribe(SUBJECT, cb=message_handler)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(loop))
    try:
        loop.run_forever()
    finally:
        loop.close()
