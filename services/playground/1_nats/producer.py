import asyncio
from datetime import datetime
from random import SystemRandom

import nats
from common import BATCH_SIZE, SERVERS, SUBJECT
from data_pb2 import Person

rng = SystemRandom()

INTERVAL = 0.0001
NAMES = ["arealperson", "dave", "notarealperson"]


async def main():
    nc = await nats.connect(servers=SERVERS)

    start_time = datetime.utcnow()

    iterations = 0
    while True:
        p = Person()
        p.name = rng.choice(NAMES)
        p.id = rng.randint(1, 2**16)
        await nc.publish(SUBJECT, p.SerializeToString())
        iterations += 1
        if iterations % BATCH_SIZE == 0:
            await nc.flush()
            difference = (datetime.utcnow() - start_time).total_seconds()
            print(f"{iterations} at {BATCH_SIZE/float(difference)}/s")
            start_time = datetime.utcnow()
        # time.sleep(INTERVAL)

    await nc.drain()


if __name__ == "__main__":
    asyncio.run(main())
