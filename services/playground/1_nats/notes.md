# Notes

- UI Available at http://127.0.0.1:8222/connz
- Batch flush vastly improves performance
- Producer Much Faster than Consumer
  - Monitor "pending_bytes": 0,
  - Particularly at message rates >100k
  - Only using single node of the 3 currently
    - Multiple nodes breaks the webui (it's per node)
  - 1_nats-nats-1 | [1] 2022/02/11 21:58:27.043077 [WRN] 172.19.0.1:36234 - cid:11 - "v2.0.0:python3" - Readloop processing time: 4.406741959s
- Protobuf works fairly well
  - Sustains 40-50k/s with a simple struct
  - Go Consumer can easily deal with ~80k w/ protobuf
    - Need multiple python producers
- QueueSubscribe
  - Registers consumers onto same queue, splits messages between
- nats-top
  - Cool tooling for message rate etc
