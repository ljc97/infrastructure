package main

import (
	"fmt"
	"time"

	"github.com/nats-io/nats.go"
	"google.golang.org/protobuf/proto"
)

func main() {
	nc, _ := nats.Connect("127.0.0.1:4222")
	fmt.Print("Connected, subscribing\n")
	start_time := time.Now().UnixMilli()
	iterations := 0
	nc.QueueSubscribe("foo", "q", func(m *nats.Msg) {
		// fmt.Printf("Received a message: %s\n", string(m.Data))
		p := &Person{}
		err := proto.Unmarshal(m.Data, p)
		if err != nil {
			fmt.Println("Fail on message\n")
		}

		iterations += 1
		if iterations%4096 == 0 {
			fmt.Printf("Received id: %s %s, %f/s\n", p.Id, iterations, float64(4096)*1000.0/float64(time.Now().UnixMilli()-start_time))
			start_time = time.Now().UnixMilli()
		}
	})

	for {

	}

	nc.Drain()
	nc.Close()
}
