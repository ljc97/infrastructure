{ pkgs }:
pkgs.mkShell {
  nativeBuildInputs = [
    pkgs.python311
    pkgs.python311Packages.protobuf
    pkgs.python311Packages.nats-py
    pkgs.nats-server
    pkgs.nats-top
    pkgs.go
  ];
}
