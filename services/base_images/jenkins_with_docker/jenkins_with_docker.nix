{ pkgs, dockerTools }: dockerTools.buildImage {
  name = "jenkins_with_docker";

  fromImage = dockerTools.pullImage {
    imageName = "jenkins/jenkins";
    imageDigest =
      "sha256:2849bf4e829b8ac2a805a7e40984606da9724424fce69d422ea7fdd747c7319b";
    finalImageName = "jenkins/jenkins";
    finalImageTag = "2.400-jdk17";
    sha256 = "sha256-N0WAJa+aslX2sAOCcpKwBhTNAip74uSpK1qA03scduk=";
    os = "linux";
    arch = "amd64";
  };

  fromImageName = "jenkins/jenkins";
  fromImageTag = "2.400-jdk17";

  copyToRoot = pkgs.buildEnv {
    name = "image-root";
    paths = [ pkgs.docker-client ];
    pathsToLink = [ "/bin" ];
  };

  config = {
    Entrypoint = [
      "/usr/bin/tini"
      "--"
      "/usr/local/bin/jenkins.sh"
    ];
  };
}
