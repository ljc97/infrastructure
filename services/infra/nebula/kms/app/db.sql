CREATE TABLE client (
    client_id   uuid    PRIMARY KEY,
    client_name varchar UNIQUE,
    ip          inet    UNIQUE
);

CREATE TABLE client_group (
    client_id   uuid    REFERENCES client (client_id),
    group_name varchar,
    UNIQUE (client_id, group_name)
);