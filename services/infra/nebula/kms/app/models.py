from dataclasses import dataclass
from pathlib import Path
from typing import List, Optional


@dataclass
class ClientCert:
    key: str
    cert: str
    ca_cert: str


@dataclass
class Client:
    client_id: str
    name: str
    ip: str
    groups: Optional[List[str]]
    cert: Optional[ClientCert]


@dataclass
class CertificateAuthority:
    name: str
    key: Path
    cert: Path

    def validate(self):
        if self.name.strip() == "":
            return False

        if not isinstance(self.key, Path) or not isinstance(self.cert, Path):
            return False

        if self.key.is_dir() or self.cert.is_dir():
            return False

        if not self.key.exists() or not self.cert.exists():
            return False

        return True

    def __post_init__(self):
        if not self.validate():
            raise ValueError("Invalid CA Provided")
