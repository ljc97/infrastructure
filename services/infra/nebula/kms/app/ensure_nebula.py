import os
import tarfile
from pathlib import Path
from typing import Dict

import requests

RELEASES = "https://api.github.com/repos/slackhq/nebula/releases/latest"
ARCH_OVERRIDE = {"x86_64": "amd64", "armv7": "arm-7"}


def get_release_list() -> Dict[str, str]:
    response = requests.get(RELEASES)
    if response.status_code != 200:
        exit(-1)

    data = response.json()

    available_releases = {a["name"]: a["browser_download_url"] for a in data["assets"] if "nebula-linux" in a["name"]}

    return available_releases


def download_file(url: str, path: str) -> None:
    response = requests.get(url)
    if response.status_code != 200:
        exit(-1)

    with open(path, "wb") as f:
        f.write(response.content)


def extract_nebula(archive: str, output_dir: str) -> None:
    with tarfile.open(archive, mode="r") as archive:
        output_path = Path(output_dir)
        if output_path.exists() and not output_path.is_dir():
            exit(-1)

        if not output_path.exists():
            os.makedirs(output_dir)

        archive.extractall(output_path)


if __name__ == "__main__":
    releases = get_release_list()
    _, _, _, _, arch = os.uname()
    arch = ARCH_OVERRIDE.get(arch, arch)

    release_url = releases.get(f"nebula-linux-{arch}.tar.gz")
    if not release_url:
        exit(-1)

    archive = "/tmp/nebula.tar.gz"
    output_dir = "/opt/nebula/"
    download_file(release_url, archive)

    extract_nebula(archive, output_dir)
