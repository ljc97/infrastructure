import os
import subprocess
from pathlib import Path
from typing import Dict, List, Optional

from models import CertificateAuthority, ClientCert


class Keygen:
    _ca_certs: Dict[str, str] = {}
    _cas: Dict[str, CertificateAuthority] = {}

    def __init__(self, nebula_dir: Path, cas: List[CertificateAuthority]):
        assert nebula_dir.exists()
        assert nebula_dir.is_dir()

        nebula_cert_path = Path(nebula_dir) / "nebula-cert"
        assert Path(nebula_cert_path).exists()

        for ca in cas:
            with open(ca.cert, "r") as ca_cert:
                self._ca_certs[ca.name] = ca_cert.read()
            self._cas[ca.name] = ca

        nebula_help = subprocess.run([nebula_cert_path, "--help"], cwd=nebula_dir)
        assert nebula_help.returncode == 0

        self._nebula_cert_path = nebula_cert_path
        self._nebula_dir = nebula_dir

    def generate_client_certificate(
        self, ca_name: str, name: str, ip: str, groups: Optional[List[str]] = None
    ) -> ClientCert:
        key_path = self._nebula_dir / f"{name}.key"
        cert_path = self._nebula_dir / f"{name}.crt"

        assert not key_path.exists()
        assert not cert_path.exists()

        args = [
            "./nebula-cert",
            "sign",
            "-name",
            name,
            "-ip",
            ip,
            "-ca-crt",
            self._cas[ca_name].cert,
            "-ca-key",
            self._cas[ca_name].key,
        ]

        if groups is not None:
            args.append("-groups")
            groups_str = ",".join(groups)
            args.append(groups_str)

        gen_cert = subprocess.run(args, cwd=self._nebula_dir)
        assert gen_cert.returncode == 0

        key = None
        with open(key_path, "r") as key_file:
            key = key_file.read()
        assert key is not None
        cert = None
        with open(cert_path, "r") as cert_file:
            cert = cert_file.read()
        assert key is not None

        os.remove(key_path)
        os.remove(cert_path)

        return ClientCert(key=key, cert=cert, ca_cert=self._ca_certs[ca_name])

    def available_ca_certs(self) -> Dict[str, str]:
        return self._ca_certs
