from typing import List, Optional, Set
from uuid import uuid4

from gw.gateway import Gateway
from keygen import Keygen
from models import Client
from netaddr import IPAddress, IPNetwork
from sqlalchemy.db import Connectable


class DBGateway(Gateway):
    _db = None

    def __init__(self, pool: str, keygen: Keygen, db: Connectable):
        super.__init__(pool, keygen)
        self._db = db

    def _get_client_groups_by_id(self, client_id: str) -> Optional[List[str]]:
        query = "SELECT group_name FROM client_group WHERE client_id = :client_id"

        results = self._db.execute(query, client_id=client_id)

        groups = []
        for row in results:
            data = dict(row)
            groups.append(data["group_name"])

        return groups if len(groups) > 0 else None

    def get_all_clients(self) -> List[Client]:
        query = "SELECT client_id, client_name, ip FROM client"

        results = self._db.execute(query)

        clients: List[Client] = []
        for row in results:
            data = dict(row)
            groups = self._get_client_groups_by_id(data["client_id"])
            clients.append(
                Client(
                    client_id=data["client_id"],
                    name=data["client_name"],
                    ip=data["ip"],
                    groups=groups,
                )
            )

    def get_client_by_id(self, client_id: str) -> Optional[Client]:
        query = "SELECT client_name, ip FROM client WHERE client_id = :client_id"

        result = self._db.execute(query).first()

        if not result:
            return None

        data = dict(result)

        return Client(
            client_id=client_id,
            name=data["client_name"],
            ip=data["ip"],
            groups=self._get_client_groups_by_id(client_id),
        )

    def _get_allocated_addresses(self) -> Set[IPAddress]:
        query = "SELECT ip FROM client"

        results = self._db.execute(query)

        ips = set()
        for row in results:
            data = dict(row)
            ips.add(data["ip"])

        return ips

    def _store_group(self, client_id: str, group: str) -> None:
        query = "INSERT into client_group (client_id, group_name) VALUES (:client_id, :group)"

        self._db.execute(query, client_id=client_id, group=group)

    def _try_create_client(self, client_id: str, name: str, ip: IPAddress) -> None:
        query = "INSERT INTO client (client_id, client_name, ip) VALUES (:client_id, :name, :ip)"

        self._db.execute(query, client_id=client_id, name=name, ip=ip)

    def create_client(self, ca_name: str, name: str, groups: Optional[List[str]]) -> Client:
        attempts = 3
        while attempts > 0:
            client_id = str(uuid4())

            allocated_addresses = self._get_allocated_addresses()
            with self._lock:
                for ip in IPNetwork(self._pool).iter_hosts():
                    str_ip = str(ip)
                    if ip not in allocated_addresses:
                        break
            str_ip = str_ip + "/" + self._pool.split("/")[1]

            try:
                self._try_create_client(client_id, name, ip)
                break
            except Exception as e:
                print(e)  # catch conflict error properly later
                attempts -= 1
        if attempts == 0:
            raise Exception("Unable to store client - out of attempts")

        _, client_with_cert = self._generate_cert(ca_name, client_id, name, str_ip, groups)
        if groups is not None:
            for g in groups:
                self._store_group(client_id, g)

        return client_with_cert

    def update_client_key_cert(self, ca_name: str, client_id: str) -> Optional[Client]:
        client = self.get_client_by_id(client_id)
        if not client:
            return None

        _, client_with_cert = self._generate_cert(ca_name, client.client_id, client.name, client.ip, client.groups)

        return client_with_cert
