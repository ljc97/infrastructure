import threading
from typing import List, Optional, Set
from uuid import uuid4

from gw.gateway import Gateway
from models import Client
from netaddr import IPNetwork


class LocalGateway(Gateway):
    CLIENTS = {}
    ALLOCATED_ADDRESSES: Set[str] = set()
    USED_NAMES: Set[str] = set()
    _lock = threading.Lock()

    def get_all_clients(self) -> List[Client]:
        return [c for _, c in self.CLIENTS.items()]

    def get_client_by_id(self, client_id: str) -> Optional[Client]:
        return self.CLIENTS.get(client_id)

    def create_client(self, ca_name: str, name: str, groups: Optional[List[str]] = None) -> Client:
        assert name not in self.USED_NAMES
        self.USED_NAMES.add(name)

        while (client_id := str(uuid4())) in self.CLIENTS:
            continue

        available_address = None
        with self._lock:
            for ip in IPNetwork(self._pool).iter_hosts():
                ip = str(ip)
                if ip not in self.ALLOCATED_ADDRESSES:
                    available_address = ip
                    break
        if available_address:
            self.ALLOCATED_ADDRESSES.add(available_address)

        ip = ip + "/" + self._pool.split("/")[1]
        client, client_with_cert = self._generate_cert(ca_name, client_id, name, ip, groups)
        self.CLIENTS[client_id] = client

        return client_with_cert

    def update_client_key_cert(self, ca_name: str, client_id: str) -> Optional[Client]:
        if client_id not in self.CLIENTS:
            return None

        client = self.CLIENTS[client_id]
        client, client_with_cert = self._generate_cert(ca_name, client.client_id, client.name, client.ip, client.groups)
        self.CLIENTS[client_id] = client

        return client_with_cert
