import abc
from typing import List, Optional, Tuple

from keygen import Keygen
from models import Client


class Gateway(metaclass=abc.ABCMeta):
    def __init__(self, pool: str, keygen: Keygen):
        self._pool = pool
        self._keygen = keygen

    @abc.abstractmethod
    def get_all_clients(self) -> List[Client]:
        pass

    @abc.abstractmethod
    def get_client_by_id(self, client_id: str) -> Optional[Client]:
        pass

    @abc.abstractmethod
    def create_client(self, name: str, groups: Optional[List[str]] = None) -> Client:
        pass

    @abc.abstractmethod
    def update_client_key_cert(self, ca_name: str, client_id: str) -> Optional[Client]:
        pass

    def _generate_cert(
        self,
        ca_name: str,
        client_id: str,
        name: str,
        ip: str,
        groups: Optional[List[str]] = None,
    ) -> Tuple[Client, Client]:
        cert = self._keygen.generate_client_certificate(ca_name, name, ip, groups)
        client = Client(client_id=client_id, name=name, ip=ip, groups=groups, cert=None)
        client_with_cert = Client(client_id=client_id, name=name, ip=ip, groups=groups, cert=cert)
        return (client, client_with_cert)
