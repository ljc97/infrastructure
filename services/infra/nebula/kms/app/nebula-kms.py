import json
from dataclasses import asdict
from functools import wraps
from pathlib import Path

import toml
from flask import Flask, current_app, make_response, request
from gw.gateway import Gateway
from gw.local_gateway import LocalGateway
from keygen import Keygen
from models import CertificateAuthority


def auth(f):
    @wraps(f)
    def check_auth(*_, **__):
        auth = request.authorization
        if auth:
            username = auth.username
            password = auth.password
            if current_app.config.get("authentication", {}).get(username, "") == password:
                return f(*_, **__)
        return make_response("UNAUTHORIZED", 401)

    return check_auth


app = Flask(__name__)


@app.get("/client/")
@auth
def get_clients():
    clients = current_app.gateway.get_all_clients()
    return {"clients": clients}


@app.get("/client/<string:client_id>/")
@auth
def get_client_by_id(client_id: str):
    client = current_app.gateway.get_client_by_id(client_id)
    return json.dumps(asdict(client)) if client else make_response("NOT_FOUND", 404)


@app.post("/client/")
@auth
def create_client():
    data = request.json
    name = data["name"]
    groups = data.get("groups", None)
    ca_name = data["ca_name"]

    client = current_app.gateway.create_client(ca_name, name, groups)
    return json.dumps(asdict(client))


@app.post("/client/<string:client_id>/refresh/")
@auth
def refresh_client_cert(client_id: str):
    data = request.json

    client = current_app.gateway.update_client_key_cert(data["ca_name"], client_id)
    return json.dumps(asdict(client)) if client else make_response("NOT_FOUND", 404)


@app.get("/certificate_authority/")
@auth
def get_cas():
    return json.dumps(current_app.keygen.available_ca_certs())


if __name__ == "__main__":
    app.config.update(toml.load("/config/config.toml"))

    config_dir = Path("/config")
    cas = []
    for ca_name, details in app.config["certificate_authorities"].items():
        cas.append(
            CertificateAuthority(
                name=ca_name,
                key=config_dir / details["key"],
                cert=config_dir / details["cert"],
            )
        )

    app.keygen: Keygen = Keygen(Path("/opt/nebula/"), cas)
    app.gateway: Gateway = LocalGateway(app.config["nebula"]["pool"], app.keygen)
    app.run(host="0.0.0.0", debug=True)
