# Nebula KMS

- Basic Implementation of CA/Client Certificate Management
- Basic Authentication supported
- Configured via "/config/config.toml"
  - Supports multiple CAs (addition/removal of CA requires app reboot)
  - Example below
- Does not persist certificates generated
- Only supports a single IP pool

## Config

```
[authentication]
basic = "password" # supports multiple different users, but no CA specific auth

[nebula]
pool = "10.10.10.0/24" # supports a single IP pool

[certificate_authorities] # multiple CA support, key/crt must end up in /config/
    [certificate_authorities.ca-2021]
    key = "2021-ca.key"
    cert = "2021-ca.crt"
```

## Build/Run

- Example Command

```
docker build . -t nebula-kms && docker run -it -p 5000:5000 -v $PWD/app/config.toml:/config/config.toml:ro -v $PWD/app/:/app/ -v $PWD/app/ca.crt:/config/2021-ca.crt -v $PWD/app/ca.key:/config/2021-ca.key nebula-kms /bin/bash
```
