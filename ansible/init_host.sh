#! /bin/bash
echo "Init Host for Ansible"

echo "Setup SSH"
copy_keys="ssh-copy-id -i ../keys/id_rsa.pub $1@$2"
echo "$copy_keys"

eval "$copy_keys"

echo "Setup Passwordless SSH"
pwd_ssh="ssh -t -i ../keys/id_rsa $1@$2 \"echo '$1 ALL=(ALL) NOPASSWD:ALL' | sudo tee /etc/sudoers.d/01-$1\""
echo "$pwd_ssh"

eval "$pwd_ssh"
