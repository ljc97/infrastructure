#!/bin/bash
/etc/openvpn/easy-rsa/easyrsa gen-crl
cp /etc/openvpn/crl.pem /etc/openvpn/crl.pem.bak
cp /etc/openvpn/easy-rsa/pki/crl.pem /etc/openvpn/crl.pem
service openvpn restart
