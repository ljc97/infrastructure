#! /bin/sh

find . -type f -name 'poetry.lock' | while read -r lockfile; do
  # Get the directory containing the poetry.lock file
  dir=$(dirname "$lockfile")

  # Change to that directory
  echo "Found poetry.lock in: $dir"
  echo "Running 'nix-shell -p poetry --command \"poetry update --lock\"' in $dir"

  # Run nix-shell command in the directory
  (
    cd "$dir" || exit
    nix-shell -p poetry --command "poetry update --lock"
    echo "Finished $dir"
  )
done
