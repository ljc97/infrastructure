#include <stdexcept>
#include <stdio.h>

int main() {

  // lifetime is post-constructor, pre-destructor
  // automatic duration when scoped
  // static or extern allocate on start, deallocate on stop
  // static == internal linkage, accessible only to this translation unit
  // local statics are also a thing

  // static thread_local is what it sounds like

  // dynamic storage duration
  int *ptr = new int{42};
  delete ptr;

  int *arr = new int[25600];
  delete[] arr;

  try {
    throw std::runtime_error{"test exception"};
  } catch (const std::runtime_error &err) {
    // exception handlers will catch class and it's children
    //  i.e subclasses
    printf("Exc %s\n", err.what());
  }

  try {
    throw 'z';
  } catch (...) {
    // 'handles' all
    // throw; re-throw
  }

  // noexcept - mark any function that can't throw
  //  allows additional compiler optimizations, e.g move semantics

  // during an exception, check current stack frame for handler
  //  if not found, move up, destructing objects as we go
  //  if any exception during destructor, terminate
  //      hence destructors should ideally be noexcept

  // copy semantics
  //  default is a member wise copy
  //  same with copy assignment
  //      and then you also need to free the lhs existing value
  //  should explicitly declare copy constructors
  //      as `= default;` if default behaviour is ok
  //      or `= delete;`

  // move semantics
  //  after y is moved into x, x is equivalent
  //  y is then 'moved-from'
  //      may only be reassigned or destructed

  // value categories
  // glvalue
  // prvalue
  // xvalue
  // lvalue
  // rvalue
  // simple version for now - lvalue is anything with a name, rvalue is !lvalue

  // std::move takes lvalue ref to rvalue ref
  // lvalue suppresses moves, rvalue enables it

  // move construction
  //  take rvalue references
  //      can take the contents, then zero/null the original members
  //      to leave other in a moved-from state

  // tldr implement nothing or everything (i.e define or delete)
  //  defining only copy will convert moves to copy implicitly

  return 0;
}