#include <stdio.h>

int main() {

  // Define interface as struct with virtual methods
  //  virtual allows deference to derived class, even if using a base class ref
  //  =0 suffix to a function (pure virtual func) requires derivation to
  //  implement
  //      and disabled instantiation of the base class
  //  destructors should be defined as virtual (in case derived needs to
  //  actually destruct stuff)

  // since we only know the T at runtime, have to use pointers to interfaces
  //  to set them, we can use constructor or property injection
  //  property injection uses pointer, if we want to change it during lifecycle

  struct Test {
    virtual ~Test() = default;
    virtual int do_something(int a, int b) = 0;
  };

  struct TestImplA : Test {
    int do_something(int a, int b) { return a * b; }
  };

  struct TestImplB : Test {
    int do_something(int a, int b) { return 2 * a * b; }
  };

  TestImplA a;
  TestImplB b;
  Test *t = &a;
  printf("%d\n", t->do_something(1, 2));
  t = &b;
  printf("%d\n", t->do_something(1, 2));

  return 0;
}