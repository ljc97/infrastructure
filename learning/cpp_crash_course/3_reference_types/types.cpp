#include <stdio.h>

int main() {

  int *ptr;
  int x{};
  ptr = &x;
  printf("%p, %d\n", ptr, x);

  *ptr = 1;
  printf("%p, %d\n", ptr, x);

  int arr[3]{};
  int *arr_ptr = arr; // decay

  arr_ptr += 2; // points at element 2
  printf("%d\n", arr[2]);
  *arr_ptr = 5;
  printf("%d\n", arr[2]);

  void *void_ptr = arr;
  // *void_ptr = 1; will fail

  // references, can't be reseated, no null assignment

  // classes have implicit `this` ptr to refer to self

  // const on args, or functions

  // member initializer
  struct TestStruct {
    TestStruct(int val, const char *other_val)
        : val{val}, other_val{other_val} {}
    int val;
    const char *other_val;
  };

  // auto works as expected
  auto y = x;
  auto &y_ref = y;
  auto *y_ptr = &y;

  return 0;
}