#! /bin/bash
dir=$(ls -d "$1"*)
echo "Building $dir"
clang++ "$dir"/*.cpp -Werror -g -std=c++20 -o "$dir"/main.out
ret=$?
if [ $ret -ne 0 ]; then
  echo "Compile Failed $ret"
  exit $ret
fi
echo "Running $dir"
./"$dir"/main.out
ret=$?
if [ $ret -ne 0 ]; then
  echo "Run Failed $ret"
  exit $ret
fi
echo "Valgrinding $dir"
valgrind --leak-check=full "$dir"/main.out
