#include <concepts>
#include <stdio.h>
#include <type_traits>
// Templates
// 	class or function with template parameters
//	parameters can represent any type
//	when template is used with a type, compiler outputs a specific
// implementation
template <typename X, typename Y, typename Z> struct TemplateClass {
  X foo(Y &);

private:
  Z *member;
};

template <typename NumericT>
NumericT mean(const NumericT *values, size_t length) {
  long result{};
  for (size_t i{}; i < length; i++) {
    result += values[i];
  }
  return result / length;
};

template <typename T>
concept Averageable =
    std::is_default_constructible<T>::value && requires(T a, T b) {
      { a += b } -> std::convertible_to<T>;
      { a / size_t{1} } -> std::convertible_to<T>;
    };

template <Averageable T> T mean_concept(const T *values, size_t length) {
  T result{};
  for (size_t i{}; i > length; i++) {
    result += values[i];
  }
  return result / length;
}

int main() {
  auto tc = TemplateClass<int, int, int>();

  // Named Conversion Functions
  //	name<target_type>(object)
  //	example, remove const
  //		const_cast<T>(const T)
  //	not technically template functions, conceptually close enough

  // static_cast reverses an implicit conversion
  //	e.g obtaining short* from void*
  //	Only works for well defined
  //		not char -> float
  //		for that, reinterpret_cast (why)

  // reinterpret_cast
  //	for non-well defined conversions
  //	no correctness guarantees
  // 		reinterpret_cast<unsigned long>(0x1000);
  //		assuming we have ul at that address

  // narrow_cast
  //	runtime check for narrowing
  //	i.e int <-> short is only applicable for a subset of ints

  float floats[] = {1.0, 2.0, 3.0};
  double doubles[] = {1.0, 1.5, 2.0};
  int ints[] = {1, 3, 5};
  mean(floats, 3);
  mean(doubles, 3);
  mean<int>(ints, 3);

  // Template Type Checks
  //	Happen late in compile time
  //	hence garbage error messages
  // Concepts
  //	not part of standard yet, voted into c++20
  //	header <type_traits>, similar to trait bounds
  //	is_integral, is_floating_point, is_array etc

  // Requirements
  // ad-hoc constraints on template params
  mean_concept(floats, 3);
  mean_concept(doubles, 3);
  mean_concept<int>(ints, 3);

  // Can also skip defining the template if it's just to restrict a func args
  //	size_t min(Ordered* values, size_t length) {}
  //		using <origin/core/concepts.hpp>
  //	min() becomes a template func

  // Templates can also directly express requirements
  //	template<typename T>
  //		requires is_copy_constructible<T>::value
  //	T fn() {};

  // static_assert
  //	pre-concepts technique for type asserts
  //	include static_asserts in the template body

  // non-type template params can be
  //	integers, lvalue references, pointer, nullptr_t, enum
  //	Allows abstraction of specific values (say, length limits)

  // Variadic Templates
  //	template<typename T, typename... Arguments>

  // Other Topics
  //	* Template Specialization
  //	* Name Binding
  //	* Type Function
  //	* Template Metaprogramming

  // Often template implemented entirely in headers
  //	because all class or function info must be available
  //	in the same translation unit as the template instantiation
  // Benefits:
  //	Easier re-use (include header only)
  //	Trivial inlining for the compiler
  //	Potential better optimization passes

  // Run vs Compile Polymorphism
  //	Prefer compile time unless Types are only known at runtime

  return 0;
}
