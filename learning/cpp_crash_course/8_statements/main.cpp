#include <stdio.h>

int main() {

  // Expression Statements
  //	expression followed by ;

  // Compound Statements
  //	also called blocks
  //	sequence enclosed by braces
  //	each block declares a block scope
  //		sets up lifetimes for automatic variables within

  // Declaration Statements
  //	introduce identifiers (func, templates, namespaces etc)

  // Functions
  //	can use declared functions whenever, as long as they are eventually
  // defined 	for member functions, we can declare them later using scope
  // resolution 		i.e <rt> MyClass::func(<args>) {}

  // Namespaces
  //	prevent naming conflicts
  //	can be nested, or just namespace One::Two::Three {}

  // Using directive
  //	shorter way to access items in a namespace
  //	should never be used in a .h file (will leak -> global of every source
  // including the header)

  // Type Aliasing
  //	using String = const char[260];
  //	no change in meaning or function
  //	Can have template params
  //		enables partial application
  //		e.g using int_int_something = SomeTemplate<int,int>;

  // Structured Bindings
  //	unpack objects into elements
  //	works for any type with non-static data members that are public
  //	auto [obj1, obj2] = some_struct;

  // Attributes
  //	implementation defined features to an expression, enclosed in [[]]
  //	e.g	noreturn, deprecated, fallthrough, nodiscard, maybe_unused,
  // carries_dependency
  //	[[noreturn]] void throw() { throw std::runtime_error("");}

  // Selection Statements
  //	conditional control flow, e.g if
  //	if supports initialisation, e.g
  //		if (auto [success, data] = read_file(); success) {}
  //	if can be constexpr
  //		valuable for templates handling specifics of some types
  //	switch, also supports instantiation

  // Iteration Statements
  // while, do-while, for, for (range based)
  // range-expressions need begin() and end() which return iterators

  // Jump Statements
  //	break, continue, goto, continue
  // 	labels, e.g
  //		some_label:
  //			printf("whatever");
  return 0;
}
