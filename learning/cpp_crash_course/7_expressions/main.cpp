#include <stdio.h>

int comma_op(int &x) { return x = 9, x++, x / 2; }

int main() {
  // Operators
  // standard, logical, bitwise
  // promotion towards larger width type
  // promotion towards signed

  // Assignment operators
  //   i.e x += y
  //  Promotion rules don't really apply, type can't change

  // Postfix/Prefix inc/dec

  // Member Access Operators
  //  Subscript arr[x]
  //  *x == element pointer
  //  x.y == Member of
  //  x->y == Member of, given x is a ptr

  // Ternary Ops
  //   x ? a : b

  // Comma Operator
  //  left to right, rightmost is return
  int x = 10;
  int y = comma_op(x);
  printf("%d, %d\n", x, y);

  // Operator Overloading
  //  class can define implementation for operators
  //  <limits> header also has functions for max/min size of numeric types

  // Overloading new
  //  allocates onto the heap via malloc
  //  some systems don't provide heap allocastion
  //    would preclude stdlib containers etc
  //  option in these environments is to overload the new operator
  //    Fragmentation becomes a common issue
  //    Can utilise bucketed blocks of memory to help
  //      See VirtualAllocEx, HeapAlloc will use this when allocating > page
  //      size (windows)

  // Placement Operators
  //  partial replacement of free store allocations

  // Operator Precedence and Associativity
  //  gigantic table, use brackets/split stmts if confusing

  // Evaluation Order
  //  Placement is well defined, compile time
  //  Execution order within a line is arbitrary
  //    If order is required, separate lines needed
  //    This way since it's better for optimisation

  // User Defined Literals
  //  e.g <chrono> defines 'ms'

  // Type Conversion
  //  Implicit conversion is common
  //    Narrowing generally unwarned
  //    Explicit init can prevent ^
  //  FP to integer - promotion rules
  //  Int to Int - implementation defined UB
  //  Fp to FP - UB if dest cannot fit source
  //  bool - implicit
  //  ptr to void* - implicit

  // Explicit Conversion
  //  generally called casts
  //  braced init a good example - type safe + non-narrowing

  // C Style Casting
  //  (new-type)object
  //  Prefer static_, const_, reinterpret_

  // UDT Conversions
  //   implicit
  //    operator destination-type() const { };
  //   explicit
  //    explicit operator destination-type() const {};

  // Constexpr
  //  move calculation to compiletime
  //  define constexpr functions, then we can use those with literals
  //  to make the compiler evaluate these rather than harcoding more complex
  //  literals

  // Volatile
  //  every access must be treated as if it has a side effect
  //  access cannot be reordered, or optimised away
  //  important in embedded in some cases

  return 0;
}