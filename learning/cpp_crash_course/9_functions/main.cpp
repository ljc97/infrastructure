#include <stdio.h>

int main(int argc, char **argv) {

  // Function Declarations
  //  prefix-modifiers return-type func-name(arguments) suffix-modifiers;

  // prefix modifiers
  //  static - internal linkage
  //    or for a class it belongs to the class, not the instance of it
  //  virtual - method can be overriden by a child class
  //  constexpr - evaluated at compile time if possible
  //  [[noreturn]] - as before
  //  inline - guides compiler optimisation

  // suffix modifiers
  //  noexcept - guide optimisations
  //  const - method won't modify, allows const references to invoke the method
  //  final - cannot be overriden by a child
  //    or inherited if applied to the class
  //  volatile - allow method call on volatile objects

  // auto return types
  //  not necessarily the best for standard functions
  //  can be useful for function templates

  // Overload Resolution
  //  1 - Exact Type Match
  //  2 - int/float promotions
  //  3 - Standard Conversions (int->float, ptr-child->ptr-parent)
  //  4 - User Defined Conversion
  //  5 - Variadic Function

  // Variadic Functions
  //  similar to *args/**kwargs
  //  cannot access args in '...' directly
  //  use <stdarg.h> functions (va_list, va_start/va_end+va_arg, va_copy)
  //  problems
  //    not type safe - second argument of va_args is a type
  //    number of elements need to be tracked separately
  //  variadic templates are safer/more performant

  // Variadic Templates
  //    template<typename... Args>
  //    return-type func-name(Args... args){}
  //  use 'parameter pack' semantics to access
  //    sizeof(args) to get size
  //  basically define a compile time recursive function

  // Fold Expressions
  //  when the aim is to use a single op over a list of args
  //    constexpr auto sum(T... args) { return(... + args);}

  // Function Pointers
  //   act like a const*
  //  type aliases can be used to clean upcode
  //    using op = float(*)(float,int);

  // Function Call Operator
  //  can make udts callable by overloading operator()()
  //  makes it a 'function type', instances are 'function objects'
  //  permits any combination of args, returns, and modifiers (except static)
  //  can be used to fix argument values, and thus create partials
  //    lambdas may be nicer

  // Lambda Expressions
  //  five components
  //    captures - member variables (partially applied params)
  //    parameters - supports defaults
  //    body
  //    specifiers
  //    return type
  //  can be made generic by including auto params
  //    ends up being duplicated for each type on instantiation
  //  capture is defaulted to by value
  //     can do reference if needed
  //  default capture
  //    [=] - all, by value
  //    [&] - all, by reference
  //  mutable specifier needed if lambda mutates value-captured vars
  //    including calling non-const methods
  //  can mix default and specific variables
  //  generally - avoid default
  //  capture list can also be used to initialise (rename or move)
  //  can capture class with this or *this
  //  by default - lambdas are constexpr, if it can be invoked at compile time
  //    can explicitly add constexpr to make sure

  int x = 2;
  auto prod_2 = [&x](int y = 0) -> int { return x * y; };
  printf("%d,%d\n", prod_2(3), prod_2());

  // std::function
  //  poymorphic wrapper around a callable
  //    std::function<int(int, int)> - return_type(arg1type,arg2type)
  //  can cause more *indirect* functional calls (ptr deref)

  // main function and the command line
  //  int main();
  //  int main(int argc, char** argc[]);
  //  int main(int argc, char** argv[], impl-parameters);
  //  usually the first arg is the path to the executable
  //
  for (size_t i{}; i < argc; i++) {
    printf("%zu: %s\n", i, argv[i]);
  }

  // implicit return 0 if omitted
  return 0;
}
