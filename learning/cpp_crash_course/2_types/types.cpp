#include <stdint.h>
#include <stdio.h>

int main() {
  // standard integer types
  // long(s) vary between Win and !Win
  // fixed width types in <stdint.h>
  short sh = 0;                     // %hd
  unsigned short u_sh = 0;          // %hu
  int integer = 0;                  // %d
  unsigned int u_integer = 0;       // %u
  long lng = 0;                     // %ld
  unsigned long u_lng = 0;          // %lu
  long long lng_lng = 0;            // %lld
  unsigned long long u_lng_lng = 0; // %llu
  int64_t fixed_64b_int = 0;

  // numerical representations;
  int x = 0'0; // dec
  x = 0b01;    // binary
  x = 012;     // oct
  x = 0x0F;    // hex

  // floating point
  // %f for decimal, %e for scientific, %g for most compact
  //      prepend l for double, L for lond double
  float f = 0.0F;
  double d = 0.0;
  long double l_d = 0.3L;

  // Characters
  // char - 1 byte
  // signed char
  // unsigned char
  char c = 'c'; // %c
  // char16_t, char32_t
  char16_t c_16 = u'c';
  char32_t c_32 = U'c';
  // wchar_t - platform specific size
  wchar_t c_w = L'c'; // %lc

  // Escapes
  // \ as usual
  // unicode 4-digit \u
  // unicode 8-digit \U
  c_w = U'\U0001F37A';

  bool b = false;

  // Usual comparsion + logical Ops

  // std::byte
  // raw byte ops <stddef.h>, no exact C match

  // size_t
  size_t s_z = sizeof(c_w);
  // used to encode the size of objects
  // generally ULL for 64 bit procs
  // format as %zu for dec, %zx for hex

  // void

  // arrays
  int arr[3];
  int arr_1[] = {0, 1, 2};

  // loops
  // sizeof(array)/sizeof(element) or std::size
  for (size_t idx = 0; idx < sizeof(arr_1) / sizeof(arr_1[0]); idx++) {
    arr_1[idx] += 1;
  }
  for (int val : arr_1) {
    val += 1;
  }

  // strings
  char c_arr[] = "A sentence";

  // UDTs
  // Enums, Classes, Unions
  // scoped (i.e ::) versus unscoped enum MyEnum {};
  enum class ExampleEnum { First, Second, Third };
  ExampleEnum e = ExampleEnum::First;

  // Members sequential in memory
  //  May be moved for word alignment
  //  Generally aim for largest -> smallest
  struct ExampleStruct {
    int x;
    ExampleEnum e;
  };
  ExampleStruct s = ExampleStruct{0, ExampleEnum::Second};

  union exampleUnion {
    int integer;
    double floating_point;
  };
  exampleUnion u;
  u.integer = 42;
  // printf("%d\n", u.integer);           // 42
  // printf("%f\n", u.floating_point);    // 0.0
  u.floating_point = 3.7;
  // printf("%d\n", u.integer);           // -1717986918
  // printf("%f\n", u.floating_point);    // 3.700000

  // Classes
  // can add methods to structs
  // public/private specifiers available
  // class keyword declares members as private by default

  // Initialization
  // braced is valid for all types
  // last not guaranteed to be zeroed
  //  same with arrays, i.e int arr[5]{}; is better
  int i = 0;
  int i_1{};
  int i_2 = {};
  int i_3;

  // Destructors
  // default implementation
  // can define, no args
  // calls setup at compile time

  return 0;
}