{ pkgs }:
pkgs.mkShell {
  nativeBuildInputs = [
    pkgs.gcc
    pkgs.gdb
    pkgs.clang_18
    pkgs.lldb_18
    pkgs.llvmPackages.bintools
    pkgs.valgrind
    pkgs.helix
    #pkgs.rnix-lsp
  ];
}
