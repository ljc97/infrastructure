# 118 Pascal's Triangle
# Given an integer numRows, return the first numRows of Pascal's triangle.
# In Pascal's triangle, each number is the sum of the two numbers directly above it as shown:


from typing import List

import pytest


class Solution:
    def generate(self, numRows: int) -> List[List[int]]:
        height = numRows
        work: List[List[int]] = [[]] * height

        for row in range(1, height + 1):
            start_idx = (row * (row - 1)) // 2
            end_idx = (row * (row + 1)) // 2
            width = end_idx - start_idx
            work[row - 1] = [0] * (end_idx - start_idx)
            for idx in range(0, width):
                if idx in (0, width - 1):
                    work[row - 1][idx] = 1
                    continue
                if row < 2:
                    work[row - 1][idx] = 1
                    continue
                work[row - 1][idx] = work[row - 2][idx - 1] + work[row - 2][idx]
        return work


@pytest.mark.parametrize(
    "n, expected",
    [
        (1, [[1]]),
        (5, [[1], [1, 1], [1, 2, 1], [1, 3, 3, 1], [1, 4, 6, 4, 1]]),
    ],
)
def test(n: int, expected: List[List[int]]) -> None:
    assert Solution().generate(n) == expected
