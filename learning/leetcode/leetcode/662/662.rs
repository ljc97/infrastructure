// 662. Maximum Width of Binary Tree
// Given the root of a binary tree, return the maximum width of the given tree.
// The maximum width of a tree is the maximum width among all levels.
// The width of one level is defined as the length between the end-nodes
//  (the leftmost and rightmost non-null nodes),
//  where the null nodes between the end-nodes that would be present in a complete binary tree
//  extending down to that level are also counted into the length calculation.
// The number of nodes in the tree is in the range [1, 3000].
// -100 <= Node.val <= 100
use std::cmp::{max, min};
use std::collections::VecDeque;
#[derive(Debug, PartialEq, Eq)]
pub struct TreeNode {
    pub val: i32,
    pub left: Option<Rc<RefCell<TreeNode>>>,
    pub right: Option<Rc<RefCell<TreeNode>>>,
}

impl TreeNode {
    #[inline]
    pub fn new(val: i32) -> Self {
        TreeNode {
            val,
            left: None,
            right: None,
        }
    }
}

use std::cell::RefCell;
use std::rc::Rc;
#[derive(Debug)]
struct QueueNode {
    node: Option<Rc<RefCell<TreeNode>>>,
    idx: usize,
    level: usize,
}
pub fn width_of_binary_tree(root: Option<Rc<RefCell<TreeNode>>>) -> i32 {
    if root.is_none() {
        return 0;
    }
    let mut queue: VecDeque<QueueNode> = VecDeque::new();
    queue.push_back(QueueNode {
        node: root,
        idx: 0,
        level: 0,
    });

    let mut min_in_level: Option<usize> = None;
    let mut max_in_level: Option<usize> = None;
    let mut current_level = 0;
    let mut max_width = 0;
    while queue.len() > 0 {
        let node = queue.pop_front().unwrap();
        if node.node.is_none() {
            continue;
        }
        if node.level != current_level {
            if max_in_level.is_some() && min_in_level.is_some() {
                max_width = max(max_width, max_in_level.unwrap() - min_in_level.unwrap() + 1);
            }
            current_level = node.level;
            min_in_level = None;
            max_in_level = None;
        }

        min_in_level = Some(min(node.idx, min_in_level.unwrap_or(node.idx)));
        max_in_level = Some(max(node.idx, max_in_level.unwrap_or(node.idx)));

        queue.push_back(QueueNode {
            node: node.node.as_ref().unwrap().borrow().left.clone(),
            idx: 2 * node.idx + 1,
            level: node.level + 1,
        });
        queue.push_back(QueueNode {
            node: node.node.as_ref().unwrap().borrow().right.clone(),
            idx: 2 * node.idx + 2,
            level: node.level + 1,
        });
    }
    if max_in_level.is_some() && min_in_level.is_some() {
        max_width = max(max_width, max_in_level.unwrap() - min_in_level.unwrap() + 1);
    }
    return max_width as i32;
}

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::rstest;

    fn convert_to_graph(input: Vec<Option<i32>>) -> Option<Rc<RefCell<TreeNode>>> {
        if input.len() == 0 {
            return None;
        }

        fn add_node(input: &Vec<Option<i32>>, idx: usize) -> Option<Rc<RefCell<TreeNode>>> {
            if idx >= input.len() || input[idx].is_none() {
                return None;
            }
            let left = 2 * idx + 1;
            let right = 2 * idx + 2;

            let node = Some(Rc::new(RefCell::new(TreeNode {
                val: input[idx].unwrap(),
                left: add_node(input, left),
                right: add_node(input, right),
            })));

            return node;
        }
        let base_node = add_node(&input, 0);
        return base_node;
    }

    #[rstest]
    #[case(vec!(), 0)]
    #[case(vec!(Some(1),Some(3),Some(2),Some(5),None,None,Some(9),Some(6),None, None, None, None, None,Some(7)), 7)]
    #[case(vec!(Some(1),Some(3),Some(2),Some(5)), 2)]
    #[case(vec!(Some(2), Some(3), None, Some(1)), 1)]
    fn test_width_of_binary_tree(#[case] input: Vec<Option<i32>>, #[case] expected: i32) {
        assert_eq!(expected, width_of_binary_tree(convert_to_graph(input)));
    }
}
