// Given an integer array nums sorted in non-decreasing order
// remove the duplicates in-place such that each unique element appears only once.
// The relative order of the elements should be kept the same.
// Then return the number of unique elements in nums.

pub fn remove_duplicates(nums: &mut Vec<i32>) -> i32 {
    if nums.len() == 0 {
        return 0;
    }

    let mut last_seen = nums[0];
    let mut idx = 1;
    let mut head_idx = 1;

    while idx < nums.len() {
        if nums[idx] > last_seen {
            nums[head_idx] = nums[idx];
            last_seen = nums[head_idx];
            head_idx += 1;
        }
        idx += 1;
    }

    return head_idx as i32;
}

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::rstest;
    #[rstest]
    #[case(vec!(), vec!(), 0)]
    #[case(vec!(1,1), vec!(1), 1)]
    #[case(vec!(1,1,2), vec!(1,2), 2)]
    #[case(vec!(0,0,1,1,1,2,2,3,3,4), vec!(0,1,2,3,4), 5)]
    fn test_remove_duplicates(
        #[case] mut input: Vec<i32>,
        #[case] expected: Vec<i32>,
        #[case] k: i32,
    ) {
        let returned_k = remove_duplicates(&mut input);
        assert_eq!(k, returned_k);
        assert_eq!(expected[0..k as usize], input[0..k as usize]);
    }
}
