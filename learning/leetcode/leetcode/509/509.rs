fn fib_dp(n: usize) -> usize {
    let mut out: Vec<usize> = vec![0; n + 1];
    if n >= 1 {
        out[1] = 1;
    }

    for idx in 2..n + 1 {
        out[idx] = out[idx - 1] + out[idx - 2];
    }
    return out[n];
}

fn fib_iter(n: usize) -> usize {
    if n == 0 {
        return 0;
    }

    let mut prec_prec: usize = 0;
    let mut prec: usize = 1;

    let mut current = prec + prec_prec;
    for _ in 2..n {
        prec_prec = prec;
        prec = current;
        current = prec + prec_prec;
    }
    return current;
}

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::rstest;

    #[rstest]
    #[case(0, 0)]
    #[case(1, 1)]
    #[case(2, 1)]
    #[case(3, 2)]
    #[case(4, 3)]
    fn test_fib_dp(#[case] input: usize, #[case] expected: usize) {
        assert_eq!(expected, fib_dp(input))
    }

    #[rstest]
    #[case(0, 0)]
    #[case(1, 1)]
    #[case(2, 1)]
    #[case(3, 2)]
    #[case(4, 3)]
    fn test_fib_iter(#[case] input: usize, #[case] expected: usize) {
        assert_eq!(expected, fib_iter(input))
    }
}
