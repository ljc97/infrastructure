# 509 Fibonacci Number
# The Fibonacci numbers, commonly denoted F(n) form a sequence, called the Fibonacci sequence
# Such that each number is the sum of the two preceding ones, starting from 0 and 1. That is,
# F(0) = 0, F(1) = 1
# F(n) = F(n - 1) + F(n - 2), for n > 1.
# Given n, calculate F(n).


import pytest


class Solution:
    def fib(self, n: int) -> int:
        out = [0] * (n + 1)
        if n >= 1:
            out[1] = 1
        for idx in range(2, n + 1):
            out[idx] = out[idx - 1] + out[idx - 2]
        return out[n]


@pytest.mark.parametrize("n, expected", [(0, 0), (1, 1), (2, 1), (3, 2), (4, 3)])
def test(n: int, expected: int) -> None:
    assert Solution().fib(n) == expected
