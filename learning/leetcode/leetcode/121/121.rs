fn max_profit(prices: Vec<usize>) -> usize {
    if prices.len() <= 1 {
        return 0;
    }

    let mut max: usize = 0;

    let mut idx = 1;
    let mut c_min = prices[0];

    while idx < prices.len() {
        let p = prices[idx];
        if p < c_min {
            c_min = p;
        }
        if p - c_min > max {
            max = p - c_min;
        }
        idx += 1;
    }
    max
}

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::rstest;

    #[rstest]
    #[case(vec ! (7, 1, 5, 3, 6, 4), 5)]
    #[case(vec ! (7, 6, 5, 4), 0)]
    #[case(vec ! (1, 3, 4, 7, 5, 9), 8)]
    #[case(vec ! (), 0)]
    #[case(vec ! (1), 0)]
    fn test_max_profit(#[case] input: Vec<usize>, #[case] expected: usize) {
        assert_eq!(expected, max_profit(input))
    }
}
