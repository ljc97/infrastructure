# 121 Best Time to Buy and Sell Stock
# You are given an array prices where prices[i] is the price of a given stock on the ith day.
# You want to maximize your profit by choosing a single day to buy one stock and choosing a different day in the future to sell that stock.
# Return the maximum profit you can achieve from this transaction. If you cannot achieve any profit, return 0.


from typing import List

import pytest


class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        c_max = 0

        idx = 0
        c_min = None
        while idx < len(prices):
            p = prices[idx]
            if c_min is None or p < c_min:
                c_min = p
            if p - c_min > c_max:
                c_max = p - c_min

            idx += 1

        return c_max


@pytest.mark.parametrize(
    "prices, expected",
    [
        ([7, 1, 5, 3, 6, 4], 5),
        ([7, 6, 5, 4], 0),
        ([1, 3, 4, 7, 5, 9], 8),
        ([], 0),
        ([1], 0),
    ],
)
def test(prices: List[int], expected: int) -> None:
    assert Solution().maxProfit(prices) == expected
