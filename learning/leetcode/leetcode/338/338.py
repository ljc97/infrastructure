# 338 Counting Bits
# Given an integer n
# return an array ans of length n + 1
# such that for each i (0 <= i <= n)
# ans[i] is the number of 1's in the binary representation of i.

from typing import List

import pytest


class Solution:
    def countBits(self, n: int) -> List[int]:
        out = [0] * (n + 1)
        for i in range(1, n + 1):
            out[i] = out[i // 2] + i % 2
        return out


# lsr and add 1 if odd
# 0 00000 = 0
# 1 00001 = 1
# 2 00010 = 1
# 3 00011 = 2

# 4 00100 = 1
# 5 00101 = 2
# 6 00110 = 2
# 7 00111 = 3


@pytest.mark.parametrize(
    "n, expected",
    [
        (2, [0, 1, 1]),
        (5, [0, 1, 1, 2, 1, 2]),
        (6, [0, 1, 1, 2, 1, 2, 2]),
        (0, [0]),
        (17, [0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 1, 2]),
    ],
)
def test(n: int, expected: List[int]) -> None:
    assert Solution().countBits(n) == expected
