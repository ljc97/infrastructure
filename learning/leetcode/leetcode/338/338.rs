fn count_bits(n: usize) -> Vec<usize> {
    let mut out: Vec<usize> = vec![0; n + 1];
    for idx in 1..n + 1 {
        out[idx] = out[idx / 2] + idx % 2
    }
    return out;
}

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::rstest;
    #[rstest]
    #[case(2, vec!(0, 1, 1))]
    #[case(5, vec!(0, 1, 1, 2, 1, 2))]
    #[case(6, vec!(0, 1, 1, 2, 1, 2, 2))]
    #[case(0, vec!(0))]
    #[case(17, vec!(0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 1, 2))]
    fn test_count_bits(#[case] input: usize, #[case] expected: Vec<usize>) {
        assert_eq!(expected, count_bits(input))
    }
}
