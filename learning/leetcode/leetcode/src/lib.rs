extern crate proc_macro;
use proc_macro::TokenStream;
use regex::Regex;
use std::fs;
use std::path::Path;

#[proc_macro]
pub fn mod_discovery(_input: TokenStream) -> TokenStream {
    let paths = fs::read_dir("./").unwrap();
    let re = Regex::new(r"^.*\d{1,9}$").unwrap();
    let mut output: String = String::new();
    for path in paths {
        let path_str = path.as_ref().unwrap().path();
        if re.is_match(path_str.to_str().unwrap()) {
            let challenge_num = &path_str.to_str().unwrap()[2..];
            if Path::new(format!("./{}/{}.rs", challenge_num, challenge_num).as_str()).exists() {
                let challenge_path = format!("../{}/{}.rs", challenge_num, challenge_num);
                output.push_str(format!("#[path = \"{}\"]\n", challenge_path).as_str());
                output.push_str(format!("mod leetcode_{};\n", challenge_num).as_str());
            }
        }
    }
    let output = output.parse().unwrap();
    return output;
}
