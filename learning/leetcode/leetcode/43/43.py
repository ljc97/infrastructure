# 43. Multiply Strings
# Given two non-negative integers num1 and num2 represented as strings, return the product of num1 and num2, also represented as a string.
# Note: You must not use any built-in BigInteger library or convert the inputs to integer directly.

from typing import List

from hypothesis import given
from hypothesis import strategies as st

# kinda cheaty but these can be made manually fairly easily
times_tables = {(str(i), str(j)): f"{i * j}" for i in range(0, 10) for j in range(0, 10)}
single_digit_add = {
    (str(i), str(j)): (str(0 if i + j < 10 else (i + j) // 10), str((i + j) % 10))
    for i in range(0, 10)
    for j in range(0, 10)
}


def generic_add(numbers: List[str]) -> List[str]:
    reversed_nums = [n[::-1] for n in numbers]
    # print(single_digit_add)
    acc = ["0"]
    carry = ["0"]
    for idx in range(0, max(len(n) for n in reversed_nums)):
        for n in reversed_nums:
            # print("start", acc)
            if idx >= len(n):
                continue
            new_digits = single_digit_add[(acc[idx], n[idx])]
            new_carry = single_digit_add[(carry[0], new_digits[0])]
            # print(idx, acc, carry, new_digits, new_carry)
            acc[idx] = new_digits[1]
            carry[0] = new_carry[1]
            if new_carry[0] != "0":
                if len(carry) == 1:
                    carry.append("1")
                else:
                    carry = generic_add(["".join(carry[::-1]), "10"])
                    # print("new carry", carry)
        acc.append(carry[0])
        # print(acc)
        carry = carry[1:] if len(carry) > 1 else ["0"]
        # print(acc)
    acc.extend(carry)
    return acc
    # print("final", acc, carry)
    # last_0_pos = None
    # for n, idx in enumerate(acc[::-1]):
    #     if n == "0":
    #         last_0_pos = len(acc) - idx
    # return (acc[:last_0_pos] if last_0_pos is not None else acc) or ["0"]
    # return "".join(acc[::-1]).lstrip("0") or "0"


class Solution:
    def multiply(self, num1: str, num2: str) -> str:
        # cross multiply
        if len(num1) < len(num2):
            num1, num2 = num2, num1
        num1 = num1[::-1]
        num2 = num2[::-1]
        multiplications = []
        for idx in range(0, len(num1)):
            for idx2 in range(0, len(num2)):
                multiplications.append(times_tables[num1[idx], num2[idx2]] + "0" * (idx + idx2))
        # print(multiplications)
        return "".join(generic_add(multiplications)[::-1]).lstrip("0") or "0"


@given(st.integers(min_value=0), st.integers(min_value=0))
def test(num1: int, num2: int) -> None:
    assert Solution().multiply(str(num1), str(num2)) == str(num1 * num2)
