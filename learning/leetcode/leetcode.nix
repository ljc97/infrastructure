{ naersk, utils, gitignore }: {
  rust = naersk.buildPackage { src = gitignore.gitignoreSource ./leetcode; doCheck = true; };
  python = utils.mkPoetryApp { projectDir = ./.; };
}
